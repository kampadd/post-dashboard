#! /bin/bash
# PostDashboardServer shutdown script
# Make sure prerequisite environment variables are set

postdashboard_pid(){
echo `ps aux | grep PostDashboardServer.jar | grep -v grep | awk '{ print $2 }'`
}

pid=$(postdashboard_pid)
if [ -n "$pid" ]; then
echo "stopping PostDashboardServer:$pid"
kill -9 $pid
sleep 5
else
echo "PostDashboardServer is not running"
fi