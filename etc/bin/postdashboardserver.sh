#! /bin/bash
### BEGIN INIT INFO
# Provides: postdashboardserver 
# Required-Start: $remote_fs $syslog $postdashboardserver 
# Required-Stop: $remote_fs $syslog $postdashboardserver
# Default-Start: 2 3 4 5 
# Default-Stop: 0 1 6 
# Short-Description: PostDashboardServer 
# Description: This file starts and stops PostDashboard server
# 
### END INIT INFO

# Author: John Kojo Ampia - Addison 
# Email:  john.addison@thalesgroup.com

# The script assumes the installation of a compatible java version
# The PostDashboardServer.jar is compiled with java 1.8
# The PostDashboardServer.jar requires java 1.8 or better

# Make sure prerequisite environment variables are set
# POST_DASHBOARD_HOME is the complete home location path of the PostDashboardServer jar file.

echo "POST_DASHBOARD_HOME: "; env | grep POST_DASHBOARD_HOME

if [ -z $POST_DASHBOARD_HOME ]; then
  #If POST_DASHBOARD_HOME not set then guess a few locations before giving
  # up and demanding user set it.
  if [ -r PostDashboardServer.jar ]; then
     echo "POST_DASHBOARD_HOME environment variable not found, using current "
     echo "directory.  If not set then running this script from other "
     echo "directories will not work in the future."
     export POST_DASHBOARD_HOME=`pwd`
  else 
    if [ -r ../PostDashboardServer.jar ]; then
      echo "POST_DASHBOARD_HOME environment variable not found, using current "
      echo "location.  If not set then running this script from other "
      echo "directories will not work in the future."
      export POST_DASHBOARD_HOME=`pwd`/..
    fi
  fi 

  if [ -z "$POST_DASHBOARD_HOME" ]; then
    echo "The POST_DASHBOARD_HOME environment variable is not defined"
    echo "This environment variable is needed to run this program"
    echo "Please set it to the directory where postdashboardserver was installed"
    echo "Using default postdashboard home set in postdashboardserver script!"
    export POST_DASHBOARD_HOME="/opt/post/dashboard"	
	  echo "POST_DASHBOARD_HOME: "; env | grep POST_DASHBOARD_HOME
  fi
fi

LOG_FILE=$POST_DASHBOARD_HOME/logs/postdashboard.log
export LOG_FILE

postdashboard_pid(){
echo `ps aux | grep PostDashboardServer.jar | grep -v grep | awk '{ print $2 }'`
}

status(){
pid=$(postdashboard_pid)
if [ -n "$pid" ];then
echo "PostDashboardServer:$pid is running"
else
echo "PostDashboardServer is not running"
fi
}


start(){
exec $POST_DASHBOARD_HOME/bin/startup.sh 2>&1 > $LOG_FILE &
}

postdashboard_debug(){
exec $POST_DASHBOARD_HOME/bin/debug.sh 2>&1 > $LOG_FILE &
}

stop(){
pid=$(postdashboard_pid)
if [ -n "$pid" ];then
exec $POST_DASHBOARD_HOME/bin/shutdown.sh 2>&1 > /dev/null
else
status
fi
}


case "$1" in
 start)
	start
	status

   ;;
 stop)
	stop
	status

   ;;
 restart)
	stop
	start
	status
   ;;
 status)
	status
	;;
 debug)
	postdashboard_debug
	status
	;;
 *)
   echo "Usage: postdashboardserver {start|stop|restart|status|debug}" >&2
   exit 3
   ;;
esac
