#!/bin/bash
# PostDashboardServer start script
# The script assumes the installation of a compatible java version
# The PostDashboardServer.jar is compiled with java 1.8.0.65
# The PostDashboardServer.jar requires java 1.8.0.65 or better

# Make sure prerequisite environment variables are set

if [ -z $POST_DASHBOARD_HOME ]; then
  #If POST_DASHBOARD_HOME not set then guess a few locations before giving
  # up and demanding user set it.
  if [ -r PostDashboardServer.jar ]; then
     echo "POST_DASHBOARD_HOME environment variable not found, using current "
     echo "directory.  If not set then running this script from other "
     echo "directories will not work in the future."
     export POST_DASHBOARD_HOME=`pwd`
  else 
    if [ -r ../PostDashboardServer.jar ]; then
      echo "POST_DASHBOARD_HOME environment variable not found, using current "
      echo "location.  If not set then running this script from other "
      echo "directories will not work in the future."
      export POST_DASHBOARD_HOME=`pwd`/..
    fi
  fi 

  if [ -z "$POST_DASHBOARD_HOME" ]; then
    echo "The POST_DASHBOARD_HOME environment variable is not defined"
    echo "This environment variable is needed to run this program"
    echo "Please set it to the directory where cisamasterserver was installed"
    exit 1
  fi

fi

cd $POST_DASHBOARD_HOME
 java\
 -server\
 -Xms64m\
 -Xmx128m -Xmn256m\
 -XX:+UseParallelGC\
 -Djava.awt.headless=true\
 -jar PostDashboardServer.jar -Dspring.profiles.active=prod -Dserver.port=8085
