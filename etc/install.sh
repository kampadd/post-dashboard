#!/bin/bash 
# PostDashboardServer install script

# Export POST_DASHBOARD_HOME
POST_DASHBOARD_HOME=`pwd`
export POST_DASHBOARD_HOME
echo "POST_DASHBOARD_HOME: "; env | grep POST_DASHBOARD_HOME

# Setup log directory
LOG_DIR="/var/log/postdashboard"

if [ ! -d $LOG_DIR ]; then
	mkdir -p $LOG_DIR
fi

# Setup logs link
logs="$POST_DASHBOARD_HOME/logs"
if [ ! -d  $logs ]; then
	ln -s $LOG_DIR logs
fi

# Setup service script
if [ -L '/etc/init.d/postdashboardserver' ]; then
	rm -f /etc/init.d/postdashboardserver
	update-rc.d -f postdashboardserver remove 
fi

ln -s $POST_DASHBOARD_HOME/bin/postdashboardserver.sh /etc/init.d/postdashboardserver
chmod +x /etc/init.d/postdashboardserver

update-rc.d postdashboardserver defaults

sleep 1

service postdashboardserver stop
service postdashboardserver start
service postdashboardserver status
