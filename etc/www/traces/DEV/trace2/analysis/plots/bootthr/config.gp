TMF_TYPE="bootthr"
set title "Thread Boot (demo/cstrace, host SCC-1)"
set ylabel "Thread"
set xtic rotate
set grid
set yrange [-1:65]
set xlabel "Time"
set format x "%.9f"
set mouse format "%.9f"
TMF_X_AXIS="timestamp"

set terminal svg size 1024,768 dynamic mouse rounded jsdir "content/js" name "thread_boot_time"
set output "thread_boot_time.svg"

plot "bootthr/config.dat" using 2:3:4:5:yticlabel(1) with vectors lc 1 lw 2 ti "Thread Boot Time"
