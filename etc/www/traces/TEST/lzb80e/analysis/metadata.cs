/* CTF 1.8 */ 
typealias integer { size = 32; align = 32; signed = false; } := uint32_t; 
typealias integer { size = 64; align = 32; signed = false; } := uint64_t; 
trace { 
  major = 1; 
  minor = 8; 
  byte_order = be; 
  packet.header := struct { 
    uint32_t magic; 
    uint32_t stream_id; 
  }; 
}; 
env { 
  hostname = "evc_s1_0"; 
  domain = "TAS/PLF CS"; 
  tracer_name = "builtin"; 
  tracer_major = 0; 
  tracer_minor = 1; 
}; 
clock { 
  name = tsc; 
  freq = 1000000000; 
  offset_s = 946860839; 
  offset = 415647549; 
}; 
typealias integer { 
  size = 64; 
  align = 32; 
  signed = false; 
  map = clock.tsc.value; 
} := tsc_clock_t; 

struct packet_context { 
  uint32_t content_size; 
  uint32_t packet_size; 
}; 
struct event_header { 
  tsc_clock_t timestamp; 
  uint32_t id; 
}; 
struct event_context { 
  uint64_t thread_hash; 
}; 
stream { 
  id = 0; 
  packet.context := struct packet_context; 
  event.header := struct event_header; 
  event.context := struct event_context; 
};

stream { 
  id = 1; 
  event.header := struct event_header; 
  packet.context := struct packet_context; 
  event.context := struct event_context; 
}; 

enum _sigev_notify_t : uint32_t {
  SIGEV_NONE = 0,
  SIGEV_SIGNAL = 1,
  SIGEV_THREAD = 2
};

enum _clock_id_t : uint32_t {
  CLOCK_SYNCHRONIZED = 42,
  CLOCK_SYNCMONO = 43,
  CLOCK_CNTIME = 44
};

enum _cs_ev_name_t : uint32_t {
  EV_POINTX,
  EV_START_ACTIVE,
  EV_START_PASSIVE,
  EV_TOUCH_PAGE,
  EV_RESTORE_PAGE,
  EV_PRE_REC,
  EV_REC_DONE,
  EV_ERROR
};

enum __error : uint32_t { 
EPERM = 1, 
ENOENT, 
ESRCH, 
EINTR, 
EIO, 
ENXIO, 
E2BIG, 
ENOEXEC, 
EBADF, 
ECHILD, 
EAGAIN, 
ENOMEM, 
EACCES, 
EFAULT, 
ENOTBLK, 
EBUSY, 
EEXIST, 
EXDEV, 
ENODEV, 
ENOTDIR, 
EISDIR, 
EINVAL, 
ENFILE, 
EMFILE, 
ENOTTY, 
ETXTBSY, 
EFBIG, 
ENOSPC, 
ESPIPE, 
EROFS, 
EMLINK, 
EPIPE, 
EDOM, 
ERANGE, 
ENOSYS = 38, 
ERESTART = 85, 
EMSGSIZE = 90, 
ENOTSUP = 95, 
};

enum __ok_fail : uint32_t { _ok, _fail };

event {
 name = "sac73_check_leave";
 id = 0;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "sac73_check_enter";
 id = 1;
 stream_id = 0;
 fields := struct {
 string _md5;
 };

 };
event {
 name = "thread_setname_leave";
 id = 2;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "thread_setname_enter";
 id = 3;
 stream_id = 0;
 fields := struct {
 string _name;
 };

 };
event {
 name = "rec_status_leave";
 id = 4;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _new;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _old;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = rec_status_enter;
 id = 5;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "mq_connwatch_leave";
 id = 6;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = hex; } _status;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _at;
 string _conname;
 string _hname;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _port;
 
} _connwatch;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = mq_connwatch_enter;
 id = 7;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "ts_synchronize_thread_leave";
 id = 8;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = ts_synchronize_thread_enter;
 id = 9;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "ts_register_thread_leave";
 id = 10;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = ts_register_thread_enter;
 id = 11;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "ts_register_process_leave";
 id = 12;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = ts_register_process_enter;
 id = 13;
 stream_id = 0;
 fields := struct {}; 
 };
event
 {
 name = thread_exit;
 id = 14;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "mq_getattr_leave";
 id = 15;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = oct; } _mq_flags;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mq_maxmsg;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mq_msgsize;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mq_curmsgs;
 
} _attr;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_getattr_enter";
 id = 16;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 };

 };
event {
 name = "mq_setattr_leave";
 id = 17;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_setattr_enter";
 id = 18;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 integer { size = 32; align = 32; signed = 1 ; base = oct; } _mq_flags;
 };

 };
event {
 name = "clock_advance";
 id = 19;
 stream_id = 0;
 fields := struct {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _clock_syncmono;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _clock_synchronized;
 };

 };
event {
 name = "sl_enter_com";
 id = 20;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _round;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _global_time;
 };

 };
event
 {
 name = trc_thread_death;
 id = 21;
 stream_id = 1;
 fields := struct {}; 
 };
event
 {
 name = trc_point_x;
 id = 22;
 stream_id = 1;
 fields := struct {}; 
 };
event {
 name = "trc_queue_del";
 id = 23;
 stream_id = 1;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _qid;
 };

 };
event {
 name = "trc_queue_name";
 id = 24;
 stream_id = 1;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _qid;
 string _queue_name;
 };

 };
event {
 name = "trc_thread_name";
 id = 25;
 stream_id = 1;
 fields := struct {
 string _thread_name;
 };

 };
event {
 name = "trc_new_thread";
 id = 26;
 stream_id = 1;
 fields := struct {
 integer { size = 64; align = 32; signed = 0; base = 10; } _id;
 string _taskset_name;
 string _process_name;
 string _thread_name;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _v_pid;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _model;
 };

 };
event {
 name = "trc_lib_print_1";
 id = 27;
 stream_id = 1;
 fields := struct {
 string _txt;
 };

 };
event {
 name = "trc_lib_print_0";
 id = 28;
 stream_id = 0;
 fields := struct {
 string _txt;
 };

 };
event {
 name = "cs_trace_print";
 id = 29;
 stream_id = 0;
 fields := struct {
 string _txt;
 };

 };
event {
 name = "wait_event_leave";
 id = 30;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 enum _cs_ev_name_t _cs_ev_name;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ts_is_active;
 integer { size = 32; align = 32; signed = 1 ; base = hex; } _shared_size;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = wait_event_enter;
 id = 31;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "get_ceid_leave";
 id = 32;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event
 {
 name = get_ceid_enter;
 id = 33;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "timer_getoverrun_leave";
 id = 34;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "timer_getoverrun_enter";
 id = 35;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _timer_id;
 };

 };
event {
 name = "timer_gettime_leave";
 id = 36;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _value;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _it_interval;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "timer_gettime_enter";
 id = 37;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _timer_id;
 };

 };
event {
 name = "timer_settime_leave";
 id = 38;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 struct {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _it_value;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _it_interval;
 
} _ovalue;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "timer_settime_enter";
 id = 39;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _timer_id;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _flags;
 struct {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _it_value;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _it_interval;
 
} _value;
 };

 };
event {
 name = "timer_delete_leave";
 id = 40;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "timer_delete_enter";
 id = 41;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _timer_id;
 };

 };
event {
 name = "timer_create_leave";
 id = 42;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _timer_id;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "timer_create_enter";
 id = 43;
 stream_id = 0;
 fields := struct {
 enum _clock_id_t _clock_id;
 struct {
 enum _sigev_notify_t _sigev_notify;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _sigev_signo;
 
} _timer_event_spec;
 };

 };
event {
 name = "clock_getres_leave";
 id = 44;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _clock_res;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "clock_getres_enter";
 id = 45;
 stream_id = 0;
 fields := struct {
 enum _clock_id_t _clock_id;
 };

 };
event {
 name = "clock_settime_leave";
 id = 46;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "clock_settime_enter";
 id = 47;
 stream_id = 0;
 fields := struct {
 enum _clock_id_t _clock_id;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _clock_value;
 };

 };
event
 {
 name = cs_sched_yield_leave;
 id = 48;
 stream_id = 0;
 fields := struct {}; 
 };
event {
 name = "cs_sched_yield_enter";
 id = 49;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _recovery_point;
 };

 };
event {
 name = "nanosleep_leave";
 id = 50;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail,__intr } __ok_fail_intr; variant <__ok_fail_intr>
 {
 struct
 {
 enum __error err;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _rem;
 } __intr; 
 uint32_t __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "nanosleep_enter";
 id = 51;
 stream_id = 0;
 fields := struct {
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_sec;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _tv_nsec;
 
} _req;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _recovery_point;
 };

 };
event {
 name = "mq_receivev_leave";
 id = 52;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 0 ; base = dec; } __data0_len;
 integer { size = 8; align = 8; signed = 0; base = hex; } _data0 [ __data0_len ];
 integer { size = 32; align = 32; signed = 0 ; base = dec; } __data1_len;
 integer { size = 8; align = 8; signed = 0; base = hex; } _data1 [ __data1_len ];
 integer { size = 32; align = 32; signed = 0 ; base = dec; } __data2_len;
 integer { size = 8; align = 8; signed = 0; base = hex; } _data2 [ __data2_len ];
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _msg_prio;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_receivev_enter";
 id = 53;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _recovery_point;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _count;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _len [3];
 };

 };
event {
 name = "mq_receive_leave";
 id = 54;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 0 ; base = dec; } __data_len;
 integer { size = 8; align = 8; signed = 0; base = hex; } _data [ __data_len ];
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _msg_prio;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_receive_enter";
 id = 55;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _msg_len;
 };

 };
event {
 name = "mq_send_leave";
 id = 56;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_send_enter";
 id = 57;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _msg_len;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _msg_prio;
 integer { size = 32; align = 32; signed = 0 ; base = dec; } __data_len;
 integer { size = 8; align = 8; signed = 0; base = hex; } _data [ __data_len ];
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _recovery_point;
 };

 };
event {
 name = "mq_unlink_leave";
 id = 58;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_unlink_enter";
 id = 59;
 stream_id = 0;
 fields := struct {
 string _name;
 };

 };
event {
 name = "mq_close_leave";
 id = 60;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_close_enter";
 id = 61;
 stream_id = 0;
 fields := struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mqdes;
 };

 };
event {
 name = "mq_open_leave";
 id = 62;
 stream_id = 0;
 fields := struct {
 enum : uint32_t { __ok,__fail } __ok_fail; variant <__ok_fail>
 {
 struct
 {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _ret;
 } __ok;
 enum __error __fail;
 } __sysret;
 };

 };
event {
 name = "mq_open_enter";
 id = 63;
 stream_id = 0;
 fields := struct {
 string _name;
 integer { size = 32; align = 32; signed = 0 ; base = hex; } _oflag;
 integer { size = 32; align = 32; signed = 0 ; base = oct; } _mode;
 struct {
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mq_maxmsg;
 integer { size = 32; align = 32; signed = 1 ; base = dec; } _mq_msgsize;
 
} _attr;
 };

 };
