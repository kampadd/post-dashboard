set title "Taskset Load (lzb80e/cstrace, host evc_s1_0)"
set xlabel "Round"
set ylabel "Time (secs)"
set key outside right
set grid
TMF_TYPE="tsload"
TMF_X_AXIS="syncround"
set term canvas enhanced size 1620,768 name "POSTCanvasPlotter" mousing rounded jsdir "/content/js"
set output './plotter.js'
plot "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-UNKNOWN.dat" using 1:2:3:4 with vector lc rgb "#CC00FF" lw 2 ti "UNKNOWN", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_application.dat" using 1:2:3:4 with vector lc rgb "#FF0000" lw 2 ti "ts_application", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_bitadapt.dat" using 1:2:3:4 with vector lc rgb "#00FF00" lw 2 ti "ts_bitadapt", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_fileparser.dat" using 1:2:3:4 with vector lc rgb "#990066" lw 2 ti "ts_fileparser", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_mvb0.dat" using 1:2:3:4 with vector lc rgb "#0000FF" lw 2 ti "ts_mvb0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_v24r_0.dat" using 1:2:3:4 with vector lc rgb "#FF3366" lw 2 ti "ts_v24r_0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_xfer0.dat" using 1:2:3:4 with vector lc rgb "#00FF66" lw 2 ti "ts_xfer0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_xferba0.dat" using 1:2:3:4 with vector lc rgb "#33FFFF" lw 2 ti "ts_xferba0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/tsload/config-ts_xfermvb.dat" using 1:2:3:4 with vector lc rgb "#000000" lw 2 ti "ts_xfermvb"
