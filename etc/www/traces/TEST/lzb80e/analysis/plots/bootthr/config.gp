TMF_TYPE="bootthr"
set title "Thread Boot (lzb80e/cstrace, host evc_s1_0)"
set ylabel "Thread"
set xtic rotate
set grid
set yrange [-1:33]
set xlabel "Time"
set format x "%.9f"
set mouse format "%.9f"
TMF_X_AXIS="timestamp"
plot "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/bootthr/config.dat" using 2:3:4:5:yticlabel(1) with vectors lc 1 lw 2 ti "Thread Boot Time"
