set title "Voter Trace (lzb80e)"
set xlabel "time"
set xtics 1
set ytics 1
set xtic rotate
set grid
set linetype  1 lc rgb "black" lw 1
set linetype  2 lc rgb "red" lw 1
set linetype  3 lc rgb "green" lw 1
set linetype  4 lc rgb "blue" lw 1
set linetype  5 lc rgb "orange" lw 1
set linetype  6 lc rgb "cyan" lw 1
set linetype  7 lc rgb "magenta" lw 1
set linetype  8 lc rgb "dark-violet" lw 1
set linetype  9 lc rgb "sienna4"  lw 1
set linetype  10 lc rgb "gray60"  lw 1
set linetype  11 lc rgb "dark-green"  lw 1
set linetype  12 lc rgb "gray30"  lw 1
set linetype  13 lc rgb "light-red"  lw 1
set linetype  14 lc rgb "brown"  lw 1
set linetype  15 lc rgb "purple"  lw 1
set linetype  16 lc rgb "royalblue"  lw 1
set linetype  17 lc rgb "greenyellow"  lw 1
set linetype  18 lc rgb "gray90"  lw 1
set key outside right
TMF_TYPE="voter"
TMF_X_AXIS="syncround"
set mouse format "%.9f"
set yrange [0:4]
plot "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:CREDIT" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:CREDIT", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:OPEN_REPLY" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:OPEN_REPLY", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:OPEN_REQUEST" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:OPEN_REQUEST", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:SAC73" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:SAC73", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:TASKSET_BOOT" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:TASKSET_BOOT", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-CS:TASKSET_REC_START" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "CS:TASKSET_REC_START", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-EXTRN" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "EXTRN", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FM_NOTE" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FM_NOTE", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FM_STATUS" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FM_STATUS", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_CN_STATE_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_CN_STATE_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_CQ_QUAL_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_CQ_QUAL_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_OLT_CHECK_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_OLT_CHECK_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_PM_SAC_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_PM_SAC_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_SL_DIAG_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_SL_DIAG_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_TS_INFO_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_TS_INFO_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:FM_TS_REC_INFO_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:FM_TS_REC_INFO_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:RM_REC_CTL_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:RM_REC_CTL_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:RM_TS_REC_DATA_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:RM_TS_REC_DATA_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:RM_TS_REC_STATUS_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:RM_TS_REC_STATUS_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:RM_VO_REC_LAST_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:RM_VO_REC_LAST_MSG", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/voter/config.dat-FT:RM_VO_REC_MSG" using 1:2:3:yticlabel(4) with points lt 7 lc variable ti "FT:RM_VO_REC_MSG"
