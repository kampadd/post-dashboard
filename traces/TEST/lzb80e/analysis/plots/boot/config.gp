set title "Taskset Boot (lzb80e/cstrace, host evc_s1_0)"
set ylabel "Taskset"
set xtic rotate
set grid
set yrange [-1:9]
TMF_TYPE="boot"
set xlabel "Time"
set format x "%.9f"
set mouse format "%.9f"
TMF_X_AXIS="timestamp"
plot "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/boot/config.dat" using 1:2:3:4:yticlabel(5) with vectors lc 1 lw 2 ti "Boot Time", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/boot/config.dat2" using 1:2:3:4 with vectors nohead lc 0 notitle, "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/boot/config.dat3" using 1:2:yticlabel(3) with point lc 2 lt 7 ti "Thread created"
