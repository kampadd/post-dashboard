set title "Taskset Boot (demo/cstrace, host SCC-1)"
set ylabel "Taskset"
set xtic rotate
set grid
set yrange [-1:8]
TMF_TYPE="boot"
set xlabel "Time"
set format x "%.9f"
set mouse format "%.9f"
TMF_X_AXIS="timestamp"

set terminal svg size 1024,768 dynamic mouse rounded jsdir "content/js" name "Boot"
set output "boot.svg"

plot "boot/config.dat" using 1:2:3:4:yticlabel(5) with vectors lc 1 lw 2 ti "Boot Time", "boot/config.dat2" using 1:2:3:4 with vectors nohead lc 0 notitle, "boot/config.dat3" using 1:2:yticlabel(3) with point lc 2 lt 7 ti "Thread created"
