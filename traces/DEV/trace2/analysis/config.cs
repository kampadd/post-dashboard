    .CE_QUALITY
        .ce_quality
            .nr_allowed_recoveries = 3
            .observation_time
                .hours = 50
    .THIS_NODE
        .CN_EC
    .QUEUE
        ./__@@_main@CN_EC_ifbdwndl2@CN_EC_credits_1
            .id = 83
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ifbdwndl2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_ifbdwndl1@CN_EC_credits_1
            .id = 82
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ifbdwndl1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_ifbdwndl0@CN_EC_credits_1
            .id = 81
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ifbdwndl0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_diag2@CN_EC_credits_2
            .id = 80
            .taskset
                .sender = "main@CN_EC"
                .receiver = "diag2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_diag1@CN_EC_credits_2
            .id = 79
            .taskset
                .sender = "main@CN_EC"
                .receiver = "diag1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_diag0@CN_EC_credits_2
            .id = 78
            .taskset
                .sender = "main@CN_EC"
                .receiver = "diag0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_thacandl2@CN_EC_credits_3
            .id = 77
            .taskset
                .sender = "main@CN_EC"
                .receiver = "thacandl2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_thacandl1@CN_EC_credits_3
            .id = 76
            .taskset
                .sender = "main@CN_EC"
                .receiver = "thacandl1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_thacandl0@CN_EC_credits_3
            .id = 75
            .taskset
                .sender = "main@CN_EC"
                .receiver = "thacandl0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_samdl2@CN_EC_credits_4
            .id = 74
            .taskset
                .sender = "main@CN_EC"
                .receiver = "samdl2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_samdl1@CN_EC_credits_4
            .id = 73
            .taskset
                .sender = "main@CN_EC"
                .receiver = "samdl1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_samdl0@CN_EC_credits_4
            .id = 72
            .taskset
                .sender = "main@CN_EC"
                .receiver = "samdl0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_candl2@CN_EC_credits_5
            .id = 71
            .taskset
                .sender = "main@CN_EC"
                .receiver = "candl2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_candl1@CN_EC_credits_5
            .id = 70
            .taskset
                .sender = "main@CN_EC"
                .receiver = "candl1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_candl0@CN_EC_credits_5
            .id = 69
            .taskset
                .sender = "main@CN_EC"
                .receiver = "candl0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_xfer@CN_EC_main@CN_EC_credits_6
            .id = 68
            .taskset
                .sender = "xfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_xfer@CN_EC_main@CN_EC_credits_7
            .id = 67
            .taskset
                .sender = "xfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_/MQ_CPU_open
            .id = 66
            .taskset
                .sender = "main@CN_EC "
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 1.000000000
        ./__@@_main@CN_EC_sdm2@CN_EC_credits_10
            .id = 65
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_sdm1@CN_EC_credits_10
            .id = 64
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_sdm0@CN_EC_credits_10
            .id = 63
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_sdm2@CN_EC_credits_11
            .id = 62
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_sdm1@CN_EC_credits_11
            .id = 61
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_sdm0@CN_EC_credits_11
            .id = 60
            .taskset
                .sender = "main@CN_EC"
                .receiver = "sdm0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_xfer@CN_EC_main@CN_EC_credits_13
            .id = 59
            .taskset
                .sender = "xfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_xfer@CN_EC_main@CN_EC_credits_14
            .id = 58
            .taskset
                .sender = "xfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.200000000
        ./__@@_main@CN_EC_ocs_dl2@CN_EC_credits_21
            .id = 57
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ocs_dl2@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.500000000
        ./__@@_main@CN_EC_ocs_dl1@CN_EC_credits_21
            .id = 56
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ocs_dl1@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.500000000
        ./__@@_main@CN_EC_ocs_dl0@CN_EC_credits_21
            .id = 55
            .taskset
                .sender = "main@CN_EC"
                .receiver = "ocs_dl0@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.500000000
        ./__@@_ocs_dlxfer@CN_EC_main@CN_EC_credits_22
            .id = 54
            .taskset
                .sender = "ocs_dlxfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.500000000
        ./__@@_ocs_dlxfer@CN_EC_main@CN_EC_credits_23
            .id = 53
            .taskset
                .sender = "ocs_dlxfer@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 0.500000000
        ./__@@_diag2@CN_EC_queue
            .id = 52
            .taskset
                .sender = "diag2@CN_EC"
                .receiver = "diag2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_diag1@CN_EC_queue
            .id = 51
            .taskset
                .sender = "diag1@CN_EC"
                .receiver = "diag1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_diag0@CN_EC_queue
            .id = 50
            .taskset
                .sender = "diag0@CN_EC"
                .receiver = "diag0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_samdl2@CN_EC_queue
            .id = 49
            .taskset
                .sender = "samdl2@CN_EC"
                .receiver = "samdl2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_samdl1@CN_EC_queue
            .id = 48
            .taskset
                .sender = "samdl1@CN_EC"
                .receiver = "samdl1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_samdl0@CN_EC_queue
            .id = 47
            .taskset
                .sender = "samdl0@CN_EC"
                .receiver = "samdl0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_candl2@CN_EC_queue
            .id = 46
            .taskset
                .sender = "candl2@CN_EC"
                .receiver = "candl2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_candl1@CN_EC_queue
            .id = 45
            .taskset
                .sender = "candl1@CN_EC"
                .receiver = "candl1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_candl0@CN_EC_queue
            .id = 44
            .taskset
                .sender = "candl0@CN_EC"
                .receiver = "candl0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_xfer@CN_EC_queue
            .id = 43
            .taskset
                .sender = "xfer@CN_EC"
                .receiver = "xfer@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 120.000000000
        ./__@@_sdm2@CN_EC_queue
            .id = 42
            .taskset
                .sender = "sdm2@CN_EC"
                .receiver = "sdm2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_sdm1@CN_EC_queue
            .id = 41
            .taskset
                .sender = "sdm1@CN_EC"
                .receiver = "sdm1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_sdm0@CN_EC_queue
            .id = 40
            .taskset
                .sender = "sdm0@CN_EC"
                .receiver = "sdm0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ifbdwndl2@CN_EC_queue
            .id = 39
            .taskset
                .sender = "ifbdwndl2@CN_EC"
                .receiver = "ifbdwndl2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ifbdwndl1@CN_EC_queue
            .id = 38
            .taskset
                .sender = "ifbdwndl1@CN_EC"
                .receiver = "ifbdwndl1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ifbdwndl0@CN_EC_queue
            .id = 37
            .taskset
                .sender = "ifbdwndl0@CN_EC"
                .receiver = "ifbdwndl0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_thacandl2@CN_EC_queue
            .id = 36
            .taskset
                .sender = "thacandl2@CN_EC"
                .receiver = "thacandl2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_thacandl1@CN_EC_queue
            .id = 35
            .taskset
                .sender = "thacandl1@CN_EC"
                .receiver = "thacandl1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_thacandl0@CN_EC_queue
            .id = 34
            .taskset
                .sender = "thacandl0@CN_EC"
                .receiver = "thacandl0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_main@CN_EC_queue
            .id = 33
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 120.000000000
        ./__@@_ocs_dl2@CN_EC_queue
            .id = 32
            .taskset
                .sender = "ocs_dl2@CN_EC"
                .receiver = "ocs_dl2@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ocs_dl1@CN_EC_queue
            .id = 31
            .taskset
                .sender = "ocs_dl1@CN_EC"
                .receiver = "ocs_dl1@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ocs_dl0@CN_EC_queue
            .id = 30
            .taskset
                .sender = "ocs_dl0@CN_EC"
                .receiver = "ocs_dl0@CN_EC"
            .vmode = "vm_1o1_lc"
            .msg_wait = 120.000000000
        ./__@@_ocs_dlxfer@CN_EC_queue
            .id = 29
            .taskset
                .sender = "ocs_dlxfer@CN_EC"
                .receiver = "ocs_dlxfer@CN_EC"
            .vmode = "vm_2o3_lc"
            .msg_wait = 120.000000000
        ./MQ_TEST_SW
            .id = 0
            .msg_wait = 2.000000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_IFB_DWL2OEC
            .id = 1
            .msg_wait = 1.000000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "ifbdwndl0@CN_EC ifbdwndl1@CN_EC ifbdwndl2@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_DM_OEC
            .id = 2
            .msg_wait = 1.000000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "diag0@CN_EC diag1@CN_EC diag2@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_THACAN_TOUCAN
            .id = 3
            .msg_wait = 1.000000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "thacandl0@CN_EC thacandl1@CN_EC thacandl2@CN_EC"
                .receiver = "main@CN_EC"
        .Q_SAM2APPL
            .id = 4
            .msg_wait = 0.200000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "samdl0@CN_EC samdl1@CN_EC samdl2@CN_EC"
                .receiver = "main@CN_EC"
        .Q_CAN_DL_DIST
            .id = 5
            .msg_wait = 0.200000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "candl0@CN_EC candl1@CN_EC candl2@CN_EC"
                .receiver = "main@CN_EC"
        .Q_APPL2XFER
            .id = 6
            .msg_wait = 0.200000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "xfer@CN_EC"
        .Q_CAN_EXT_B
            .id = 7
            .msg_wait = 0.200000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "xfer@CN_EC"
        ./MQ_CPU
            .id = 8
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_lc_concat"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./datacheck
            .id = 9
            .msg_wait = 0.200000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_SDM_EC_MON
            .id = 10
            .msg_wait = 0.200000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "sdm0@CN_EC sdm1@CN_EC sdm2@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_SDM_EC
            .id = 11
            .msg_wait = 0.200000000
            .vmode = "vm_1o1_lc"
            .taskset
                .sender = "sdm0@CN_EC sdm1@CN_EC sdm2@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_EC_SDM
            .id = 12
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_TOUCAN_XFER
            .id = 13
            .msg_wait = 0.200000000
            .vmode = "vm_2o3_lc"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "xfer@CN_EC"
        ./MQ_TOUCAN_XFER_SAFE
            .id = 14
            .msg_wait = 0.200000000
            .chksum_len = 4
            .vmode = "vm_2o3_lc_ocs"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "xfer@CN_EC"
        ./MQ_TOUCAN_OEC
            .id = 15
            .msg_wait = 0.200000000
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_OEC_TOUCAN
            .id = 16
            .msg_wait = 0.200000000
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_EC_IM
            .id = 17
            .msg_wait = 0.200000000
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_IM_EC
            .id = 18
            .msg_wait = 0.200000000
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_DATA_CHECK
            .id = 19
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./MQ_OLT_CHECK
            .id = 20
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_ec"
            .taskset
                .sender = "main@CN_EC"
                .receiver = "main@CN_EC"
        ./ocs_dl2pl_A
            .id = 21
            .msg_wait = 0.500000000
            .vmode = "vm_1o1_lc"
            .taskset
                .receiver = "main@CN_EC"
                .sender = "ocs_dl0@CN_EC ocs_dl1@CN_EC ocs_dl2@CN_EC"
        ./ocs_ctrl2dl_A
            .id = 22
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_lc"
            .taskset
                .receiver = "ocs_dlxfer@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_dilo2dl_A
            .id = 23
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_lc_ocs"
            .chksum_len = 8
            .taskset
                .receiver = "ocs_dlxfer@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_pl2dilo_A
            .id = 24
            .msg_wait = 0.200000000
            .taskset
                .receiver = "main@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_pl_si_A
            .id = 25
            .msg_wait = 0.500000000
            .vmode = "vm_2o3_lc"
            .taskset
                .receiver = "main@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_ctrl_mq_A
            .id = 26
            .msg_wait = 0.200000000
            .taskset
                .receiver = "main@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_rcv_mq_A
            .id = 27
            .msg_wait = 0.200000000
            .taskset
                .receiver = "main@CN_EC"
                .sender = "main@CN_EC"
        ./ocs_send_mq_A
            .id = 28
            .msg_wait = 0.200000000
            .taskset
                .receiver = "main@CN_EC"
                .sender = "main@CN_EC"
    .TASKSET
        .diag2@CN_EC
            .id = 0
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "DIAG2"
            .ce_location = "2"
        .diag1@CN_EC
            .id = 1
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "DIAG1"
            .ce_location = "1"
        .diag0@CN_EC
            .id = 2
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "DIAG0"
            .ce_location = "0"
        .samdl2@CN_EC
            .id = 3
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SAMDL2"
            .ce_location = "2"
        .samdl1@CN_EC
            .id = 4
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SAMDL1"
            .ce_location = "1"
        .samdl0@CN_EC
            .id = 5
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SAMDL0"
            .ce_location = "0"
        .candl2@CN_EC
            .id = 6
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "CANDL2"
            .ce_location = "2"
        .candl1@CN_EC
            .id = 7
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "CANDL1"
            .ce_location = "1"
        .candl0@CN_EC
            .id = 8
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "CANDL0"
            .ce_location = "0"
        .xfer@CN_EC
            .id = 9
            .sched_policy = "sched_run2compl"
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 2
            .recovery = 1
            .ts_boot_wd = 180.000000000
            .ts_wd = 2.000000000
            .max_red = 3
            .min_red = 2
            .ft_boot_wait = 120.000000000
            .process = "XFER"
            .ce_location = "0 1 2"
        .sdm2@CN_EC
            .id = 10
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SDM2"
            .ce_location = "2"
        .sdm1@CN_EC
            .id = 11
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SDM1"
            .ce_location = "1"
        .sdm0@CN_EC
            .id = 12
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "SDM0"
            .ce_location = "0"
        .ifbdwndl2@CN_EC
            .id = 13
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "IFBDWL_2"
            .ce_location = "2"
        .ifbdwndl1@CN_EC
            .id = 14
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "IFBDWL_1"
            .ce_location = "1"
        .ifbdwndl0@CN_EC
            .id = 15
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "IFBDWL_0"
            .ce_location = "0"
        .thacandl2@CN_EC
            .id = 16
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "THACANDL_2"
            .ce_location = "2"
        .thacandl1@CN_EC
            .id = 17
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "THACANDL_1"
            .ce_location = "1"
        .thacandl0@CN_EC
            .id = 18
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .recovery = 0
            .sched_policy = "sched_posix"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "THACANDL_0"
            .ce_location = "0"
        .main@CN_EC
            .id = 19
            .sched_policy = "sched_run2compl"
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 2
            .recovery = 1
            .ts_boot_wd = 180.000000000
            .ts_wd = 0.000000000
            .max_red = 3
            .min_red = 2
            .ft_boot_wait = 120.000000000
            .process = "EC905 ECOCS QSM TOUCAN pl dilo_md4S"
            .ce_location = "0 1 2"
        .ocs_dl2@CN_EC
            .id = 20
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .sched_policy = "sched_posix"
            .recovery = 0
            .ce_location = "2"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "dl2"
        .ocs_dl1@CN_EC
            .id = 21
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .sched_policy = "sched_posix"
            .recovery = 0
            .ce_location = "1"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "dl1"
        .ocs_dl0@CN_EC
            .id = 22
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 0
            .sched_policy = "sched_posix"
            .recovery = 0
            .ce_location = "0"
            .max_red = 1
            .min_red = 1
            .ft_boot_wait = 120.000000000
            .process = "dl0"
        .ocs_dlxfer@CN_EC
            .id = 23
            .sched_policy = "sched_run2compl"
            .max_trans_fault_rate = 1
            .max_perm_fault_count = 1
            .notextcrc = 0
            .priority = 40
            .rec_policy = 2
            .recovery = 1
            .ce_location = "0 1 2"
            .max_red = 3
            .min_red = 2
            .ft_boot_wait = 120.000000000
            .process = "dlxfer"
    .OCS_DEVICE
        .IM_dev_1_D_server@CN_IM
            .id = 0
            .nodename = "10.220.83.204"
            .type = "udp"
            .ce_location = 1
        .IM_dev_0_C_server@CN_IM
            .id = 1
            .nodename = "10.220.83.204"
            .type = "udp"
            .ce_location = 0
        .EC_dev_1@CN_EC
            .id = 2
            .nodename = "192.168.17.201"
            .type = "udp"
            .ce_location = 1
        .EC_dev_0@CN_EC
            .id = 3
            .nodename = "192.168.16.200"
            .type = "udp"
            .ce_location = 0
    .OCS_CONNECT
        .s2_IM
            .id = 0
            .port = 8001
            .endpoint2 = "IM_dev_1_D_server@CN_IM"
            .endpoint1 = "EC_dev_1@CN_EC"
        .s1_IM
            .id = 1
            .port = 7001
            .endpoint2 = "IM_dev_0_C_server@CN_IM"
            .endpoint1 = "EC_dev_0@CN_EC"
    .OCS_VCHAN
        .IM
            .id = 0
            .secondary_send_time = 10000
            .secondary_recv_timeout = 12000
            .check_code_type = "NONE"
            .ipv6_tclass = 184
            .channel_type = "MASTER"
            .remote_auth = 2
            .local_auth = 1
            .measure_wnd = 5000
            .max_link_delay = 1000
            .msg_miss_thrs = 1
            .check_code_err_thrs = 50
            .diag_queue_size = 11
            .defer_queue_size = 10
            .defer_to = 200
            .init_to = 6000
            .seq_err_thrs = 50
            .semantic_err_thrs = 1
            .auth_err_thrs = 1
            .safety_code_err_thrs = 50
            .rx_wnd_low = 50
            .rx_wnd_max = 60
            .max_msg_rtd = 3000
            .retrans_disable = 0
            .retry_connection = 0
            .wait_for_dl_ack = 1000
            .reopen_to = 1000
            .switch_to = 1000
            .connect_to = 2500
            .primary_recv_timeout = 2000
            .primary_send_time = 500
            .subchan_names = "s1_IM s2_IM"
            .retransmission = 100
            .redundancy = "HOTHOT"
            .protocol_spec = "SAHARA"
            .msg_max = 400
            .msg_size = 150
    .OCS_PROTOCOL
        .SAHARA
            .id = 0
            .dilo2dl_mq = "/ocs_dilo2dl_A"
            .pl2dilo_mq = "/ocs_pl2dilo_A"
            .dilo = "dilo_md4S"
            .safety = "MD4S"
            .protocol = "SAHARA"
    .OCS_GLOBAL
        .TAS_OCS
            .id = 0
            .dl2pl_mq_prio = 0
            .down2dl_mq_prio = 0
            .mode = "RECOVERY"
            .pl_si_mq = "/ocs_pl_si_A"
            .ctrl2dl_mq = "/ocs_ctrl2dl_A"
            .dl2pl_mq = "/ocs_dl2pl_A"
            .api_ctrl_mq = "/ocs_ctrl_mq_A"
            .api_recv_mq = "/ocs_rcv_mq_A"
            .api_send_mq = "/ocs_send_mq_A"
            .device_layer = "dl"
            .protocol_layer = "pl"
    .NODE
        .CN_IM
            .id = 0
            .max_red = 3
            .min_red = 2
            .ft_rec_wait = 480.000000000
            .ft_boot_wait = 30.000000000
        .CN_EC
            .id = 1
            .max_red = 3
            .min_red = 2
            .ft_rec_wait = 480.000000000
            .ft_boot_wait = 30.000000000
    .TS_RECOVERY
        .ts_recovery
            .ft_rec_wait = 120.000000000
    .SL_LM
        .sl_lm
            .max_perm_fault = 15
            .max_trans_fault = 10
            .max_cmsg_len = 16384
            .link
                .order = "1 2"
                .max_delay = 0.005000000
                .min_delay = 0.000000000
            .global
                .chan2link = "0 1"
    .SL_SP
        .sl_sp
            .max_period = 0.050000000
            .min_period = 0.045000000
            .boot_period = 0.600000000
    .SL_PM
        .sl_pm
            .adjtime = 0
            .nr_link = 2
            .nr_channel = 2
            .link_speed = 100
    .SL_NI
        .sl_ni
            .address = "192.168.1.1 192.168.3.1 192.168.2.1 192.168.3.2 192.168.2.2 192.168.1.2"
            .repl_id = "0 1 2 0 1 2"
            .link_id = "0 0 0 1 1 1"
            .port = 2001
            .flowctl = 1
    .SL_RI
        .sl_ri
            .uname
                .nodename = "SCC-1 SCC-2 SCC-3"
            .select = "uname"
    .SL_IB
        .sl_ib
            .rec_bandwidth = 200
    .SL_AI
        .sl_ai
            .flowctl = 0
    .FT_OM
        .ft_om
            .lab_mode = "no"
            .arm
                .nr_trace = 0
                .file = "/tmp/arm.trc"
            .rm
                .ev
                    .nr_trace = 0
                    .file = "/tmp/rm_ev.trc"
                .ra
                    .nr_trace = 0
                    .file = "/tmp/rm_ra.trc"
            .fm
                .state
                    .nr_trace = 0
                    .file = "/tmp/fm_state.trc"
                .ts
                    .nr_trace = 0
                    .file = "/tmp/fm_ts.trc"
                .ev
                    .nr_trace = 0
                    .file = "/tmp/fm_ev.trc"
                .ce
                    .nr_trace = 0
                    .file = "/tmp/fm_ce.trc"
            .vo
                .nr_trace = 0
                .file = "/tmp/vo.trc"
            .sl
                .flowctl = 0
                .nr_trace = 0
                .file = "/tmp/sl.trc"
    .RM
        .rm
            .rec_msg_wait = 5.000000000
    .SL_XP
        .sl_xp
            .lateness = 0.000000000
            .fatal_lateness = 0.000000000
    .CN_QUALITY
        .cn_quality
            .nr_allowed_reboots = 5
