/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceComponent } from '../../../../../../main/webapp/app/entities/trace/trace.component';
import { TraceService } from '../../../../../../main/webapp/app/entities/trace/trace.service';
import { Trace } from '../../../../../../main/webapp/app/entities/trace/trace.model';

describe('Component Tests', () => {

    describe('Trace Management Component', () => {
        let comp: TraceComponent;
        let fixture: ComponentFixture<TraceComponent>;
        let service: TraceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceComponent],
                providers: [
                    TraceService
                ]
            })
            .overrideTemplate(TraceComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Trace('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.traces[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
