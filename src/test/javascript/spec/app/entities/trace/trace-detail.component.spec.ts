/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceDetailComponent } from '../../../../../../main/webapp/app/entities/trace/trace-detail.component';
import { TraceService } from '../../../../../../main/webapp/app/entities/trace/trace.service';
import { Trace } from '../../../../../../main/webapp/app/entities/trace/trace.model';

describe('Component Tests', () => {

    describe('Trace Management Detail Component', () => {
        let comp: TraceDetailComponent;
        let fixture: ComponentFixture<TraceDetailComponent>;
        let service: TraceService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceDetailComponent],
                providers: [
                    TraceService
                ]
            })
            .overrideTemplate(TraceDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Trace('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.trace).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
