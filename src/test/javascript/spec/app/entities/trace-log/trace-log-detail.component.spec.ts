/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceLogDetailComponent } from '../../../../../../main/webapp/app/entities/trace-log/trace-log-detail.component';
import { TraceLogService } from '../../../../../../main/webapp/app/entities/trace-log/trace-log.service';
import { TraceLog } from '../../../../../../main/webapp/app/entities/trace-log/trace-log.model';

describe('Component Tests', () => {

    describe('TraceLog Management Detail Component', () => {
        let comp: TraceLogDetailComponent;
        let fixture: ComponentFixture<TraceLogDetailComponent>;
        let service: TraceLogService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceLogDetailComponent],
                providers: [
                    TraceLogService
                ]
            })
            .overrideTemplate(TraceLogDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceLogDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceLogService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TraceLog('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.traceLog).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
