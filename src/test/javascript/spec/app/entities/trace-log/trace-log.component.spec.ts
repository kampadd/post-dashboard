/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceLogComponent } from '../../../../../../main/webapp/app/entities/trace-log/trace-log.component';
import { TraceLogService } from '../../../../../../main/webapp/app/entities/trace-log/trace-log.service';
import { TraceLog } from '../../../../../../main/webapp/app/entities/trace-log/trace-log.model';

describe('Component Tests', () => {

    describe('TraceLog Management Component', () => {
        let comp: TraceLogComponent;
        let fixture: ComponentFixture<TraceLogComponent>;
        let service: TraceLogService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceLogComponent],
                providers: [
                    TraceLogService
                ]
            })
            .overrideTemplate(TraceLogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceLogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceLogService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TraceLog('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.traceLogs[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
