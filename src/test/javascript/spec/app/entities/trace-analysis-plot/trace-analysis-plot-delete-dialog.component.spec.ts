/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisPlotDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot-delete-dialog.component';
import { TraceAnalysisPlotService } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.service';

describe('Component Tests', () => {

    describe('TraceAnalysisPlot Management Delete Component', () => {
        let comp: TraceAnalysisPlotDeleteDialogComponent;
        let fixture: ComponentFixture<TraceAnalysisPlotDeleteDialogComponent>;
        let service: TraceAnalysisPlotService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisPlotDeleteDialogComponent],
                providers: [
                    TraceAnalysisPlotService
                ]
            })
            .overrideTemplate(TraceAnalysisPlotDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisPlotDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisPlotService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
