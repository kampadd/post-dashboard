/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisPlotDetailComponent } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot-detail.component';
import { TraceAnalysisPlotService } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.service';
import { TraceAnalysisPlot } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.model';

describe('Component Tests', () => {

    describe('TraceAnalysisPlot Management Detail Component', () => {
        let comp: TraceAnalysisPlotDetailComponent;
        let fixture: ComponentFixture<TraceAnalysisPlotDetailComponent>;
        let service: TraceAnalysisPlotService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisPlotDetailComponent],
                providers: [
                    TraceAnalysisPlotService
                ]
            })
            .overrideTemplate(TraceAnalysisPlotDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisPlotDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisPlotService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TraceAnalysisPlot('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.traceAnalysisPlot).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
