/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisPlotComponent } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.component';
import { TraceAnalysisPlotService } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.service';
import { TraceAnalysisPlot } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.model';

describe('Component Tests', () => {

    describe('TraceAnalysisPlot Management Component', () => {
        let comp: TraceAnalysisPlotComponent;
        let fixture: ComponentFixture<TraceAnalysisPlotComponent>;
        let service: TraceAnalysisPlotService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisPlotComponent],
                providers: [
                    TraceAnalysisPlotService
                ]
            })
            .overrideTemplate(TraceAnalysisPlotComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisPlotComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisPlotService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TraceAnalysisPlot('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.traceAnalysisPlots[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
