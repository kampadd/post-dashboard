/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisPlotDialogComponent } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot-dialog.component';
import { TraceAnalysisPlotService } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.service';
import { TraceAnalysisPlot } from '../../../../../../main/webapp/app/entities/trace-analysis-plot/trace-analysis-plot.model';

describe('Component Tests', () => {

    describe('TraceAnalysisPlot Management Dialog Component', () => {
        let comp: TraceAnalysisPlotDialogComponent;
        let fixture: ComponentFixture<TraceAnalysisPlotDialogComponent>;
        let service: TraceAnalysisPlotService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisPlotDialogComponent],
                providers: [
                    TraceAnalysisPlotService
                ]
            })
            .overrideTemplate(TraceAnalysisPlotDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisPlotDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisPlotService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TraceAnalysisPlot('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.traceAnalysisPlot = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'traceAnalysisPlotListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new TraceAnalysisPlot();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.traceAnalysisPlot = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'traceAnalysisPlotListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
