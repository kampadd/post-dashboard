/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisDetailComponent } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis-detail.component';
import { TraceAnalysisService } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis.service';
import { TraceAnalysis } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis.model';

describe('Component Tests', () => {

    describe('TraceAnalysis Management Detail Component', () => {
        let comp: TraceAnalysisDetailComponent;
        let fixture: ComponentFixture<TraceAnalysisDetailComponent>;
        let service: TraceAnalysisService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisDetailComponent],
                providers: [
                    TraceAnalysisService
                ]
            })
            .overrideTemplate(TraceAnalysisDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new TraceAnalysis('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.traceAnalysis).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
