/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PostDashboardTestModule } from '../../../test.module';
import { TraceAnalysisComponent } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis.component';
import { TraceAnalysisService } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis.service';
import { TraceAnalysis } from '../../../../../../main/webapp/app/entities/trace-analysis/trace-analysis.model';

describe('Component Tests', () => {

    describe('TraceAnalysis Management Component', () => {
        let comp: TraceAnalysisComponent;
        let fixture: ComponentFixture<TraceAnalysisComponent>;
        let service: TraceAnalysisService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [TraceAnalysisComponent],
                providers: [
                    TraceAnalysisService
                ]
            })
            .overrideTemplate(TraceAnalysisComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TraceAnalysisComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TraceAnalysisService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new TraceAnalysis('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.traceAnalyses[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
