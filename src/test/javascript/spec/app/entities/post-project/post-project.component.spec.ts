/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PostDashboardTestModule } from '../../../test.module';
import { PostProjectComponent } from '../../../../../../main/webapp/app/entities/post-project/post-project.component';
import { PostProjectService } from '../../../../../../main/webapp/app/entities/post-project/post-project.service';
import { PostProject } from '../../../../../../main/webapp/app/entities/post-project/post-project.model';

describe('Component Tests', () => {

    describe('PostProject Management Component', () => {
        let comp: PostProjectComponent;
        let fixture: ComponentFixture<PostProjectComponent>;
        let service: PostProjectService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [PostProjectComponent],
                providers: [
                    PostProjectService
                ]
            })
            .overrideTemplate(PostProjectComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PostProjectComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PostProjectService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PostProject('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.postProjects[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
