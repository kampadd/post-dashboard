/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { PostDashboardTestModule } from '../../../test.module';
import { PostProjectDetailComponent } from '../../../../../../main/webapp/app/entities/post-project/post-project-detail.component';
import { PostProjectService } from '../../../../../../main/webapp/app/entities/post-project/post-project.service';
import { PostProject } from '../../../../../../main/webapp/app/entities/post-project/post-project.model';

describe('Component Tests', () => {

    describe('PostProject Management Detail Component', () => {
        let comp: PostProjectDetailComponent;
        let fixture: ComponentFixture<PostProjectDetailComponent>;
        let service: PostProjectService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PostDashboardTestModule],
                declarations: [PostProjectDetailComponent],
                providers: [
                    PostProjectService
                ]
            })
            .overrideTemplate(PostProjectDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PostProjectDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PostProjectService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PostProject('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.postProject).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
