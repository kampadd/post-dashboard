package at.thalesgroup.web.rest;

import at.thalesgroup.PostDashboardApp;

import at.thalesgroup.domain.PostTrace;
import at.thalesgroup.repository.PostTraceRepository;
import at.thalesgroup.service.PostTraceService;
import at.thalesgroup.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static at.thalesgroup.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TraceResource REST controller.
 *
 * @see PostTraceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PostDashboardApp.class)
public class PostTraceResourceIntTest {

    private static final String DEFAULT_TIMESTAMP = "AAAAAAAAAA";
    private static final String UPDATED_TIMESTAMP = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_FILE = "AAAAAAAAAA";
    private static final String UPDATED_FILE = "BBBBBBBBBB";

    @Autowired
    private PostTraceRepository traceRepository;

    @Autowired
    private PostTraceService traceService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTraceMockMvc;

    private PostTrace trace;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PostTraceResource traceResource = new PostTraceResource(traceService);
        this.restTraceMockMvc = MockMvcBuilders.standaloneSetup(traceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PostTrace createEntity() {
        PostTrace trace = new PostTrace()
            .timestamp(DEFAULT_TIMESTAMP)
            .name(DEFAULT_NAME)
            .file(DEFAULT_FILE);
        return trace;
    }

    @Before
    public void initTest() {
        traceRepository.deleteAll();
        trace = createEntity();
    }

    @Test
    public void createTrace() throws Exception {
        int databaseSizeBeforeCreate = traceRepository.findAll().size();

        // Create the Trace
        restTraceMockMvc.perform(post("/api/traces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trace)))
            .andExpect(status().isCreated());

        // Validate the Trace in the database
        List<PostTrace> traceList = traceRepository.findAll();
        assertThat(traceList).hasSize(databaseSizeBeforeCreate + 1);
        PostTrace testTrace = traceList.get(traceList.size() - 1);
        assertThat(testTrace.getTimestamp()).isEqualTo(DEFAULT_TIMESTAMP);
        assertThat(testTrace.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTrace.getFile()).isEqualTo(DEFAULT_FILE);
    }

    @Test
    public void createTraceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = traceRepository.findAll().size();

        // Create the Trace with an existing ID
        trace.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTraceMockMvc.perform(post("/api/traces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trace)))
            .andExpect(status().isBadRequest());

        // Validate the Trace in the database
        List<PostTrace> traceList = traceRepository.findAll();
        assertThat(traceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllTraces() throws Exception {
        // Initialize the database
        traceRepository.save(trace);

        // Get all the traceList
        restTraceMockMvc.perform(get("/api/traces?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(trace.getId())))
            .andExpect(jsonPath("$.[*].timestamp").value(hasItem(DEFAULT_TIMESTAMP.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].file").value(hasItem(DEFAULT_FILE.toString())));
    }

    @Test
    public void getTrace() throws Exception {
        // Initialize the database
        traceRepository.save(trace);

        // Get the trace
        restTraceMockMvc.perform(get("/api/traces/{id}", trace.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(trace.getId()))
            .andExpect(jsonPath("$.timestamp").value(DEFAULT_TIMESTAMP.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.file").value(DEFAULT_FILE.toString()));
    }

    @Test
    public void getNonExistingTrace() throws Exception {
        // Get the trace
        restTraceMockMvc.perform(get("/api/traces/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTrace() throws Exception {
        // Initialize the database
        traceService.save(trace);

        int databaseSizeBeforeUpdate = traceRepository.findAll().size();

        // Update the trace
        PostTrace updatedTrace = traceRepository.findOne(trace.getId());
        updatedTrace
            .timestamp(UPDATED_TIMESTAMP)
            .name(UPDATED_NAME)
            .file(UPDATED_FILE);

        restTraceMockMvc.perform(put("/api/traces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTrace)))
            .andExpect(status().isOk());

        // Validate the Trace in the database
        List<PostTrace> traceList = traceRepository.findAll();
        assertThat(traceList).hasSize(databaseSizeBeforeUpdate);
        PostTrace testTrace = traceList.get(traceList.size() - 1);
        assertThat(testTrace.getTimestamp()).isEqualTo(UPDATED_TIMESTAMP);
        assertThat(testTrace.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTrace.getFile()).isEqualTo(UPDATED_FILE);
    }

    @Test
    public void updateNonExistingTrace() throws Exception {
        int databaseSizeBeforeUpdate = traceRepository.findAll().size();

        // Create the Trace

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTraceMockMvc.perform(put("/api/traces")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(trace)))
            .andExpect(status().isCreated());

        // Validate the Trace in the database
        List<PostTrace> traceList = traceRepository.findAll();
        assertThat(traceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteTrace() throws Exception {
        // Initialize the database
        traceService.save(trace);

        int databaseSizeBeforeDelete = traceRepository.findAll().size();

        // Get the trace
        restTraceMockMvc.perform(delete("/api/traces/{id}", trace.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PostTrace> traceList = traceRepository.findAll();
        assertThat(traceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PostTrace.class);
        PostTrace trace1 = new PostTrace();
        trace1.setId("id1");
        PostTrace trace2 = new PostTrace();
        trace2.setId(trace1.getId());
        assertThat(trace1).isEqualTo(trace2);
        trace2.setId("id2");
        assertThat(trace1).isNotEqualTo(trace2);
        trace1.setId(null);
        assertThat(trace1).isNotEqualTo(trace2);
    }
}
