package at.thalesgroup.web.rest;

import at.thalesgroup.PostDashboardApp;

import at.thalesgroup.domain.PostProject;
import at.thalesgroup.domain.PostTrace;
import at.thalesgroup.repository.PostProjectRepository;
import at.thalesgroup.service.PostProjectService;
import at.thalesgroup.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static at.thalesgroup.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PostProjectResource REST controller.
 *
 * @see PostProjectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PostDashboardApp.class)
public class PostProjectResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final List<PostTrace> DEFAULT_POST_TRACES = new ArrayList<>();
    private static final List<PostTrace> UPDATED_POST_TRACES = new ArrayList<>();

    private static final Map<String, String> DEFAULT_METADATA = new HashMap<>();
    private static final Map<String, String> UPDATED_METADATA = new HashMap<>();

    @Autowired
    private PostProjectRepository postProjectRepository;

    @Autowired
    private PostProjectService postProjectService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPostProjectMockMvc;

    private PostProject postProject;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PostProjectResource postProjectResource = new PostProjectResource(postProjectService);
        this.restPostProjectMockMvc = MockMvcBuilders.standaloneSetup(postProjectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PostProject createEntity() {
        DEFAULT_POST_TRACES.add(new PostTrace().name("AAAAAAAAAA"));
        DEFAULT_METADATA.put("Status", "AAAAAAAA");
        PostProject postProject = new PostProject()
            .name(DEFAULT_NAME)
            .postTraces(DEFAULT_POST_TRACES)
            .metadata(DEFAULT_METADATA);
        return postProject;
    }

    @Before
    public void initTest() {
        postProjectRepository.deleteAll();
        postProject = createEntity();
    }

    @Test
    public void createPostProject() throws Exception {
        int databaseSizeBeforeCreate = postProjectRepository.findAll().size();

        // Create the PostProject
        restPostProjectMockMvc.perform(post("/api/post-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postProject)))
            .andExpect(status().isCreated());

        // Validate the PostProject in the database
        List<PostProject> postProjectList = postProjectRepository.findAll();
        assertThat(postProjectList).hasSize(databaseSizeBeforeCreate + 1);
        PostProject testPostProject = postProjectList.get(postProjectList.size() - 1);
        assertThat(testPostProject.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPostProject.getPostTraces()).isEqualTo(DEFAULT_POST_TRACES);
        assertThat(testPostProject.getMetadata()).isEqualTo(DEFAULT_METADATA);
    }

    @Test
    public void createPostProjectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = postProjectRepository.findAll().size();

        // Create the PostProject with an existing ID
        postProject.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restPostProjectMockMvc.perform(post("/api/post-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postProject)))
            .andExpect(status().isBadRequest());

        // Validate the PostProject in the database
        List<PostProject> postProjectList = postProjectRepository.findAll();
        assertThat(postProjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllPostProjects() throws Exception {
        // Initialize the database
        postProjectRepository.save(postProject);

        // Get all the postProjectList
        restPostProjectMockMvc.perform(get("/api/post-projects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(postProject.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].postTraces").value(hasItem(DEFAULT_POST_TRACES.toString())))
            .andExpect(jsonPath("$.[*].metadata").value(hasItem(DEFAULT_METADATA.toString())));
    }

    @Test
    public void getPostProject() throws Exception {
        // Initialize the database
        postProjectRepository.save(postProject);

        // Get the postProject
        restPostProjectMockMvc.perform(get("/api/post-projects/{id}", postProject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(postProject.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.postTraces").value(DEFAULT_POST_TRACES.toString()))
            .andExpect(jsonPath("$.metadata").value(DEFAULT_METADATA.toString()));
    }

    @Test
    public void getNonExistingPostProject() throws Exception {
        // Get the postProject
        restPostProjectMockMvc.perform(get("/api/post-projects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePostProject() throws Exception {
        // Initialize the database
        postProjectService.save(postProject);

        int databaseSizeBeforeUpdate = postProjectRepository.findAll().size();

        // Update the postProject
        PostProject updatedPostProject = postProjectRepository.findOne(postProject.getId());

        UPDATED_POST_TRACES.add(new PostTrace().name("BBBBBBBB"));
        UPDATED_METADATA.put("Status", "BBBBBB");

        updatedPostProject
            .name(UPDATED_NAME)
            .postTraces(UPDATED_POST_TRACES)
            .metadata(UPDATED_METADATA);

        restPostProjectMockMvc.perform(put("/api/post-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPostProject)))
            .andExpect(status().isOk());

        // Validate the PostProject in the database
        List<PostProject> postProjectList = postProjectRepository.findAll();
        assertThat(postProjectList).hasSize(databaseSizeBeforeUpdate);
        PostProject testPostProject = postProjectList.get(postProjectList.size() - 1);
        assertThat(testPostProject.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPostProject.getPostTraces()).isEqualTo(UPDATED_POST_TRACES);
        assertThat(testPostProject.getMetadata()).isEqualTo(UPDATED_METADATA);
    }

    @Test
    public void updateNonExistingPostProject() throws Exception {
        int databaseSizeBeforeUpdate = postProjectRepository.findAll().size();

        // Create the PostProject

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPostProjectMockMvc.perform(put("/api/post-projects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(postProject)))
            .andExpect(status().isCreated());

        // Validate the PostProject in the database
        List<PostProject> postProjectList = postProjectRepository.findAll();
        assertThat(postProjectList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePostProject() throws Exception {
        // Initialize the database
        postProjectService.save(postProject);

        int databaseSizeBeforeDelete = postProjectRepository.findAll().size();

        // Get the postProject
        restPostProjectMockMvc.perform(delete("/api/post-projects/{id}", postProject.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PostProject> postProjectList = postProjectRepository.findAll();
        assertThat(postProjectList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PostProject.class);
        PostProject postProject1 = new PostProject();
        postProject1.setId("id1");
        PostProject postProject2 = new PostProject();
        postProject2.setId(postProject1.getId());
        assertThat(postProject1).isEqualTo(postProject2);
        postProject2.setId("id2");
        assertThat(postProject1).isNotEqualTo(postProject2);
        postProject1.setId(null);
        assertThat(postProject1).isNotEqualTo(postProject2);
    }
}
