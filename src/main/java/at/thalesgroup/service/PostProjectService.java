package at.thalesgroup.service;

import at.thalesgroup.domain.PostFile;
import at.thalesgroup.domain.PostProject;
import at.thalesgroup.domain.PostTrace;
import at.thalesgroup.repository.PostProjectRepository;
import at.thalesgroup.service.util.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executors;

/**
 * Service Implementation for managing PostProject.
 */
@Service
public class PostProjectService {

    private final Logger log = LoggerFactory.getLogger(PostProjectService.class);

    private final PostProjectRepository postProjectRepository;
    private final PostTraceService postTraceService;
    private Properties configurationProperties = System.getProperties();
    private String TRACES_ROOT = "TRACES_ROOT";
    private final String TRACES_DIR = "traces";

    @Value("${file.location}")
    private String location;

    public PostProjectService(PostProjectRepository postProjectRepository, PostTraceService postTraceService) {
        this.postProjectRepository = postProjectRepository;
        this.postTraceService = postTraceService;
        this.TRACES_ROOT = configurationProperties.getProperty("TRACES_ROOT",
                Tools.getApplicationRoot().concat(TRACES_DIR).concat(Tools.getSeparator()));
        log.info("TRACES_ROOT: {}", this.TRACES_ROOT);
    }

    /**
     * Save a postProject.
     *
     * @param postProject the entity to save
     * @return the persisted entity
     */
    public PostProject save(PostProject postProject) {
        log.debug("Request to save PostProject : {}", postProject);
        return postProjectRepository.save(postProject);
    }

    /**
     * Upload a postProject
     */
    public PostProject upload(MultipartFile file, String project) throws IOException {
        String projectName = (project == null) ? file.getName() : project;
        String fileName = projectName.concat(".").concat(file.getContentType());
        String filePath = TRACES_ROOT.endsWith(Tools.getSeparator()) ?
                          TRACES_ROOT.concat(fileName) :
                          TRACES_ROOT.concat(Tools.getSeparator()).concat(fileName);
        File projectFile = new File(filePath);
        file.transferTo(projectFile);
        log.debug("Saved file to: {}", projectFile.getAbsolutePath());
        // create process commands
        List<String> commands = new ArrayList<>();
        commands.add("cd ".concat(TRACES_ROOT));
        commands.add("tar xzf ".concat(projectFile.getName()));
        // create process builder
        ProcessBuilder builder = new ProcessBuilder();
        // get process builder environment
        Map<String, String> environment = builder.environment();
        // set traces root directory
        environment.put("TRACES_ROOT", TRACES_ROOT);
        // switch to traces directory
        builder.directory(Tools.getDirectory(TRACES_ROOT));
        // set command
        builder.command(commands);
        try {
            builder.redirectErrorStream(true);
            Process process = builder.start();
            StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), log::info);
            Executors.newSingleThreadExecutor().submit(streamGobbler);
            int result = process.waitFor();
            log.debug("process result: {}", result);
            if(result > 0){
                return initProject(project);
            }
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
        return null;
    }

    /**
     * Get all the postProjects.
     *
     * @return the list of entities
     */
    public List<PostProject> findAll() {
        log.debug("Request to get all PostProjects");
        return postProjectRepository.findAll();
    }

    public List<PostProject> loadProjects(){
        List<PostProject> postProjects = new ArrayList<>();
        File projectRootDirectory = Tools.getDirectory(TRACES_ROOT);
        Set<File> projectsDirectories = Tools.getDirectories(projectRootDirectory);
        if(!projectsDirectories.isEmpty()){
            projectsDirectories.stream().forEach(project -> {
                PostProject postProject = new PostProject();
                postProject.setName(project.getName());
                postProject.setPostTraces(postTraceService.loadTraceRoot(project.getName()));
                postProjects.add(postProject);
                if(!postProjectRepository.findByName(project.getName()).isPresent()){
                    postProjectRepository.save(postProject);
                }
            });
        }
        return postProjects;
    }

    public List<PostFile> listFiles(){
        log.debug("post-files:");
        List<PostFile> postFiles = new ArrayList<>();
        File projectRootDirectory = Tools.getDirectory(TRACES_ROOT);
        Set<File> projectDirectories = Tools.getDirectories(projectRootDirectory);
        if(!projectDirectories.isEmpty()){
            log.debug("post-files: {}", projectDirectories);
            projectDirectories.stream().forEach(file -> {
                log.debug("file: {}", file);
                PostFile postFile = new PostFile();
                postFile.setId(String.valueOf(UUID.randomUUID()));
                postFile.setPath(file.getPath());
                postFile.setName(file.getName());
                if(file.isDirectory()) {
                    postFile.setType(Tools.DIRECTORY);
                    postFile.setFiles(loadFiles(file.getAbsolutePath()));
                    log.debug("project: {}", postFile);
                }else{
                    postFile.setType(Tools.getFileType(file.toPath()));
                    log.debug("Project File: {}", postFile);
                }
                postFiles.add(postFile);
            });
        }
        log.debug("postFiles: {}", postFiles);
        return postFiles;
    }

    public Set<PostFile> loadFiles(String path){
        Set<PostFile> list = new HashSet<>();
        File input = new File(path);
        if(input.isDirectory()){
            Set<File> projectDirectories = Tools.getDirectories(input);
            if(!projectDirectories.isEmpty()){
                projectDirectories.stream().forEach(file -> {
                    PostFile postFile = new PostFile();
                    postFile.setId(String.valueOf(UUID.randomUUID()));
                    postFile.setPath(file.getPath());
                    postFile.setName(file.getName());
                    if(file.isDirectory()) {
                        postFile.setType(Tools.DIRECTORY);
                        postFile.setFiles(loadFiles(file.getAbsolutePath()));
                        log.debug("Plot: {}", postFile);
                    }else{
                        postFile.setType(Tools.getFileType(file.toPath()));
                        log.debug("PlotFile: {}", postFile);
                    }
                    list.add(postFile);
                });
            }

            for(File f: input.listFiles()) {
                if(f.isFile()){
                    PostFile postFile = new PostFile();
                    postFile.setId(String.valueOf(UUID.randomUUID()));
                    postFile.setPath(f.getPath());
                    postFile.setName(f.getName());
                    postFile.setType(Tools.getFileType(f.toPath()));
                    list.add(postFile);
                }
            }
        }else{
            PostFile postFile = new PostFile();
            postFile.setId(String.valueOf(UUID.randomUUID()));
            postFile.setPath(input.getPath());
            postFile.setName(input.getName());
            postFile.setType(Tools.getFileType(input.toPath()));
            list.add(postFile);
        }
        log.debug("load-files-list: {}", list);
        return list;
    }

    /**
     * Get one postProject by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    public PostProject findOne(String id) {
        log.debug("Request to get PostProject : {}", id);
        return postProjectRepository.findOne(id);
    }

    /**
     * Get one postProject by name.
     *
     * @param name the name of the entity
     * @return the entity
     */
    public PostProject findByName(String name) {
        log.debug("Request to get PostProject : {}", name);
        return postProjectRepository.findByName(name).get();
    }
    /**
     * Delete the postProject by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete PostProject : {}", id);
        postProjectRepository.delete(id);
    }

    public PostProject initProject(String name) {
        PostProject postProject = new PostProject();
        postProject.setName(name);
        String project = TRACES_ROOT.concat(Tools.getSeparator()).concat(name);
        File projectFile = new File(project);
        if(projectFile.exists()){
            int result = tmfInit(name);
            if(result > 0){
                postProject.setPostTraces(loadTracesByProject(name));
                return save(postProject);
            }
        }
        return  null;
    }

    public List<PostTrace> loadTracesByProject(String project) {
        return postTraceService.loadTraceRoot(project);
    }
    public int tmfInit(String project){
        int result = -1;
        String projectRoot = TRACES_ROOT.endsWith(Tools.getSeparator())?
            TRACES_ROOT.concat(project) :
            TRACES_ROOT.concat(Tools.getSeparator()).concat(project);
        log.debug("Request to tmf init in: {}", projectRoot);
        Set<String> commands = new HashSet<>();
        // Enter command directory
        commands.add("cd " + projectRoot); //"tmf init";
        commands.add("tmf init");
        Runtime runtime = Runtime.getRuntime();
        String[] cmds = (String[]) commands.toArray();
        String[] envs = (String[]) System.getProperties().values().toArray();
        try {
            Process process = runtime.exec(cmds, envs, Tools.getDirectory(TRACES_ROOT));
            result = process.waitFor();
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
        return result;
    }

    public void cleanPath(String path) {
        postTraceService.tmfClean(path);
    }
}
