package at.thalesgroup.service;

import at.thalesgroup.domain.PostTrace;
import at.thalesgroup.domain.TraceAnalysis;
import at.thalesgroup.domain.TraceAnalysisPlot;
import at.thalesgroup.repository.PostTraceRepository;
import at.thalesgroup.service.util.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Trace.
 */
@Service
public class PostTraceService {

    private final Logger log = LoggerFactory.getLogger(PostTraceService.class);

    private final PostTraceRepository traceRepository;
    private Properties configurationProperties = System.getProperties();
    private String TRACES_ROOT = "TRACES_ROOT";
    private final String ANALYSIS_NAME = "analysis";
    private final String TRACES_DIR = "traces";

    public PostTraceService(PostTraceRepository traceRepository) {
        this.traceRepository = traceRepository;
        this.TRACES_ROOT = configurationProperties.getProperty("TRACES_ROOT",
                Tools.getApplicationRoot().concat(TRACES_DIR).concat(Tools.getSeparator()));
        log.info("TRACES_ROOT: {}", this.TRACES_ROOT);
    }

    /**
     * Save a trace.
     *
     * @param trace the entity to save
     * @return the persisted entity
     */
    public PostTrace save(PostTrace trace) {
        log.debug("Request to save Trace : {}", trace);
        return traceRepository.save(trace);
    }

    /**
     * Get all the traces.
     *
     * @return the list of entities
     */
    public List<PostTrace> findAll() {
        log.debug("Request to get all Traces");
        List<PostTrace> postTraces = traceRepository.findAll();
        if(postTraces.isEmpty()){
            postTraces = loadTraceRoot(null);
        }
        return postTraces;
    }

    /**
     * Get one trace by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    public PostTrace findOne(String id) {
        log.debug("Request to get Trace : {}", id);
        return traceRepository.findOne(id);
    }

    /**
     * Load analysis and plots data under trace folder
     */
    public List<PostTrace> loadTraceRoot(String project){
        List<PostTrace> traces = new ArrayList<>();
        String projectPath = (project == null) ? "" : project.concat(Tools.getSeparator());
        File tracesRootDirectory = Tools.getDirectory(TRACES_ROOT.concat(projectPath));
        Set<File> traceDirectories = Tools.getDirectories(tracesRootDirectory);
        if(!traceDirectories.isEmpty()){
            traceDirectories.stream().forEach(traceDirectoryFile -> {
                PostTrace postTrace = new PostTrace();
                postTrace.setFile(TRACES_DIR.concat(Tools.getSeparator()).concat(projectPath).concat(traceDirectoryFile.getName()));
                postTrace.setName(traceDirectoryFile.getName());
                postTrace.setTimestamp(Tools.getCreatedDate(traceDirectoryFile.toPath()).toString());
                Set<File> analysisDirectories = Tools.getDirectories(traceDirectoryFile);
                log.info(postTrace.getName());
                if(!analysisDirectories.isEmpty()){
                    analysisDirectories.stream().forEach(analysisFile -> {
                        if(analysisFile.getName().equals(ANALYSIS_NAME)){
                            TraceAnalysis ta = new TraceAnalysis();
                            ta.setUrl(postTrace.getFile().concat(Tools.getSeparator()).concat(analysisFile.getName()));
                            ta.setDescription(analysisFile.getName());
                            ta.setCreatedDate(Tools.getCreatedDate(analysisFile.toPath()));
                            ta.setCreatedBy(Tools.getOwner(analysisFile.toPath()));
                            ta.setId(String.valueOf(UUID.randomUUID()));
                            log.info("analysis");
                            log.info(analysisFile.getAbsolutePath());
                            Set<File> plotDirectories = Tools.getDirectories(Tools.getDirectory(analysisFile.getAbsolutePath()));
                            log.debug("Plots: {}", plotDirectories);
                            if(!plotDirectories.isEmpty()){
                                plotDirectories.stream().forEach(plotDirectory -> {
                                    log.debug("Plot directory: {}", plotDirectory.getAbsoluteFile());
                                    Set<File> plotFolders = Tools.getDirectories(plotDirectory);
                                    log.info("Plot Folders found: {}", plotFolders.size());
                                    if(!plotFolders.isEmpty()){
                                        Set<TraceAnalysisPlot> plots = new HashSet<>();
                                        plotFolders.stream().forEach(plot -> {
                                            log.debug("Plot found: {}", plot.getName());
                                            Set<File> plotFiles = Tools.getFiles(plot);
                                            plotFiles.stream().forEach(file -> {
                                                String fileType = file.getAbsolutePath();
                                                if(fileType.contains(".svg")
                                                        || fileType.contains(".png")
                                                        || fileType.contains(".js")
                                                        || fileType.contains(".jpg")
                                                        || fileType.contains(".dat")
                                                        ) {
                                                    log.debug("File found: {} ", file.getName());
                                                    TraceAnalysisPlot tap = new TraceAnalysisPlot();
                                                    String url = ta.getUrl()
                                                            .concat(Tools.getSeparator())
                                                            .concat(plotDirectory.getName())
                                                            .concat(Tools.getSeparator())
                                                            .concat(plot.getName())
                                                            .concat(Tools.getSeparator())
                                                            .concat(file.getName());
                                                    tap.setUrl(url);
                                                    tap.setName(plot.getName().concat("-").concat(file.getName().substring(0, file.getName().lastIndexOf('.'))));
                                                    tap.setType(fileType.substring(fileType.lastIndexOf('.')+1));
                                                    tap.setCreatedDate(ZonedDateTime.ofInstant(Tools.getCreatedDate(file.toPath()), ZoneId.systemDefault()));
                                                    tap.setId(String.valueOf(UUID.randomUUID()));
                                                    plots.add(tap);
                                                }
                                            });
                                        });
                                        ta.setPlots(plots);
                                    }
                                });
                            }
                            postTrace.setAnalysis(ta);
                        }
                    });
                    traces.add(postTrace);
                }
            });
        }
        log.info("loaded traces...");
        return traces;
    }

    /**
     * Delete the trace by id.
     *
     * @param id the id of the entity
     */
    public void delete(String id) {
        log.debug("Request to delete Trace : {}", id);
        traceRepository.delete(id);
    }

    public List<PostTrace> deleteAll() {
        log.debug("Request to delete all Traces");
        traceRepository.deleteAll();
        return findAll();
    }

    public void remove(String id){
        log.debug("Request to remove Trace : {}", id);
        PostTrace postTrace = findOne(id);
        Tools.removeFile(postTrace.getFile());
        delete(id);
    }

    public List<PostTrace> removeAll(){
        log.debug("Request to remove all Traces");
        List<PostTrace> traces = findAll();
        traces.stream().forEach(pt -> Tools.removeFile(pt.getFile()));
        return deleteAll();
    }

    public void tmfInit(String project){
        String projectRoot = TRACES_ROOT.endsWith(Tools.getSeparator())?
                                            TRACES_ROOT.concat(project) :
                                            TRACES_ROOT.concat(Tools.getSeparator()).concat(project);
        log.debug("Request to tmf init in: {}", projectRoot);
        Set<String> commands = new HashSet<>();
        // Enter command directory
        commands.add("cd " + projectRoot); //"tmf init";
        commands.add("tmf init");
        Runtime runtime = Runtime.getRuntime();
        String[] cmds = (String[]) commands.toArray();
        String[] envs = (String[]) System.getProperties().values().toArray();
        try {
            Process process = runtime.exec(cmds, envs, Tools.getDirectory(TRACES_ROOT));
            int result = process.waitFor();
            if(result > 0)  loadTraceRoot(project);
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
    }

    public void tmfClean(String project, List<String> args){
        String projectRoot = TRACES_ROOT.endsWith(Tools.getSeparator())?
                TRACES_ROOT.concat(project) :
                TRACES_ROOT.concat(Tools.getSeparator()).concat(project);
        log.debug("Request to tmf clean: {}/{}", projectRoot);
        // create process commands
        List<String> commands = new ArrayList<>();
        commands.add("cd ".concat(projectRoot));
        commands.add("tmf clean ");
        // concatenate process command arguments
        Collections.addAll(commands, (String[])args.toArray());
        // create process builder
        ProcessBuilder builder = new ProcessBuilder();
        // get process builder environment
        Map<String, String> environment = builder.environment();
        // set traces root directory
        environment.put("TRACES_ROOT", TRACES_ROOT);
        // switch to traces directory
        builder.directory(Tools.getDirectory(TRACES_ROOT));
        // set command
        builder.command(commands);

        try {
            builder.redirectErrorStream(true);
            Process process = builder.start();
            StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), log::info);
            Executors.newSingleThreadExecutor().submit(streamGobbler);
            int result = process.waitFor();
            log.debug("process result: {}", result);
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
    }

    public void tmfClean(String project){
        log.debug("Request to tmf clean: {}/{}", TRACES_ROOT, project);
        // create process commands
        List<String> commands = new ArrayList<>();
        commands.add("cd ".concat(TRACES_ROOT));
        commands.add("tmf clean ".concat(project));
        // create process builder
        ProcessBuilder builder = new ProcessBuilder();
        // get process builder environment
        Map<String, String> environment = builder.environment();
        // set traces root directory
        environment.put("TRACES_ROOT", TRACES_ROOT);
        // switch to traces directory
        builder.directory(Tools.getDirectory(TRACES_ROOT));
        // set command
        builder.command(commands);

        try {
            builder.redirectErrorStream(true);
            Process process = builder.start();
            StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), log::info);
            Executors.newSingleThreadExecutor().submit(streamGobbler);
            int result = process.waitFor();
            log.debug("clean process result: {}", result);
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
    }

    public void tmfCleanPlots(String project) {
        log.debug("Request to tmf clean: {}/{}", TRACES_ROOT);
        // create process commands
        List<String> commands = new ArrayList<>();
        commands.add("cd ".concat(TRACES_ROOT));
        commands.add("tmf clean-plots ".concat(project));
        // create process builder
        ProcessBuilder builder = new ProcessBuilder();
        // get process builder environment
        Map<String, String> environment = builder.environment();
        // set traces root directory
        environment.put("TRACES_ROOT", TRACES_ROOT);
        // switch to traces directory
        builder.directory(Tools.getDirectory(TRACES_ROOT));
        // set command
        builder.command(commands);

        try {
            builder.redirectErrorStream(true);
            Process process = builder.start();
            StreamGobbler streamGobbler = new StreamGobbler(process.getInputStream(), log::info);
            Executors.newSingleThreadExecutor().submit(streamGobbler);
            int result = process.waitFor();
            log.debug("process result: {}", result);
        }catch (Exception e) {
            log.error(e.getMessage(), e.getCause());
        }
    }
}
