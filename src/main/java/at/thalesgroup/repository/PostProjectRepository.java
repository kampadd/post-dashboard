package at.thalesgroup.repository;

import at.thalesgroup.domain.PostProject;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the PostProject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PostProjectRepository extends MongoRepository<PostProject, String> {
    Optional<PostProject> findByName(String name);
}
