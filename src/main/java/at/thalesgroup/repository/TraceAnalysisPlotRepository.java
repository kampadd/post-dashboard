package at.thalesgroup.repository;

import at.thalesgroup.domain.TraceAnalysisPlot;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TraceAnalysisPlot entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraceAnalysisPlotRepository extends MongoRepository<TraceAnalysisPlot, String> {

}
