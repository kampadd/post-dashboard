package at.thalesgroup.repository;

import at.thalesgroup.domain.TraceLog;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TraceLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraceLogRepository extends MongoRepository<TraceLog, String> {

}
