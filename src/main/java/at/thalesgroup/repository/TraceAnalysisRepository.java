package at.thalesgroup.repository;

import at.thalesgroup.domain.TraceAnalysis;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TraceAnalysis entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TraceAnalysisRepository extends MongoRepository<TraceAnalysis, String> {

}
