package at.thalesgroup.repository;

import at.thalesgroup.domain.PostTrace;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Trace entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PostTraceRepository extends MongoRepository<PostTrace, String> {

    Optional<PostTrace> findByName(String name);
}
