package at.thalesgroup.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.File;
import java.io.Serializable;
import java.util.*;

/**
 * A PostProject.
 */
@Document(collection = "post_project")
public class PostFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("type")
    private String type;

    @Field("path")
    private String path;

    @Field("files")
    private Set<PostFile> files = new HashSet<>();



    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PostFile name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {return type;}

    public void setType(String type) { this.type = type;}

    public PostFile type(String type) {
        this.type = type;
        return this;
    }

    public Set<PostFile> getFiles(){ return files; }

    public void setFiles(Set<PostFile> files) {
        this.files = files;
    }

    public PostFile files(Set<PostFile> files) {
        this.files = files;
        return this;
    }

    public String getPath(){ return path;}

    public void setPath(String  file) { this.path = file;}

    public PostFile path(String file) {
        this.path = file;
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PostProject{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            ", path='" + getPath() + "'" +
            ", files='" + getFiles() + "'" +
            "}";
    }
}
