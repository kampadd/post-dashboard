package at.thalesgroup.domain;

import org.aspectj.weaver.tools.Trace;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Trace.
 */
@Document(collection = "trace")
public class PostTrace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("timestamp")
    private String timestamp;

    @Field("name")
    private String name;

    @Field("file")
    private String file;

    @Field("analysis")
    private TraceAnalysis analysis;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public PostTrace timestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public PostTrace name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public PostTrace file(String file) {
        this.file = file;
        return this;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public TraceAnalysis getAnalysis(){
        return analysis;
    }
    public PostTrace analysis(TraceAnalysis analysis){
        this.analysis = analysis;
        return this;
    }
    public void setAnalysis(TraceAnalysis analysis) {
        this.analysis = analysis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PostTrace trace = (PostTrace) o;
        if (trace.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), trace.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Trace{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", name='" + getName() + "'" +
            ", file='" + getFile() + "'" +
            ", analysis='" + getAnalysis() + "'" +
            "}";
    }
}
