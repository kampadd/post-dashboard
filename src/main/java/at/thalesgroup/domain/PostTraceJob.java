package at.thalesgroup.domain;

import java.time.ZonedDateTime;
import java.util.Objects;

public class PostTraceJob {

	private String id;
	private String name;
	private String command;
	private String status;
	private ZonedDateTime timestamp;

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public ZonedDateTime getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(ZonedDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PostTraceJob postTraceJob = (PostTraceJob) o;
		if (postTraceJob.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), postTraceJob.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

}
