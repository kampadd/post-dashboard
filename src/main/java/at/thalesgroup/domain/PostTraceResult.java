package at.thalesgroup.domain;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class PostTraceResult {
	
	private String id;
	private String name; // Synonymous with job name
	private Set<TraceAnalysis> results = new HashSet<>();
	
	public String getId() {
		return id;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public Set<TraceAnalysis> getResults(){
		return results;
	}
	
	public void setResults(Set<TraceAnalysis> results) {
		this.results = results;
	}
	

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PostTraceResult postTraceResult = (PostTraceResult) o;
		if (postTraceResult.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), postTraceResult.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

}
