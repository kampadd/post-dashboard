package at.thalesgroup.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.*;

/**
 * A PostProject.
 */
@Document(collection = "post_project")
public class PostProject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("post_traces")
    private List<PostTrace> postTraces = new ArrayList<>();

    @Field("metadata")
    private Map<String, String> metadata = new HashMap<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PostProject name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PostTrace> getPostTraces() {
        return postTraces;
    }

    public PostProject postTraces(List<PostTrace> postTraces) {
        this.postTraces = postTraces;
        return this;
    }

    public void setPostTraces(List<PostTrace> postTraces) {
        this.postTraces = postTraces;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public PostProject metadata(Map<String, String> metadata) {
        this.metadata = metadata;
        return this;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PostProject postProject = (PostProject) o;
        if (postProject.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), postProject.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PostProject{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", postTraces='" + getPostTraces() + "'" +
            ", metadata='" + getMetadata() + "'" +
            "}";
    }
}
