/**
 * View Models used by Spring MVC REST controllers.
 */
package at.thalesgroup.web.rest.vm;
