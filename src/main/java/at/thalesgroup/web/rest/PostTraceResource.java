package at.thalesgroup.web.rest;

import com.codahale.metrics.annotation.Timed;
import at.thalesgroup.domain.PostTrace;
import at.thalesgroup.service.PostTraceService;
import at.thalesgroup.web.rest.errors.BadRequestAlertException;
import at.thalesgroup.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;

/**
 * REST controller for managing Trace.
 */
@RestController
@RequestMapping("/api")
public class PostTraceResource {

    private final Logger log = LoggerFactory.getLogger(PostTraceResource.class);

    private static final String ENTITY_NAME = "trace";

    private final PostTraceService traceService;

    public PostTraceResource(PostTraceService traceService) {
        this.traceService = traceService;
    }

    /**
     * POST  /traces : Create a new trace.
     *
     * @param trace the trace to create
     * @return the ResponseEntity with status 201 (Created) and with body the new trace, or with status 400 (Bad Request) if the trace has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/traces")
    @Timed
    public ResponseEntity<PostTrace> createTrace(@RequestBody PostTrace trace) throws URISyntaxException {
        log.debug("REST request to save Trace : {}", trace);
        if (trace.getId() != null) {
            throw new BadRequestAlertException("A new trace cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PostTrace result = traceService.save(trace);
        return ResponseEntity.created(new URI("/api/traces/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /traces : Updates an existing trace.
     *
     * @param trace the trace to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated trace,
     * or with status 400 (Bad Request) if the trace is not valid,
     * or with status 500 (Internal Server Error) if the trace couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/traces")
    @Timed
    public ResponseEntity<PostTrace> updateTrace(@RequestBody PostTrace trace) throws URISyntaxException {
        log.debug("REST request to update Trace : {}", trace);
        if (trace.getId() == null) {
            return createTrace(trace);
        }
        PostTrace result = traceService.save(trace);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, trace.getId().toString()))
            .body(result);
    }

    /**
     * GET  /traces : get all the traces.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of traces in body
     */
    @GetMapping("/traces")
    @Timed
    public List<PostTrace> getAllTraces() {
        log.debug("REST request to get all Traces");
        return traceService.findAll();
        }
    /**
     * GET  /traces : load and return all the traces.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of traces in body
     */
    @GetMapping("/traces/tmf/list")
    @Timed
    public List<PostTrace> loadAllTraces() {
        log.debug("REST request to load all Traces");
        return traceService.loadTraceRoot(null);
    }
    /**
     * GET  /traces/command/:cmd : execute command on the underlying tmf.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of traces in body
     */
    @GetMapping("/traces/command/{projectName}/{cmd}")
    @Timed
    public List<PostTrace> tracesLoad(@PathVariable String projectName, @PathVariable String cmd) {
        log.debug("REST request to load all Traces from directory");

        String[] commands = cmd.split("\\s");
        String command = commands[0];


        List<PostTrace> traces = new ArrayList<>();

        if(command.startsWith("init")){
            return tmfInit(projectName);
        }

        if(command.startsWith("clean")){
            ArrayList<String> args = new ArrayList<>();
            Collections.addAll(args, commands);
            args.remove(0);
            traceService.tmfClean(projectName, args);
            return getAllTraces();
        }

        return traces;
    }
    /**
     * GET  /traces : get all the traces.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of traces in body
     */
    @GetMapping("/traces/tmf/init")
    @Timed
    public List<PostTrace> tmfInit(String project) {
        log.debug("REST request to init traces");
        traceService.tmfInit(project);
        return getAllTraces();
    }

    /**
     * GET  /traces/:id : get the "id" trace.
     *
     * @param id the id of the trace to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the trace, or with status 404 (Not Found)
     */
    @GetMapping("/traces/{id}")
    @Timed
    public ResponseEntity<PostTrace> getTrace(@PathVariable String id) {
        log.debug("REST request to get Trace : {}", id);
        PostTrace trace = traceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(trace));
    }

    /**
     * DELETE  /traces/:id : delete the "id" trace.
     *
     * @param id the id of the trace to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traces/{id}")
    @Timed
    public ResponseEntity<Void> deleteTrace(@PathVariable String id) {
        log.debug("REST request to delete Trace : {}", id);
        traceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * DELETE  /traces/delete-all : delete all traces.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traces/delete-all")
    @Timed
    public ResponseEntity<Void> deleteAllTrace() {
        log.debug("REST request to delete all Traces ");
        traceService.deleteAll();
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "all-traces")).build();
    }

    /**
     * DELETE  /traces/tmf/clean: tmf clean directory.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traces/tmf/clean/{project}")
    @Timed
    public List<PostTrace> tmfClean(@PathVariable String project) {
        log.debug("REST request to delete all Traces ");
        traceService.tmfClean(project);
        return getAllTraces();
    }
    /**
     * DELETE  /traces/tmf/clean/:directory : tmf clean directory.
     *
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/traces/tmf/clean-plots/{directory}")
    @Timed
    public List<PostTrace> tmfCleanPlots(@PathVariable String directory) {
        log.debug("REST request to plots of directory : {} ", directory);
        traceService.tmfCleanPlots(directory);
        return getAllTraces();
    }
}
