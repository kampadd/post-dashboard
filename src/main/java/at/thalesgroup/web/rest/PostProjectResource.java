package at.thalesgroup.web.rest;

import at.thalesgroup.domain.PostFile;
import com.codahale.metrics.annotation.Timed;
import at.thalesgroup.domain.PostProject;
import at.thalesgroup.service.PostProjectService;
import at.thalesgroup.web.rest.errors.BadRequestAlertException;
import at.thalesgroup.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing PostProject.
 */
@RestController
@RequestMapping("/api")
public class PostProjectResource {

    private final Logger log = LoggerFactory.getLogger(PostProjectResource.class);

    private static final String ENTITY_NAME = "postProject";

    private final PostProjectService postProjectService;

    public PostProjectResource(PostProjectService postProjectService) {
        this.postProjectService = postProjectService;
    }

    /**
     * POST  /post-projects : Create a new postProject.
     *
     * @param postProject the postProject to create
     * @return the ResponseEntity with status 201 (Created) and with body the new postProject, or with status 400 (Bad Request) if the postProject has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/post-projects")
    @Timed
    public ResponseEntity<PostProject> createPostProject(@RequestBody PostProject postProject) throws URISyntaxException {
        log.debug("REST request to save PostProject : {}", postProject);
        if (postProject.getId() != null) {
            throw new BadRequestAlertException("A new postProject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PostProject result = postProjectService.save(postProject);
        return ResponseEntity.created(new URI("/api/post-projects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("post-projects/upload")
    @Timed
    public ResponseEntity<PostProject> upload(@RequestPart("file") MultipartFile file, @RequestParam("project") String project) throws URISyntaxException, IOException {
        PostProject postProject = new PostProject();
        postProject.setName(project);
        postProjectService.upload(file, project);
        postProject.setPostTraces(postProjectService.loadTracesByProject(project));
        PostProject result = postProjectService.save(postProject);
        return ResponseEntity.created(new URI("/api/post-projects/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /post-projects : Updates an existing postProject.
     *
     * @param postProject the postProject to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated postProject,
     * or with status 400 (Bad Request) if the postProject is not valid,
     * or with status 500 (Internal Server Error) if the postProject couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/post-projects")
    @Timed
    public ResponseEntity<PostProject> update(@RequestBody PostProject postProject) throws URISyntaxException {
        log.debug("REST request to update PostProject : {}", postProject);
        if (postProject.getId() == null) {
            return createPostProject(postProject);
        }
        PostProject result = postProjectService.save(postProject);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, postProject.getId().toString()))
            .body(result);
    }

    /**
     * GET  /post-projects/path/ : list generic files and directories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of postProjects in body
     */
    @GetMapping("/post-projects/path/{path}")
    @Timed
    public Set<PostFile> list(@PathVariable String path) {
        log.debug("REST request to get all PostProjects");
        Set<PostFile> projects = postProjectService.loadFiles(path);
        return projects;
    }

    /**
     * GET  /post-projects/clean/ : clean generic files and directories.
     *
     * @return the ResponseEntity with status 200 (OK) to indicate successful cleanup
     */
    @GetMapping("/post-projects/clean/{path}")
    @Timed
    public ResponseEntity<Void> clean(@PathVariable @NotNull String path) {
        log.debug("REST request to clean path: {}", path);
        if(path != null && !path.isEmpty() && !path.equals("null") && !path.startsWith("null")) {
            PostProject project = postProjectService.findByName(path);
            if(Optional.ofNullable(project).isPresent()){
                postProjectService.cleanPath(path);
                return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, path)).build();
            }else{
                return ResponseEntity.notFound().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "clean")).build();
            }
        }
        return ResponseEntity.badRequest().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, "clean")).build();
    }

    /**
     * GET  /post-projects/display/ : display all the postProjects.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of postProjects in body
     */
    @GetMapping("/post-projects/display")
    @Timed
    public List<PostFile> display() {
        log.debug("REST request to display all project files");
        List<PostFile> projects = postProjectService.listFiles();
        log.debug("found: {}", projects);
        return projects;
    }

    /**
     * GET  /post-projects/load : load all the postProjects.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of postProjects in body
     */
    @GetMapping("/post-projects/load")
    @Timed
    public List<PostProject> load() {
        log.debug("REST request to get all PostProjects");
        List<PostProject> projects = postProjectService.loadProjects();
        return projects;
    }

    /**
     * GET  /post-projects/init/{name} : init  postProject by name.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of postProjects in body
     */
    @GetMapping("/post-projects/init/{name}")
    @Timed
    public PostProject init(@PathVariable String name) {
        log.debug("REST request to get all PostProjects");
        PostProject project = postProjectService.initProject(name);
        return project;
    }

    /**
     * GET  /post-projects : get all the postProjects.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of postProjects in body
     */
    @GetMapping("/post-projects")
    @Timed
    public List<PostProject> getAll() {
        log.debug("REST request to get all PostProjects");
        List<PostProject> projects = postProjectService.findAll();
        if (projects.isEmpty()) {
            projects = postProjectService.loadProjects();
        }
        return projects;
    }

    /**
     * GET  /post-projects/:id : get the "id" postProject.
     *
     * @param id the id of the postProject to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the postProject, or with status 404 (Not Found)
     */
    @GetMapping("/post-projects/{id}")
    @Timed
    public ResponseEntity<PostProject> get(@PathVariable String id) {
        log.debug("REST request to get PostProject : {}", id);
        PostProject postProject = postProjectService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(postProject));
    }

    /**
     * DELETE  /post-projects/:id : delete the "id" postProject.
     *
     * @param id the id of the postProject to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/post-projects/{id}")
    @Timed
    public ResponseEntity<Void> delete(@PathVariable String id) {
        log.debug("REST request to delete PostProject : {}", id);
        postProjectService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
