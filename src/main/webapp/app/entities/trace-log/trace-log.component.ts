import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService, JhiDataUtils } from 'ng-jhipster';

import { TraceLog } from './trace-log.model';
import { TraceLogService } from './trace-log.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-trace-log',
    templateUrl: './trace-log.component.html'
})
export class TraceLogComponent implements OnInit, OnDestroy {
traceLogs: TraceLog[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private traceLogService: TraceLogService,
        private jhiAlertService: JhiAlertService,
        private dataUtils: JhiDataUtils,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.traceLogService.query().subscribe(
            (res: HttpResponse<TraceLog[]>) => {
                this.traceLogs = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTraceLogs();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TraceLog) {
        return item.id;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    registerChangeInTraceLogs() {
        this.eventSubscriber = this.eventManager.subscribe('traceLogListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
