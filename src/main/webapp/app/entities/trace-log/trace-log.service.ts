import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TraceLog } from './trace-log.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TraceLog>;

@Injectable()
export class TraceLogService {

    private resourceUrl =  SERVER_API_URL + 'api/trace-logs';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(traceLog: TraceLog): Observable<EntityResponseType> {
        const copy = this.convert(traceLog);
        return this.http.post<TraceLog>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(traceLog: TraceLog): Observable<EntityResponseType> {
        const copy = this.convert(traceLog);
        return this.http.put<TraceLog>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<TraceLog>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TraceLog[]>> {
        const options = createRequestOption(req);
        return this.http.get<TraceLog[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TraceLog[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TraceLog = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TraceLog[]>): HttpResponse<TraceLog[]> {
        const jsonResponse: TraceLog[] = res.body;
        const body: TraceLog[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TraceLog.
     */
    private convertItemFromServer(traceLog: TraceLog): TraceLog {
        const copy: TraceLog = Object.assign({}, traceLog);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(traceLog.createdDate);
        copy.modifiedDate = this.dateUtils
            .convertDateTimeFromServer(traceLog.modifiedDate);
        return copy;
    }

    /**
     * Convert a TraceLog to a JSON which can be sent to the server.
     */
    private convert(traceLog: TraceLog): TraceLog {
        const copy: TraceLog = Object.assign({}, traceLog);

        copy.createdDate = this.dateUtils.toDate(traceLog.createdDate);

        copy.modifiedDate = this.dateUtils.toDate(traceLog.modifiedDate);
        return copy;
    }
}
