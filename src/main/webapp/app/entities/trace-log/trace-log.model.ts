import { BaseEntity } from './../../shared';

export class TraceLog implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public dataContentType?: string,
        public data?: any,
        public createdBy?: string,
        public createdDate?: any,
        public modifiedBy?: string,
        public modifiedDate?: any,
    ) {
    }
}
