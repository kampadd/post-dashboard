import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../../shared';
import {
    TraceLogService,
    TraceLogPopupService,
    TraceLogComponent,
    TraceLogDetailComponent,
    TraceLogDialogComponent,
    TraceLogPopupComponent,
    TraceLogDeletePopupComponent,
    TraceLogDeleteDialogComponent,
    traceLogRoute,
    traceLogPopupRoute,
} from './';

const ENTITY_STATES = [
    ...traceLogRoute,
    ...traceLogPopupRoute,
];

@NgModule({
    imports: [
        PostDashboardSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TraceLogComponent,
        TraceLogDetailComponent,
        TraceLogDialogComponent,
        TraceLogDeleteDialogComponent,
        TraceLogPopupComponent,
        TraceLogDeletePopupComponent,
    ],
    entryComponents: [
        TraceLogComponent,
        TraceLogDialogComponent,
        TraceLogPopupComponent,
        TraceLogDeleteDialogComponent,
        TraceLogDeletePopupComponent,
    ],
    providers: [
        TraceLogService,
        TraceLogPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardTraceLogModule {}
