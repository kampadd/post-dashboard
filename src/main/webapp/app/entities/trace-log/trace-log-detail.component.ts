import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { TraceLog } from './trace-log.model';
import { TraceLogService } from './trace-log.service';

@Component({
    selector: 'jhi-trace-log-detail',
    templateUrl: './trace-log-detail.component.html'
})
export class TraceLogDetailComponent implements OnInit, OnDestroy {

    traceLog: TraceLog;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataUtils: JhiDataUtils,
        private traceLogService: TraceLogService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTraceLogs();
    }

    load(id) {
        this.traceLogService.find(id)
            .subscribe((traceLogResponse: HttpResponse<TraceLog>) => {
                this.traceLog = traceLogResponse.body;
            });
    }
    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTraceLogs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'traceLogListModification',
            (response) => this.load(this.traceLog.id)
        );
    }
}
