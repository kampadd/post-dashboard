import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';

import { TraceLog } from './trace-log.model';
import { TraceLogPopupService } from './trace-log-popup.service';
import { TraceLogService } from './trace-log.service';

@Component({
    selector: 'jhi-trace-log-dialog',
    templateUrl: './trace-log-dialog.component.html'
})
export class TraceLogDialogComponent implements OnInit {

    traceLog: TraceLog;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private dataUtils: JhiDataUtils,
        private traceLogService: TraceLogService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.traceLog.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traceLogService.update(this.traceLog));
        } else {
            this.subscribeToSaveResponse(
                this.traceLogService.create(this.traceLog));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TraceLog>>) {
        result.subscribe((res: HttpResponse<TraceLog>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TraceLog) {
        this.eventManager.broadcast({ name: 'traceLogListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-trace-log-popup',
    template: ''
})
export class TraceLogPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceLogPopupService: TraceLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.traceLogPopupService
                    .open(TraceLogDialogComponent as Component, params['id']);
            } else {
                this.traceLogPopupService
                    .open(TraceLogDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
