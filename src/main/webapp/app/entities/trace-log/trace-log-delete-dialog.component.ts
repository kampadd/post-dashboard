import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TraceLog } from './trace-log.model';
import { TraceLogPopupService } from './trace-log-popup.service';
import { TraceLogService } from './trace-log.service';

@Component({
    selector: 'jhi-trace-log-delete-dialog',
    templateUrl: './trace-log-delete-dialog.component.html'
})
export class TraceLogDeleteDialogComponent {

    traceLog: TraceLog;

    constructor(
        private traceLogService: TraceLogService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.traceLogService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'traceLogListModification',
                content: 'Deleted an traceLog'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trace-log-delete-popup',
    template: ''
})
export class TraceLogDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceLogPopupService: TraceLogPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.traceLogPopupService
                .open(TraceLogDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
