import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { TraceLog } from './trace-log.model';
import { TraceLogService } from './trace-log.service';

@Injectable()
export class TraceLogPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private traceLogService: TraceLogService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.traceLogService.find(id)
                    .subscribe((traceLogResponse: HttpResponse<TraceLog>) => {
                        const traceLog: TraceLog = traceLogResponse.body;
                        traceLog.createdDate = this.datePipe
                            .transform(traceLog.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                        traceLog.modifiedDate = this.datePipe
                            .transform(traceLog.modifiedDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.traceLogModalRef(component, traceLog);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.traceLogModalRef(component, new TraceLog());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    traceLogModalRef(component: Component, traceLog: TraceLog): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.traceLog = traceLog;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
