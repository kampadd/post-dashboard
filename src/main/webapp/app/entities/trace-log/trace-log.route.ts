import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TraceLogComponent } from './trace-log.component';
import { TraceLogDetailComponent } from './trace-log-detail.component';
import { TraceLogPopupComponent } from './trace-log-dialog.component';
import { TraceLogDeletePopupComponent } from './trace-log-delete-dialog.component';

export const traceLogRoute: Routes = [
    {
        path: 'trace-log',
        component: TraceLogComponent,
        data: {
            authorities: [],
            pageTitle: 'TraceLogs'
        }
    }, {
        path: 'trace-log/:id',
        component: TraceLogDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'TraceLogs'
        }
    }
];

export const traceLogPopupRoute: Routes = [
    {
        path: 'trace-log-new',
        component: TraceLogPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'TraceLogs'
        },
        outlet: 'popup'
    },
    {
        path: 'trace-log/:id/edit',
        component: TraceLogPopupComponent,
        data: {
            authorities: [],
            pageTitle: 'TraceLogs'
        },
        outlet: 'popup'
    },
    {
        path: 'trace-log/:id/delete',
        component: TraceLogDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'TraceLogs'
        },
        outlet: 'popup'
    }
];
