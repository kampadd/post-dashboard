import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Trace } from './trace.model';
import { TracePopupService } from './trace-popup.service';
import { TraceService } from './trace.service';

@Component({
    selector: 'jhi-trace-delete-dialog',
    templateUrl: './trace-delete-dialog.component.html'
})
export class TraceDeleteDialogComponent {

    trace: Trace;

    constructor(
        private traceService: TraceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.traceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'traceListModification',
                content: 'Deleted an trace'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trace-delete-popup',
    template: ''
})
export class TraceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tracePopupService: TracePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.tracePopupService
                .open(TraceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
