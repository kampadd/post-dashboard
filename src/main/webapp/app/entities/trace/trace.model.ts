import { BaseEntity } from './../../shared';
import { TraceAnalysis } from '../trace-analysis';

export class Trace implements BaseEntity {
    constructor(
        public id?: string,
        public timestamp?: string,
        public name?: string,
        public file?: string,
        public analysis?: TraceAnalysis,
    ) {
    }
}
/**
export class TraceAnalysis {
    constructor(
        public id?: string,
        public description?: string,
        public url?: string,
        public createdDate?: any,
        public createdBy?: string,
    ) {
    }
}
*/
