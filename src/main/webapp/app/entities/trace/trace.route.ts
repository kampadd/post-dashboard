import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TraceComponent } from './trace.component';
import { TraceDetailComponent } from './trace-detail.component';
import { TracePopupComponent } from './trace-dialog.component';
import { TraceDeletePopupComponent } from './trace-delete-dialog.component';

export const traceRoute: Routes = [
    {
        path: 'trace',
        component: TraceComponent,
        data: {
            authorities: [],
            pageTitle: 'Traces'
        }
    }, {
        path: 'trace/:id',
        component: TraceDetailComponent,
        data: {
            authorities: [],
            pageTitle: 'Traces'
        }
    }
];

export const tracePopupRoute: Routes = [
    {
        path: 'trace-new',
        component: TracePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'Traces'
        },
        outlet: 'popup'
    },
    {
        path: 'trace/:id/edit',
        component: TracePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'Traces'
        },
        outlet: 'popup'
    },
    {
        path: 'trace/:id/delete',
        component: TraceDeletePopupComponent,
        data: {
            authorities: [],
            pageTitle: 'Traces'
        },
        outlet: 'popup'
    }
];
