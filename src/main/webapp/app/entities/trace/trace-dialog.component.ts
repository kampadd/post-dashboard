import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Trace } from './trace.model';
import { TracePopupService } from './trace-popup.service';
import { TraceService } from './trace.service';

@Component({
    selector: 'jhi-trace-dialog',
    templateUrl: './trace-dialog.component.html'
})
export class TraceDialogComponent implements OnInit {

    trace: Trace;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private traceService: TraceService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.trace.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traceService.update(this.trace));
        } else {
            this.subscribeToSaveResponse(
                this.traceService.create(this.trace));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Trace>>) {
        result.subscribe((res: HttpResponse<Trace>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Trace) {
        this.eventManager.broadcast({ name: 'traceListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-trace-popup',
    template: ''
})
export class TracePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private tracePopupService: TracePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.tracePopupService
                    .open(TraceDialogComponent as Component, params['id']);
            } else {
                this.tracePopupService
                    .open(TraceDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
