import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Trace } from './trace.model';
import { TraceService } from './trace.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-trace-header',
    templateUrl: './trace-header.component.html',
    styleUrls: ['./trace-header.component.scss']
})
export class TraceHeaderComponent implements OnInit, OnDestroy {
    @Input() traces: Trace[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private traceService: TraceService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    init() {
        console.log('trace-header-init-called');
    }

    list() {
        console.log('trace-header-list-called');
    }
    display() {
        console.log('trace-header-display-called');
    }
    clean() {
        console.log('trace-header-clean-called');
    }
    upload() {
        console.log('trace-header-upload-called');
    }

    loadAll() {
        this.traceService.query().subscribe(
            (res: HttpResponse<Trace[]>) => {
                this.traces = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTraces();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Trace) {
        return item.id;
    }

    registerChangeInTraces() {
        this.eventSubscriber = this.eventManager.subscribe('traceListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
