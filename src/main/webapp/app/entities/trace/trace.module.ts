import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../../shared';
import {
    TraceService,
    TracePopupService,
    TraceComponent,
    TraceDetailComponent,
    TraceDialogComponent,
    TracePopupComponent,
    TraceDeletePopupComponent,
    TraceDeleteDialogComponent,
    traceRoute,
    tracePopupRoute,
    TraceHeaderComponent,
} from './';

const ENTITY_STATES = [
    ...traceRoute,
    ...tracePopupRoute,
];

@NgModule({
    imports: [
        PostDashboardSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TraceComponent,
        TraceDetailComponent,
        TraceDialogComponent,
        TraceDeleteDialogComponent,
        TracePopupComponent,
        TraceDeletePopupComponent,
        TraceHeaderComponent,
    ],
    entryComponents: [
        TraceComponent,
        TraceHeaderComponent,
        TraceDialogComponent,
        TracePopupComponent,
        TraceDeleteDialogComponent,
        TraceDeletePopupComponent,
    ],
    providers: [
        TraceService,
        TracePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardTraceModule {}
