import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Trace } from './trace.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Trace>;

@Injectable()
export class TraceService {

    private resourceUrl =  SERVER_API_URL + 'api/traces';
    private tmfResourceUrl =  SERVER_API_URL + 'api/traces/tmf';

    constructor(private http: HttpClient) { }

    load(req?: any): Observable<HttpResponse<Trace[]>> {
        const options = createRequestOption(req);
        const loadURL = this.resourceUrl + '/load';
        return this.http.get<Trace[]>(loadURL, { params: options, observe: 'response' })
            .map((res: HttpResponse<Trace[]>) => this.convertArrayResponse(res));
    }

    command(command: string): Observable<HttpResponse<Trace[]>> {
        const url = this.tmfResourceUrl + '/' + command;
        return this.http.get<Trace[]>(url, { observe: 'response' })
            .map((res: HttpResponse<Trace[]>) => this.convertArrayResponse(res));
    }

    create(trace: Trace): Observable<EntityResponseType> {
        const copy = this.convert(trace);
        return this.http.post<Trace>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(trace: Trace): Observable<EntityResponseType> {
        const copy = this.convert(trace);
        return this.http.put<Trace>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<Trace>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Trace[]>> {
        const options = createRequestOption(req);
        return this.http.get<Trace[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Trace[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Trace = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Trace[]>): HttpResponse<Trace[]> {
        const jsonResponse: Trace[] = res.body;
        const body: Trace[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Trace.
     */
    private convertItemFromServer(trace: Trace): Trace {
        const copy: Trace = Object.assign({}, trace);
        return copy;
    }

    /**
     * Convert a Trace to a JSON which can be sent to the server.
     */
    private convert(trace: Trace): Trace {
        const copy: Trace = Object.assign({}, trace);
        return copy;
    }
}
