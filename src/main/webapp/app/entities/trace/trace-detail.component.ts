import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Trace } from './trace.model';
import { TraceService } from './trace.service';

@Component({
    selector: 'jhi-trace-detail',
    templateUrl: './trace-detail.component.html'
})
export class TraceDetailComponent implements OnInit, OnDestroy {

    trace: Trace;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private traceService: TraceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTraces();
    }

    load(id) {
        this.traceService.find(id)
            .subscribe((traceResponse: HttpResponse<Trace>) => {
                this.trace = traceResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTraces() {
        this.eventSubscriber = this.eventManager.subscribe(
            'traceListModification',
            (response) => this.load(this.trace.id)
        );
    }
}
