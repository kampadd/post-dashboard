import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../../shared';
import {
    TraceAnalysisService,
    TraceAnalysisPopupService,
    TraceAnalysisComponent,
    TraceAnalysisDetailComponent,
    TraceAnalysisDialogComponent,
    TraceAnalysisPopupComponent,
    TraceAnalysisDeletePopupComponent,
    TraceAnalysisDeleteDialogComponent,
    traceAnalysisRoute,
    traceAnalysisPopupRoute,
} from './';

const ENTITY_STATES = [
    ...traceAnalysisRoute,
    ...traceAnalysisPopupRoute,
];

@NgModule({
    imports: [
        PostDashboardSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TraceAnalysisComponent,
        TraceAnalysisDetailComponent,
        TraceAnalysisDialogComponent,
        TraceAnalysisDeleteDialogComponent,
        TraceAnalysisPopupComponent,
        TraceAnalysisDeletePopupComponent,
    ],
    entryComponents: [
        TraceAnalysisComponent,
        TraceAnalysisDialogComponent,
        TraceAnalysisPopupComponent,
        TraceAnalysisDeleteDialogComponent,
        TraceAnalysisDeletePopupComponent,
    ],
    providers: [
        TraceAnalysisService,
        TraceAnalysisPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardTraceAnalysisModule {}
