import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysis } from './trace-analysis.model';
import { TraceAnalysisPopupService } from './trace-analysis-popup.service';
import { TraceAnalysisService } from './trace-analysis.service';

@Component({
    selector: 'jhi-trace-analysis-dialog',
    templateUrl: './trace-analysis-dialog.component.html'
})
export class TraceAnalysisDialogComponent implements OnInit {

    traceAnalysis: TraceAnalysis;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private traceAnalysisService: TraceAnalysisService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.traceAnalysis.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traceAnalysisService.update(this.traceAnalysis));
        } else {
            this.subscribeToSaveResponse(
                this.traceAnalysisService.create(this.traceAnalysis));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TraceAnalysis>>) {
        result.subscribe((res: HttpResponse<TraceAnalysis>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TraceAnalysis) {
        this.eventManager.broadcast({ name: 'traceAnalysisListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-trace-analysis-popup',
    template: ''
})
export class TraceAnalysisPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceAnalysisPopupService: TraceAnalysisPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.traceAnalysisPopupService
                    .open(TraceAnalysisDialogComponent as Component, params['id']);
            } else {
                this.traceAnalysisPopupService
                    .open(TraceAnalysisDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
