import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysis } from './trace-analysis.model';
import { TraceAnalysisService } from './trace-analysis.service';

@Component({
    selector: 'jhi-trace-analysis-detail',
    templateUrl: './trace-analysis-detail.component.html'
})
export class TraceAnalysisDetailComponent implements OnInit, OnDestroy {

    traceAnalysis: TraceAnalysis;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private traceAnalysisService: TraceAnalysisService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTraceAnalyses();
    }

    load(id) {
        this.traceAnalysisService.find(id)
            .subscribe((traceAnalysisResponse: HttpResponse<TraceAnalysis>) => {
                this.traceAnalysis = traceAnalysisResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTraceAnalyses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'traceAnalysisListModification',
            (response) => this.load(this.traceAnalysis.id)
        );
    }
}
