import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TraceAnalysis } from './trace-analysis.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TraceAnalysis>;

@Injectable()
export class TraceAnalysisService {

    private resourceUrl =  SERVER_API_URL + 'api/trace-analyses';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(traceAnalysis: TraceAnalysis): Observable<EntityResponseType> {
        const copy = this.convert(traceAnalysis);
        return this.http.post<TraceAnalysis>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(traceAnalysis: TraceAnalysis): Observable<EntityResponseType> {
        const copy = this.convert(traceAnalysis);
        return this.http.put<TraceAnalysis>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<TraceAnalysis>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TraceAnalysis[]>> {
        const options = createRequestOption(req);
        return this.http.get<TraceAnalysis[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TraceAnalysis[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TraceAnalysis = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TraceAnalysis[]>): HttpResponse<TraceAnalysis[]> {
        const jsonResponse: TraceAnalysis[] = res.body;
        const body: TraceAnalysis[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TraceAnalysis.
     */
    private convertItemFromServer(traceAnalysis: TraceAnalysis): TraceAnalysis {
        const copy: TraceAnalysis = Object.assign({}, traceAnalysis);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(traceAnalysis.createdDate);
        return copy;
    }

    /**
     * Convert a TraceAnalysis to a JSON which can be sent to the server.
     */
    private convert(traceAnalysis: TraceAnalysis): TraceAnalysis {
        const copy: TraceAnalysis = Object.assign({}, traceAnalysis);

        copy.createdDate = this.dateUtils.toDate(traceAnalysis.createdDate);
        return copy;
    }
}
