import { BaseEntity } from './../../shared';
import { TraceAnalysisPlot } from '../trace-analysis-plot';

export class TraceAnalysis implements BaseEntity {
    constructor(
        public id?: string,
        public description?: string,
        public url?: string,
        public createdDate?: any,
        public createdBy?: string,
        public plots?: TraceAnalysisPlot[],
    ) {
    }
}
