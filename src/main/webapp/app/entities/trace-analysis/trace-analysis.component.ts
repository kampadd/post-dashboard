import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TraceAnalysis } from './trace-analysis.model';
import { TraceAnalysisService } from './trace-analysis.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-trace-analysis',
    templateUrl: './trace-analysis.component.html'
})
export class TraceAnalysisComponent implements OnInit, OnDestroy {
traceAnalyses: TraceAnalysis[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private traceAnalysisService: TraceAnalysisService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.traceAnalysisService.query().subscribe(
            (res: HttpResponse<TraceAnalysis[]>) => {
                this.traceAnalyses = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTraceAnalyses();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TraceAnalysis) {
        return item.id;
    }
    registerChangeInTraceAnalyses() {
        this.eventSubscriber = this.eventManager.subscribe('traceAnalysisListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
