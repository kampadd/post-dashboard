import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysis } from './trace-analysis.model';
import { TraceAnalysisPopupService } from './trace-analysis-popup.service';
import { TraceAnalysisService } from './trace-analysis.service';

@Component({
    selector: 'jhi-trace-analysis-delete-dialog',
    templateUrl: './trace-analysis-delete-dialog.component.html'
})
export class TraceAnalysisDeleteDialogComponent {

    traceAnalysis: TraceAnalysis;

    constructor(
        private traceAnalysisService: TraceAnalysisService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.traceAnalysisService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'traceAnalysisListModification',
                content: 'Deleted an traceAnalysis'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trace-analysis-delete-popup',
    template: ''
})
export class TraceAnalysisDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceAnalysisPopupService: TraceAnalysisPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.traceAnalysisPopupService
                .open(TraceAnalysisDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
