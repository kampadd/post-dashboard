import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TraceAnalysisComponent } from './trace-analysis.component';
import { TraceAnalysisDetailComponent } from './trace-analysis-detail.component';
import { TraceAnalysisPopupComponent } from './trace-analysis-dialog.component';
import { TraceAnalysisDeletePopupComponent } from './trace-analysis-delete-dialog.component';

export const traceAnalysisRoute: Routes = [
    {
        path: 'trace-analysis',
        component: TraceAnalysisComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalyses'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trace-analysis/:id',
        component: TraceAnalysisDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalyses'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const traceAnalysisPopupRoute: Routes = [
    {
        path: 'trace-analysis-new',
        component: TraceAnalysisPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalyses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trace-analysis/:id/edit',
        component: TraceAnalysisPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalyses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trace-analysis/:id/delete',
        component: TraceAnalysisDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalyses'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
