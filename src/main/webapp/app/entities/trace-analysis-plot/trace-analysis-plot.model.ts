import { BaseEntity } from './../../shared';

export class TraceAnalysisPlot implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public type?: string,
        public url?: string,
        public createdDate?: any,
    ) {
    }
}
