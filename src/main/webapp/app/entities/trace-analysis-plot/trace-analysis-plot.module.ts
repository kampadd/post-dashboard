import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../../shared';
import {
    TraceAnalysisPlotService,
    TraceAnalysisPlotPopupService,
    TraceAnalysisPlotComponent,
    TraceAnalysisPlotDetailComponent,
    TraceAnalysisPlotDialogComponent,
    TraceAnalysisPlotPopupComponent,
    TraceAnalysisPlotDeletePopupComponent,
    TraceAnalysisPlotDeleteDialogComponent,
    traceAnalysisPlotRoute,
    traceAnalysisPlotPopupRoute,
} from './';

const ENTITY_STATES = [
    ...traceAnalysisPlotRoute,
    ...traceAnalysisPlotPopupRoute,
];

@NgModule({
    imports: [
        PostDashboardSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TraceAnalysisPlotComponent,
        TraceAnalysisPlotDetailComponent,
        TraceAnalysisPlotDialogComponent,
        TraceAnalysisPlotDeleteDialogComponent,
        TraceAnalysisPlotPopupComponent,
        TraceAnalysisPlotDeletePopupComponent,
    ],
    entryComponents: [
        TraceAnalysisPlotComponent,
        TraceAnalysisPlotDialogComponent,
        TraceAnalysisPlotPopupComponent,
        TraceAnalysisPlotDeleteDialogComponent,
        TraceAnalysisPlotDeletePopupComponent,
    ],
    providers: [
        TraceAnalysisPlotService,
        TraceAnalysisPlotPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardTraceAnalysisPlotModule {}
