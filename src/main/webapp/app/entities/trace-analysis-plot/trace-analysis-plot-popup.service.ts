import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { TraceAnalysisPlotService } from './trace-analysis-plot.service';

@Injectable()
export class TraceAnalysisPlotPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private traceAnalysisPlotService: TraceAnalysisPlotService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.traceAnalysisPlotService.find(id)
                    .subscribe((traceAnalysisPlotResponse: HttpResponse<TraceAnalysisPlot>) => {
                        const traceAnalysisPlot: TraceAnalysisPlot = traceAnalysisPlotResponse.body;
                        traceAnalysisPlot.createdDate = this.datePipe
                            .transform(traceAnalysisPlot.createdDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.traceAnalysisPlotModalRef(component, traceAnalysisPlot);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.traceAnalysisPlotModalRef(component, new TraceAnalysisPlot());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    traceAnalysisPlotModalRef(component: Component, traceAnalysisPlot: TraceAnalysisPlot): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.traceAnalysisPlot = traceAnalysisPlot;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
