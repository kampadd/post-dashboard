import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TraceAnalysisPlot>;

@Injectable()
export class TraceAnalysisPlotService {

    private resourceUrl =  SERVER_API_URL + 'api/trace-analysis-plots';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(traceAnalysisPlot: TraceAnalysisPlot): Observable<EntityResponseType> {
        const copy = this.convert(traceAnalysisPlot);
        return this.http.post<TraceAnalysisPlot>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(traceAnalysisPlot: TraceAnalysisPlot): Observable<EntityResponseType> {
        const copy = this.convert(traceAnalysisPlot);
        return this.http.put<TraceAnalysisPlot>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<TraceAnalysisPlot>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TraceAnalysisPlot[]>> {
        const options = createRequestOption(req);
        return this.http.get<TraceAnalysisPlot[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TraceAnalysisPlot[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TraceAnalysisPlot = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TraceAnalysisPlot[]>): HttpResponse<TraceAnalysisPlot[]> {
        const jsonResponse: TraceAnalysisPlot[] = res.body;
        const body: TraceAnalysisPlot[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TraceAnalysisPlot.
     */
    private convertItemFromServer(traceAnalysisPlot: TraceAnalysisPlot): TraceAnalysisPlot {
        const copy: TraceAnalysisPlot = Object.assign({}, traceAnalysisPlot);
        copy.createdDate = this.dateUtils
            .convertDateTimeFromServer(traceAnalysisPlot.createdDate);
        return copy;
    }

    /**
     * Convert a TraceAnalysisPlot to a JSON which can be sent to the server.
     */
    private convert(traceAnalysisPlot: TraceAnalysisPlot): TraceAnalysisPlot {
        const copy: TraceAnalysisPlot = Object.assign({}, traceAnalysisPlot);

        copy.createdDate = this.dateUtils.toDate(traceAnalysisPlot.createdDate);
        return copy;
    }
}
