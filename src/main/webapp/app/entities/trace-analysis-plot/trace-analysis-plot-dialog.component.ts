import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { TraceAnalysisPlotPopupService } from './trace-analysis-plot-popup.service';
import { TraceAnalysisPlotService } from './trace-analysis-plot.service';

@Component({
    selector: 'jhi-trace-analysis-plot-dialog',
    templateUrl: './trace-analysis-plot-dialog.component.html'
})
export class TraceAnalysisPlotDialogComponent implements OnInit {

    traceAnalysisPlot: TraceAnalysisPlot;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private traceAnalysisPlotService: TraceAnalysisPlotService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.traceAnalysisPlot.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traceAnalysisPlotService.update(this.traceAnalysisPlot));
        } else {
            this.subscribeToSaveResponse(
                this.traceAnalysisPlotService.create(this.traceAnalysisPlot));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TraceAnalysisPlot>>) {
        result.subscribe((res: HttpResponse<TraceAnalysisPlot>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TraceAnalysisPlot) {
        this.eventManager.broadcast({ name: 'traceAnalysisPlotListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-trace-analysis-plot-popup',
    template: ''
})
export class TraceAnalysisPlotPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceAnalysisPlotPopupService: TraceAnalysisPlotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.traceAnalysisPlotPopupService
                    .open(TraceAnalysisPlotDialogComponent as Component, params['id']);
            } else {
                this.traceAnalysisPlotPopupService
                    .open(TraceAnalysisPlotDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
