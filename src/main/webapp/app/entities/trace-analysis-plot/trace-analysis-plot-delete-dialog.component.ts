import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { TraceAnalysisPlotPopupService } from './trace-analysis-plot-popup.service';
import { TraceAnalysisPlotService } from './trace-analysis-plot.service';

@Component({
    selector: 'jhi-trace-analysis-plot-delete-dialog',
    templateUrl: './trace-analysis-plot-delete-dialog.component.html'
})
export class TraceAnalysisPlotDeleteDialogComponent {

    traceAnalysisPlot: TraceAnalysisPlot;

    constructor(
        private traceAnalysisPlotService: TraceAnalysisPlotService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.traceAnalysisPlotService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'traceAnalysisPlotListModification',
                content: 'Deleted an traceAnalysisPlot'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-trace-analysis-plot-delete-popup',
    template: ''
})
export class TraceAnalysisPlotDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private traceAnalysisPlotPopupService: TraceAnalysisPlotPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.traceAnalysisPlotPopupService
                .open(TraceAnalysisPlotDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
