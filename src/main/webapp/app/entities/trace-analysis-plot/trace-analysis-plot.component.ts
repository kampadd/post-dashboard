import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { TraceAnalysisPlotService } from './trace-analysis-plot.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-trace-analysis-plot',
    templateUrl: './trace-analysis-plot.component.html'
})
export class TraceAnalysisPlotComponent implements OnInit, OnDestroy {
traceAnalysisPlots: TraceAnalysisPlot[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private traceAnalysisPlotService: TraceAnalysisPlotService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.traceAnalysisPlotService.query().subscribe(
            (res: HttpResponse<TraceAnalysisPlot[]>) => {
                this.traceAnalysisPlots = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTraceAnalysisPlots();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TraceAnalysisPlot) {
        return item.id;
    }
    registerChangeInTraceAnalysisPlots() {
        this.eventSubscriber = this.eventManager.subscribe('traceAnalysisPlotListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
