import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { TraceAnalysisPlot } from './trace-analysis-plot.model';
import { TraceAnalysisPlotService } from './trace-analysis-plot.service';

@Component({
    selector: 'jhi-trace-analysis-plot-detail',
    templateUrl: './trace-analysis-plot-detail.component.html'
})
export class TraceAnalysisPlotDetailComponent implements OnInit, OnDestroy {

    traceAnalysisPlot: TraceAnalysisPlot;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private traceAnalysisPlotService: TraceAnalysisPlotService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInTraceAnalysisPlots();
    }

    load(id) {
        this.traceAnalysisPlotService.find(id)
            .subscribe((traceAnalysisPlotResponse: HttpResponse<TraceAnalysisPlot>) => {
                this.traceAnalysisPlot = traceAnalysisPlotResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInTraceAnalysisPlots() {
        this.eventSubscriber = this.eventManager.subscribe(
            'traceAnalysisPlotListModification',
            (response) => this.load(this.traceAnalysisPlot.id)
        );
    }
}
