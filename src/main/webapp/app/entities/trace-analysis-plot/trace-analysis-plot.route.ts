import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { TraceAnalysisPlotComponent } from './trace-analysis-plot.component';
import { TraceAnalysisPlotDetailComponent } from './trace-analysis-plot-detail.component';
import { TraceAnalysisPlotPopupComponent } from './trace-analysis-plot-dialog.component';
import { TraceAnalysisPlotDeletePopupComponent } from './trace-analysis-plot-delete-dialog.component';

export const traceAnalysisPlotRoute: Routes = [
    {
        path: 'trace-analysis-plot',
        component: TraceAnalysisPlotComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalysisPlots'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'trace-analysis-plot/:id',
        component: TraceAnalysisPlotDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalysisPlots'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const traceAnalysisPlotPopupRoute: Routes = [
    {
        path: 'trace-analysis-plot-new',
        component: TraceAnalysisPlotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalysisPlots'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trace-analysis-plot/:id/edit',
        component: TraceAnalysisPlotPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalysisPlots'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'trace-analysis-plot/:id/delete',
        component: TraceAnalysisPlotDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'TraceAnalysisPlots'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
