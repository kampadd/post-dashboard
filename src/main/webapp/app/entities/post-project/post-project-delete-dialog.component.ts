import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PostProject } from './post-project.model';
import { PostProjectPopupService } from './post-project-popup.service';
import { PostProjectService } from './post-project.service';

@Component({
    selector: 'jhi-post-project-delete-dialog',
    templateUrl: './post-project-delete-dialog.component.html'
})
export class PostProjectDeleteDialogComponent {

    postProject: PostProject;

    constructor(
        private postProjectService: PostProjectService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.postProjectService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'postProjectListModification',
                content: 'Deleted an postProject'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-post-project-delete-popup',
    template: ''
})
export class PostProjectDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private postProjectPopupService: PostProjectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.postProjectPopupService
                .open(PostProjectDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
