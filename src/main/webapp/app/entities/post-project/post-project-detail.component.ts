import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PostProject } from './post-project.model';
import { PostProjectService } from './post-project.service';

@Component({
    selector: 'jhi-post-project-detail',
    templateUrl: './post-project-detail.component.html'
})
export class PostProjectDetailComponent implements OnInit, OnDestroy {

    postProject: PostProject;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private postProjectService: PostProjectService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPostProjects();
    }

    load(id) {
        this.postProjectService.find(id)
            .subscribe((postProjectResponse: HttpResponse<PostProject>) => {
                this.postProject = postProjectResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPostProjects() {
        this.eventSubscriber = this.eventManager.subscribe(
            'postProjectListModification',
            (response) => this.load(this.postProject.id)
        );
    }
}
