import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PostProjectComponent } from './post-project.component';
import { PostProjectDetailComponent } from './post-project-detail.component';
import { PostProjectPopupComponent } from './post-project-dialog.component';
import { PostProjectDeletePopupComponent } from './post-project-delete-dialog.component';

export const postProjectRoute: Routes = [
    {
        path: 'post-project',
        component: PostProjectComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PostProjects'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'post-project/:id',
        component: PostProjectDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PostProjects'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const postProjectPopupRoute: Routes = [
    {
        path: 'post-project-new',
        component: PostProjectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PostProjects'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'post-project/:id/edit',
        component: PostProjectPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PostProjects'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'post-project/:id/delete',
        component: PostProjectDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'PostProjects'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
