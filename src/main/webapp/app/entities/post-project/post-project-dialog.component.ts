import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PostProject } from './post-project.model';
import { PostProjectPopupService } from './post-project-popup.service';
import { PostProjectService } from './post-project.service';

@Component({
    selector: 'jhi-post-project-dialog',
    templateUrl: './post-project-dialog.component.html'
})
export class PostProjectDialogComponent implements OnInit {

    postProject: PostProject;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private postProjectService: PostProjectService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.postProject.id !== undefined) {
            this.subscribeToSaveResponse(
                this.postProjectService.update(this.postProject));
        } else {
            this.subscribeToSaveResponse(
                this.postProjectService.create(this.postProject));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PostProject>>) {
        result.subscribe((res: HttpResponse<PostProject>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PostProject) {
        this.eventManager.broadcast({ name: 'postProjectListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-post-project-popup',
    template: ''
})
export class PostProjectPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private postProjectPopupService: PostProjectPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.postProjectPopupService
                    .open(PostProjectDialogComponent as Component, params['id']);
            } else {
                this.postProjectPopupService
                    .open(PostProjectDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
