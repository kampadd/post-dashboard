import { BaseEntity } from './../../shared';
import { Trace } from '../trace';

export class PostProject implements BaseEntity {
    constructor(
        public id?: string,
        public name?: string,
        public postTraces?: Trace[],
        public metadata?: any,
    ) {
    }
}

export class PostFile {
    constructor(
        public id?: string,
        public name?: string,
        public path?: string,
        public files?: PostFile[],
    ) {
    }
}
