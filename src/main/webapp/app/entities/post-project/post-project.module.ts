import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../../shared';
import {
    PostProjectService,
    PostProjectPopupService,
    PostProjectComponent,
    PostProjectDetailComponent,
    PostProjectDialogComponent,
    PostProjectPopupComponent,
    PostProjectDeletePopupComponent,
    PostProjectDeleteDialogComponent,
    postProjectRoute,
    postProjectPopupRoute,
} from './';

const ENTITY_STATES = [
    ...postProjectRoute,
    ...postProjectPopupRoute,
];

@NgModule({
    imports: [
        PostDashboardSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PostProjectComponent,
        PostProjectDetailComponent,
        PostProjectDialogComponent,
        PostProjectDeleteDialogComponent,
        PostProjectPopupComponent,
        PostProjectDeletePopupComponent,
    ],
    entryComponents: [
        PostProjectComponent,
        PostProjectDialogComponent,
        PostProjectPopupComponent,
        PostProjectDeleteDialogComponent,
        PostProjectDeletePopupComponent,
    ],
    providers: [
        PostProjectService,
        PostProjectPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardPostProjectModule {}
