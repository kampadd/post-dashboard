import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { PostFile, PostProject } from './post-project.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PostProject>;

@Injectable()
export class PostProjectService {

    private resourceUrl =  SERVER_API_URL + 'api/post-projects';

    constructor(private http: HttpClient) { }

    path(path?: string): Observable<EntityResponseType> {
        return this.http.get<PostFile>(`${this.resourceUrl}/path/${path}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertPostFileResponse(res));
    }

    clean(path: string): Observable<EntityResponseType> {
        return this.http.get(`${this.resourceUrl}/clean/${path}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertPostFileResponse(res));
    }

    init(project?: string): Observable<EntityResponseType> {
        return this.http.get<PostProject>(`${this.resourceUrl}/init/${project}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    display(): Observable<HttpResponse<PostProject[]>> {
        return this.http.get<PostProject[]>(`${this.resourceUrl}/display`, { observe: 'response' })
            .map((res: HttpResponse<PostProject[]>) => this.convertArrayResponse(res));
    }

    load(project?: string): Observable<HttpResponse<PostProject[]>> {
        return this.http.get<PostProject[]>(this.resourceUrl, { observe: 'response' })
            .map((res: HttpResponse<PostProject[]>) => this.convertArrayResponse(res));
    }

    create(postProject: PostProject): Observable<EntityResponseType> {
        const copy = this.convert(postProject);
        return this.http.post<PostProject>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(postProject: PostProject): Observable<EntityResponseType> {
        const copy = this.convert(postProject);
        return this.http.put<PostProject>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<PostProject>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PostProject[]>> {
        const options = createRequestOption(req);
        return this.http.get<PostProject[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PostProject[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertPostFileResponse(res: EntityResponseType): EntityResponseType {
        const body: PostFile = Object.assign({}, res.body);
        return res.clone({body});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PostProject = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<PostProject[]>): HttpResponse<PostProject[]> {
        const jsonResponse: PostProject[] = res.body;
        const body: PostProject[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to PostProject.
     */
    private convertItemFromServer(postProject: PostProject): PostProject {
        const copy: PostProject = Object.assign({}, postProject);
        return copy;
    }

    /**
     * Convert a PostProject to a JSON which can be sent to the server.
     */
    private convert(postProject: PostProject): PostProject {
        const copy: PostProject = Object.assign({}, postProject);
        return copy;
    }
}
