import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PostProject } from './post-project.model';
import { PostProjectService } from './post-project.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-post-project',
    templateUrl: './post-project.component.html'
})
export class PostProjectComponent implements OnInit, OnDestroy {
postProjects: PostProject[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private postProjectService: PostProjectService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.postProjectService.query().subscribe(
            (res: HttpResponse<PostProject[]>) => {
                this.postProjects = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInPostProjects();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PostProject) {
        return item.id;
    }
    registerChangeInPostProjects() {
        this.eventSubscriber = this.eventManager.subscribe('postProjectListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
