import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PostDashboardTraceAnalysisModule } from './trace-analysis/trace-analysis.module';
import { PostDashboardTraceLogModule } from './trace-log/trace-log.module';
import { PostDashboardTraceModule } from './trace/trace.module';
import { PostDashboardTraceAnalysisPlotModule } from './trace-analysis-plot/trace-analysis-plot.module';
import { PostDashboardPostProjectModule } from './post-project/post-project.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        PostDashboardTraceAnalysisModule,
        PostDashboardTraceLogModule,
        PostDashboardTraceModule,
        PostDashboardTraceAnalysisPlotModule,
        PostDashboardPostProjectModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardEntityModule {}
