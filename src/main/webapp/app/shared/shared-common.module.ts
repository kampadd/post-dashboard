import { NgModule, LOCALE_ID } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import locale from '@angular/common/locales/en';

import {
    PostDashboardSharedLibsModule,
    JhiAlertComponent,
    JhiAlertErrorComponent
} from './';
import { DataService } from './data/data.service';

@NgModule({
    imports: [
        PostDashboardSharedLibsModule
    ],
    declarations: [
        JhiAlertComponent,
        JhiAlertErrorComponent
    ],
    providers: [
        Title,
        {
            provide: LOCALE_ID,
            useValue: 'en'
        },
        DataService,
    ],
    exports: [
        PostDashboardSharedLibsModule,
        JhiAlertComponent,
        JhiAlertErrorComponent
    ]
})
export class PostDashboardSharedCommonModule {
    constructor() {
        registerLocaleData(locale);
    }
}
