import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators/tap';

@Injectable()
export class DataService {
    constructor(private http: HttpClient) { }
    find(url: string): Observable<any> {
        // Typical use case for non-json data
        return this.http.get(url, { observe: 'response', responseType: 'text'})
        .pipe(
            tap(
                (data) => this.log(url, data),
                (error) => this.logerror(url, error)
            )
        );
    }
    log(url: string, res: any) {
        console.log('#> success results');
        console.log('url: ' + url);
       // console.log(res);
    }
    logerror(url: string, error: any) {
        console.log('#> error');
        console.log('url: ' + url);
        console.log(error);
    }
}
