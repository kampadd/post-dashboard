import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import {PostFile, PostProject, PostProjectService } from '../../entities/post-project';
import { JhiEventManager } from 'ng-jhipster';
import { Trace, TraceService } from '../../entities/trace';
import { LocalStorage } from 'ngx-webstorage';

@Component({
    selector: 'jhi-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    showSidebar = false;
    eventSubscriber: Subscription;
    @Input() projects: PostProject[];
    traces: Trace[];
    showList = false;
    showDisplay = false;
    postFiles: PostFile[];
    isPostFiles: boolean;
    isProject: boolean;
    showUploader = false;
    title: string;
    isLoading = true;
    @LocalStorage() selected: string;
    @LocalStorage() project: string;

    constructor(private eventManager: JhiEventManager) {
                    this.projects = [];
                    this.title = '';
    }

    ngOnInit() {
        console.log('selected: ' + this.selected);
        this.registerEvents();
    }

    registerEvents() {
        this.eventSubscriber = this.eventManager.subscribe('tmf-list', (response) => {
            console.log('tmf-list event in sidebar');
            console.log(response);
            this.postFiles = response.content;
            console.log(this.postFiles);
            this.isPostFiles = true;
            this.isProject = false;
            this.showSidebar = true;
            this.showUploader = false;
            this.title = 'List';
            if (this.selected !== null) {
                console.log('project: ' + this.project);
                console.log('selected: ' + this.selected);
                const pFile = this.findSelectedFolder(this.selected, this.project);
                if (pFile) {
                    this.onPathSelected(pFile, this.project);
                }
            }
        });
        this.eventSubscriber = this.eventManager.subscribe('tmf-clean', () => {
            console.log('tmf-clean event in sidebar');
            this.isPostFiles = false;
            this.isProject = false;
            this.showSidebar = false;
            this.showUploader = false;
            this.title = 'Clean';
            this.isLoading = false;
        });
        this.eventSubscriber = this.eventManager.subscribe('tmf-init', () => {
            console.log('tmf-init event in sidebar');
            this.isPostFiles = false;
            this.isProject = false;
            this.showSidebar = false;
            this.showUploader = false;
            this.title = 'Init';
            this.isLoading = false;
        });
        this.eventSubscriber = this.eventManager.subscribe('tmf-display', (response) => {
            console.log('tmf-display event in sidebar');
            this.projects = response.content;
            this.isPostFiles = false;
            this.isProject = true;
            this.showSidebar = true;
            this.showUploader = false;
            this.title = 'Display';
            if (this.selected !== null) {
                console.log('selected: ' + this.selected);
                console.log('project: ' + this.project);
                const trace = this.findSelectedTrace(this.selected, this.project);
                if (trace) {
                    this.onTraceSelected(trace, this.project);
                }
            }
        });
        this.eventSubscriber = this.eventManager.subscribe('tmf-upload', (response) => {
            console.log('tmf-upload event in sidebar');
            this.isPostFiles = false;
            this.isProject = false;
            this.showSidebar = false;
            this.showUploader = true;
            this.isLoading = false;
            console.log('isloading: ' + this.isLoading);
        });
        this.eventSubscriber = this.eventManager.subscribe('cancel-upload', (response) => {
            console.log('cancel-upload event in sidebar');
            this.cancelUpload();
        });
        this.eventManager.subscribe('headerEvent', () => {
            console.log('header-event panel');
            this.showSidebar = false;
            this.isLoading = true;
        });
        this.eventManager.subscribe('loading-completed', () => {
            console.log('header-event loading-completed');
            this.isLoading = false;
        });
    }

    findSelectedTrace(traceName: string, projectName: string): Trace {
        let trace: Trace = null;
        this.projects.forEach((project) => {
            if (project.name.indexOf(projectName) > -1) {
                project.postTraces.forEach((pTrace) => {
                    if (pTrace.name.indexOf(traceName) > -1) {
                        trace = pTrace;
                    }
                });
            }
        });
        return trace;
    }

    findSelectedFolder(folderName: string, projectName: string): PostFile {
        let postFile: PostFile = null;
            this.postFiles.forEach((pFile) => {
                console.log(projectName);
                console.log(pFile);
                if (pFile.name.indexOf(projectName) > -1) {
                    pFile.files.forEach((file) => {
                        console.log(file);
                        if (file.name.indexOf(folderName) > -1) {
                            postFile = file;
                        }
                        console.log('postFile:');
                        console.log(postFile);
                    });
                }
            });
        return postFile;
    }

    cancelUpload() {
        this.showUploader = false;
        this.eventManager.broadcast({ name: 'initialise', content: 'OK'});
    }

    toggleSidebar() {
        this.showSidebar = !this.showSidebar;
    }

    onTraceSelected(trace, project) {
        this.showSidebar = false;
        console.log('sidebar-trace-selected');
        console.log(trace);
        this.broadcastSelectedProject(trace.name, project);
        this.eventManager.broadcast({
            name: 'headerChanged',
            content: trace.name
        });
        this.eventManager.broadcast({
            name: 'traceSelected',
            content: trace
        });
    }

    traceId(index: number, item: Trace) {
        return item.id;
    }

    projectId(index: number, item: PostProject) {
        return item.id;
    }

    onPathSelected(postFile: PostFile, project: string) {
        console.log('sidebar-path-selected');
        console.log(postFile);
        this.toggleSidebar();
        this.broadcastSelectedProject(postFile.name, project);
        this.eventManager.broadcast({
            name: 'headerChanged',
            content: postFile.name
        });
        this.eventManager.broadcast({
            name: 'pathSelected',
            content: postFile
        });
    }

    broadcastSelectedProject(trace: string, project: string) {
        this.selected = trace;
        this.project = project;
        console.log('set-selected: ' + this.selected);
        console.log('set-project: ' + this.project);
    }

    toggleUploader() {
        this.showUploader = !this.showUploader;
    }

}
