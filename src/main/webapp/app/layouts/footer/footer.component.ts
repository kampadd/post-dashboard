import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';

@Component({
    selector: 'jhi-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    isFixedBottom = true;
    constructor(private eventManager: JhiEventManager) {}

    ngOnInit() {
        this.eventManager.subscribe('traceSelected', () => {
            this.isFixedBottom = false;
        });
    }

}
