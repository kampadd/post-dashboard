import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Principal } from '../../shared';
import { Trace, TraceService } from '../../entities/trace';
import {PostProject, PostProjectService } from '../../entities/post-project';

@Component({
    selector: 'jhi-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    @Input() traces: Trace[];
    title: string;
    currentAccount: any;
    eventSubscriber: Subscription;
    headerSubscriber: Subscription;
    project: PostProject;
    showProjectNamePanel: boolean;
    projects: PostProject[];
    projectName: string;
    active: string;
    errorMessage: string;

    constructor(
        private traceService: TraceService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal,
        private projectService: PostProjectService,
    ) {
        this.initialise();
        this.errorMessage = null;
    }

    isActive(item: string) {
        return (this.active.indexOf(item) > -1);
    }

    initialise() {
        this.title = 'Trace Monitoring Framework';
        this.projects = [];
        this.projectName = null;
        this.active = 'none';
    }

    headerEvent() {
        console.log('Broadcast header change');
        this.eventManager.broadcast({
            name: 'headerEvent',
            content: 'header-changed'
        });
    }

    init() {
        console.log('tmf-header-init-called');
        this.errorMessage = null;
        this.active = 'init';
        this.title = 'TMF-INIT';
        if (this.projectName === null) {
            console.log('project-name is null');
            this.showProjectNamePanel = true;
            this.eventManager.broadcast({
                name: 'tmf-init',
                content: 'OK'
            });
        } else {
            console.log('project-name: ' + this.projectName);
            this.showProjectNamePanel = false;
            this.projectService.init(this.projectName).subscribe((response) => {
                this.projectName = null;
                this.project = response.body;
                this.projects.push(this.project);
                this.eventManager.broadcast({
                    name: 'tmf-display',
                    content: this.projects
                });
                this.eventManager.broadcast({
                    name: 'projectSelected',
                    content: this.project
                });
            });
        }
    }

    list() {
        console.log('trace-header-list-called');
        this.active = 'list';
        this.title = 'TMF-LIST';
        this.showProjectNamePanel = false;
        this.headerEvent();
        this.projectService.display().subscribe((response) => {
            this.eventManager.broadcast({
                name: 'tmf-list',
                content: response.body
            });
            this.loadingCompleted();
        });
    }

    display() {
        console.log('trace-header-display-called');
        this.active = 'display';
        this.title = 'TMF-DISPLAY';
        this.showProjectNamePanel = false;
        this.headerEvent();
        this.projectService.query({}).subscribe((response) => {
            this.projects = response.body;
            console.log(this.projects);
            this.eventManager.broadcast({
                name: 'tmf-display',
                content: this.projects
            });
            this.loadingCompleted();
        });
    }

    loadingCompleted() {
        this.eventManager.broadcast({ name: 'loading-completed', content: 'OK'});
    }

    clean() {
        console.log('trace-header-clean-called');
        this.title = 'TMF-CLEAN';
        this.active = 'clean';
        this.headerEvent();
        if (this.projectName === null) {
            console.log('project-name is null');
            this.list();
            this.showProjectNamePanel = true;
            this.active = 'clean';
            this.title = 'TMF-CLEAN';
        } else {
            console.log('project-name: ' + this.projectName);
            this.errorMessage = null;
            const projects = this.projects.filter((_project) => _project.name === this.projectName );
            if (projects.length > 0) {
                this.showProjectNamePanel = false;
                this.projectService.clean(this.projectName).subscribe(() => {
                    this.projectName = null;
                    this.list();
                    this.loadingCompleted();
                    this.title = 'TMF-CLEAN';
                    this.active = 'clean';
                });
            } else {
                this.errorMessage = this.projectName + ' not found!';
                this.projectName = null;
                this.clean();
            }
        }
    }

    upload() {
        console.log('trace-header-upload-called');
        this.active = 'upload';
        this.title = 'TMF-UPLOAD';
        this.showProjectNamePanel = false;
        this.headerEvent();
        this.eventManager.broadcast({
            name: 'tmf-upload',
            content: 'open'
        });
    }

    loadAll() {
        this.projectService.query().subscribe(
            (res: HttpResponse<Trace[]>) => {
                this.projects = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTraces();
        this.headerChanged();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Trace) {
        return item.id;
    }

    registerChangeInTraces() {
        this.eventSubscriber = this.eventManager.subscribe('projectListModification', (response) => this.loadAll());
    }

    headerChanged() {
        this.eventManager.subscribe('initialise', () => {
            this.initialise();
        });
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
