import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';
import { Account, LoginModalService, Principal } from '../shared';
import { HomeService } from './home.service';
import { DomSanitizer, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { Trace } from '../entities/trace';
import { TraceAnalysis } from '../entities/trace-analysis';
import { TraceAnalysisPlot } from '../entities/trace-analysis-plot';
import { PostFile } from '../entities/post-project/post-project.model';

@Component({
    selector: 'jhi-home',
    templateUrl: './home.component.html',
    styleUrls: [
        'home.scss'
    ],
    providers: [HomeService]

})
export class HomeComponent implements OnInit {
    account: Account;
    modalRef: NgbModalRef;
    imageStaticUrl = '../../svgs/config-00-taskset.svg';
    svgTraceFile = '/svgs/trace.svg';
    messageFlow: any;
    traceData: any;
    filter: any;
    trace: Trace;
    analysis: TraceAnalysis;
    plots: TraceAnalysisPlot[];
    htmlPlots: TraceAnalysisPlot[];
    canvasPlots: TraceAnalysisPlot[];
    imagePlots: TraceAnalysisPlot[];
    traceLoaded: boolean;
    canvasHTMLPlotterUrl: SafeResourceUrl;
    plotterURL: string;
    panelReady: boolean;
    displayView: boolean;
    listView: boolean;
    postFile: PostFile;
    loading: boolean;
    fileReady: boolean;
    postFileSelected: PostFile;

    constructor(
        private principal: Principal,
        private jhiAlertService: JhiAlertService,
        private loginModalService: LoginModalService,
        private eventManager: JhiEventManager,
        private domSanitizer: DomSanitizer,
    ) {
        this.listView = false;
        this.displayView = false;
        this.loading = false;
        this.panelReady = true;
        this.fileReady = false;
        this.postFile = null;
    }

    ngOnInit() {
        this.principal.identity().then((account) => {
            this.account = account;
        });
        this.registerAuthenticationSuccess();
        this.registerListView();
        this.registerDisplayView();
        this.registerHeaderEvent();
        this.registerLoadingCompleted();
    }

    onFileSelected(event: any) {
        console.log('file-selected');
        console.log(event);
        this.postFileSelected = new PostFile('post-file-view', event.text, event.value, []);
        this.eventManager.broadcast({
            name: 'file-selected',
            content: this.postFileSelected
        });
    }

    registerAuthenticationSuccess() {
        this.eventManager.subscribe('authenticationSuccess', (message) => {
            this.principal.identity().then((account) => {
                this.account = account;
            });
        });
    }

    registerDisplayView() {
        this.eventManager.subscribe('traceSelected', (trace) => {
            console.log('loading trace selected...');
            console.log(trace);
            this.trace = Object.assign({}, trace.content);
            console.log('trace');
            console.log(this.trace);
            this.analysis = this.trace.analysis;
            console.log('analysis');
            console.log(this.analysis);
            this.plots = this.analysis.plots;
            console.log('plots');
            console.log(this.plots);
            this.imagePlots = [];
            this.htmlPlots = [];
            this.canvasPlots = [];
            this.plots.forEach( (plot) => {
                console.log('checking type: ' + plot.type);
                if (plot.type.indexOf('js') > -1) {
                    console.log('JS Canvas-Plot: ');
                    console.log(plot);
                    this.canvasPlots.push(plot);
                }
                if (plot.type.indexOf('html') > -1) {
                    console.log('HTML Canvas-Plot: ');
                    console.log(plot);
                    this.htmlPlots.push(plot);
                }
                if (plot.type.indexOf('svg') > -1 || plot.type.indexOf('png') > -1 || plot.type.indexOf('jpg') > -1) {
                    this.imagePlots.push(plot);
                }
            });
            console.log('canvas-html-plots');
            console.log(this.htmlPlots);
            console.log('canvas-js-plots');
            console.log(this.canvasPlots);
            console.log('image-plots');
            console.log(this.imagePlots);
            this.displayView = true;
            this.listView = false;
            this.loading = false;
        });
    }

    registerListView() {
        this.eventManager.subscribe('pathSelected', (pathSelected) => {
            console.log(pathSelected);
            this.postFile = Object.assign({}, pathSelected.content);
            this.displayView = false;
            this.listView = true;
            this.loading = false;
            this.panelReady = false;
        });
        this.eventManager.subscribe('file-ready', () => {
            this.fileReady = true;
        });
    }

    registerLoadingCompleted() {
        this.eventManager.subscribe('loading-completed', () => {
            console.log('loading complete');
            this.loading = false;
            this.jhiAlertService.info('loading-completed');
        });
    }

    registerHeaderEvent() {
        this.eventManager.subscribe('headerEvent', () => {
            console.log('clearing panel');
            this.panelReady = false;
            this.displayView = false;
            this.listView = false;
            this.loading = true;
        });
    }

    randomGif() {
        const num = Math.floor(Math.random() * Math.floor(4));
        let url = '/content/images/0.gif';
        switch (num) {
            case 0:
                url = '/content/images/0.gif';
                break;
            case 1:
                url = '/content/images/1.gif';
                break;
            case 2:
                url = '/content/images/2.gif';
                break;
            case 3:
                url = '/content/images/3.gif';
                break;
            default:
                url = '/content/images/0.gif';
                break;
        }
        return this.getSafeUrl(url);
    }

    loadingGif() {
        const num = Math.floor(Math.random() * Math.floor(2));
        let url = '/content/images/0.gif';
        switch (num) {
            case 0:
                url = '/content/images/loading-0.gif';
                break;
            case 1:
                url = '/content/images/loading-1.gif';
                break;
            default:
                url = '/content/images/0.gif';
                break;
        }
        return this.getSafeUrl(url);
    }

    getSafeUrl(url: string): SafeResourceUrl {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }

    isAuthenticated() {
        return this.principal.isAuthenticated();
    }

    login() {
        this.modalRef = this.loginModalService.open();
    }

    filterSelected(filter: any) {
        console.log('filter-selected');
        console.log(filter);
        this.filter = filter.x;
    }

    plotId(index: number, plot: TraceAnalysisPlot) {
        return plot.id;
    }

    htmlPlotId(index: number, plot: TraceAnalysisPlot) {
        return plot.id;
    }

    canvasPlotId(index: number, plot: TraceAnalysisPlot) {
        return plot.id;
    }
}
