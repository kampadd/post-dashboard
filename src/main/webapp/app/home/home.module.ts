import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PostDashboardSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import { WidgetsModule } from '../widgets/widgets.module';
import { PostDashboardEntityModule } from '../entities/entity.module';
import { PostDashboardTraceModule } from '../entities/trace/trace.module';

@NgModule({
    imports: [
        PostDashboardSharedModule,
        WidgetsModule,
        PostDashboardEntityModule,
        PostDashboardTraceModule,
        RouterModule.forChild([ HOME_ROUTE ])
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PostDashboardHomeModule {}
