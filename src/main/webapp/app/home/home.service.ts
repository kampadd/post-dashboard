import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpParams, } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HomeService {

  constructor(private http: HttpClient, ) { }

  getResource(sourceFile: string, type?: HttpHeaders): Observable<any> {
    console.log('get resource:' + sourceFile);
    const jsonHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
    const rtype = type || jsonHeaders;
    return this.http.get(sourceFile, { headers: rtype, responseType: 'text' });
    /**
    return this.http.get(sourceFile, { headers: rtype, responseType: 'text' }).map((res: HttpResponse) => {
      console.log(res);
      const payload = res.body;
      console.log(payload);
      return payload;
    }, (res: HttpErrorResponse) => {
      console.log(res);
      return res.error.text;
    });
     */
  }

}
