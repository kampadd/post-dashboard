import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule  } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularOpenlayersModule } from 'ngx-openlayers';
import { PostUploaderComponent,
        NGXPostTreeviewComponent,
        PostTreeviewComponent,
        MapComponent,
        SVGViewerComponent,
        D3SvgViewerComponent,
        CanvasViewerComponent,
        TracerComponent,
        FileviewerComponent} from '.';
import { FormsModule } from '@angular/forms';
import { TreeviewModule } from 'ngx-treeview';
import { TreeModule } from 'angular-tree-component';
import { HighlightModule } from 'ngx-highlightjs';
import { NgxUploaderModule } from 'ngx-uploader';
import { PostDashboardSharedCommonModule, DataService } from '../shared';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        AngularOpenlayersModule,
        FormsModule,
        TreeviewModule.forRoot(),
        TreeModule.forRoot(),
        NgxUploaderModule,
        HighlightModule.forRoot({ theme: 'sunburst', path: 'content/js/highlight' }),
        PostDashboardSharedCommonModule
    ],
    declarations: [
        PostUploaderComponent,
        NGXPostTreeviewComponent,
        PostTreeviewComponent,
        TracerComponent,
        CanvasViewerComponent,
        MapComponent,
        SVGViewerComponent,
        D3SvgViewerComponent,
        FileviewerComponent
        ],
    exports: [
        PostUploaderComponent,
        NGXPostTreeviewComponent,
        PostTreeviewComponent,
        TracerComponent,
        CanvasViewerComponent,
        D3SvgViewerComponent,
        SVGViewerComponent,
        MapComponent,
        NgbModule,
        NgxUploaderModule,
        FileviewerComponent
    ],
    providers: [DataService],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class WidgetsModule {
}
