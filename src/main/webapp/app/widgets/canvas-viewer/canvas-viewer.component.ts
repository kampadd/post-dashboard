import { EventEmitter, OnInit, Component, Input, HostListener, Output, OnDestroy,
     ViewContainerRef, ViewChild, ViewRef, TemplateRef, AfterViewInit} from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { DataService } from '../../shared';

declare var gnuplot;
declare function POSTCanvasPlotter(): void;

@Component({
    selector: 'jhi-canvas',
    templateUrl: './canvas-viewer.component.html',
    styleUrls: ['./canvas-viewer.component.scss', '../../../content/js/gnuplot_mouse.css'],
    providers: [DataService]
})
export class CanvasViewerComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() width = 1680;
    @Input() height= 680;
    @Input() plotterURL: string;
    postCanvasPlotter: any;
    pointX: number;
    pointY: number;
    clickedX: string;
    clickedY: string;
    created: boolean;
    @Output() itemSelected: EventEmitter<any> = new EventEmitter<any>();
    @Output() canvasCreated: EventEmitter<any> = new EventEmitter<any>();
    canvasModule: any;
    loadAPI: Promise<any>;
    isCanvasReady: boolean;
    basePath: string;
    dataFilePath: string;
    data: string[];
    @Input() plotter: any;
    scripter: any;
    postCanvas: any;
    canvasScriptId = 'plotscript';
    postDashboardId = 'postdashboard';
    @ViewChild('canvasContainer', {read: ViewContainerRef}) canvasContainer: ViewContainerRef;
    @ViewChild('canvasTemplate', {read: TemplateRef}) canvasTemplate: TemplateRef<any>;
    @ViewChild('canvasView') canvasView: ViewRef;
    constructor(private eventManager: JhiEventManager, private dataService: DataService) {
        console.log('On-Constructor');
        this.scripter = document.getElementById(this.canvasScriptId);
        console.log(this.scripter);
        document.getElementById(this.postDashboardId).removeChild(this.scripter);
        this.isCanvasReady = false;
    }

    ngOnDestroy() {
        this.canvasContainer.remove();
    }

    ngOnInit(): void {
        console.log('On-Init');
        console.log('PlotterURL: ' + this.plotterURL);
        this.initCanvas();
    }

    ngAfterViewInit() {
        // this.canvasCreated.emit('canvas-created');
        setTimeout(() => {
        try {
            POSTCanvasPlotter();
            gnuplot.init();
            console.log('canvas initialized.');
        }catch (e) {
            console.log(e);
        }} , 200);
    }

    initCanvas() {
        if (this.plotterURL !== undefined) {
            this.loadScript();
            this.canvasView = this.canvasTemplate.createEmbeddedView(null);
            this.canvasContainer.insert(this.canvasView);
            console.log('plotter set: ' + this.plotterURL);
            this.basePath = this.plotterURL.substring(0, this.plotterURL.lastIndexOf('\\') + 1);
            console.log('Base path: ' + this.basePath);
            this.dataFilePath = this.basePath + 'config.dat';
            console.log('DataFilePath: ' + this.dataFilePath);
            this.dataService.find(this.dataFilePath).subscribe((text) => {
                console.log(text);
                if (text.body) {
                    this.loadData(text.body);
                }
            });
            this.isCanvasReady = true;
        }
    }

    loadData(text: string) {
        const newline = '\n';
        this.data = String(text).split(newline);
    }

    @HostListener('document:mouseover', ['$event'])
    mouseOver(evt: MouseEvent) {
        console.log('mouseover event');
        evt.preventDefault();
    }
    @HostListener('document:keypress', ['$event'])
    onkeyDown(evt: KeyboardEvent) {
        console.log(evt);
        gnuplot.do_hotkey(evt);
    }

    onContextMenu(evt: MouseEvent): boolean {
        console.log('context-menu');
        console.log(evt);
        return false;
    }
    /**
     * findClosest
     * @param targetVal
     * @param set
     */
    findClosest( targetVal: number, set: number[]) {
      let dif = 100, cand = 0;
      for ( const x in set) {
        if (Math.abs(Number(x) - targetVal) < dif) {
            dif = Math.abs(Number(x) - targetVal);
            cand = Number(x);
        }
      }
      return cand;
  }

  toggle_plot(view: string) {
        gnuplot.toggle_plot(view);
    }

    toggle_grid() {
        console.log('toggle-grid');
        gnuplot.toggle_grid();
        console.log('grid-toggled');
    }

    unzoom() {
        console.log('unzoom');
        gnuplot.unzoom();
        console.log('unzoomed');
    }

    rezoom() {
        console.log('rezoom');
        gnuplot.rezoom();
        console.log('rezoomed');
    }

    loadScript() {
        console.log('<loading-scripts>');
        console.log(this.plotterURL);
        const node = document.createElement('script');
        node.src = this.plotterURL;
        node.type = 'text/javascript';
        node.async = false;
        node.charset = 'utf-8';
        node.id = this.canvasScriptId;
        document.getElementById(this.postDashboardId).appendChild(node);
        this.scripter = document.getElementById(this.canvasScriptId);
        console.log(this.scripter);
        console.log('</loading-scripts>');
    }
}
