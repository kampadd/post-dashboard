import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';

@Component({
    selector: 'jhi-treeview',
    templateUrl: './treeview.component.html',
    styles: ['./treeview.component.scss']
})
export class PostTreeviewComponent implements OnInit {
    config: ITreeOptions;
    @Input() items: any;
    constructor() {
        this.initConfig();
    }

    initConfig() {
      this.config =  {
        displayField: 'name',
        isExpandedField: 'expanded',
        idField: 'id',
        hasChildrenField: 'files',
        };
    }

    ngOnInit() {
        console.log('<treeview>');
        console.log(this.items);
        console.log('</treeview>');
    }

    onSelectedChange(item: any) {
        console.log(item);
    }

}
