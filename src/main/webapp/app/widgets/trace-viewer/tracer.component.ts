import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'jhi-tracer',
    templateUrl: './tracer.component.html',
    styleUrls: ['./tracer.component.scss']
})
export class TracerComponent implements OnInit {
    @Input() traceSource: any[];
    @Input() filter: string;
    filteredTraceData: Trace[];
    config: PageConfig = { maxResults: 100, maxPage: 100, page: 1};
    no_of_pages: number;
    traceData: Trace[] = [];
    filterIsSet = false;
    numberOfRecords = 0;
    constructor(private sanitizer: DomSanitizer) {
        this.filteredTraceData = [];
        this.traceData = [];
    }
    ngOnInit(): void {
        const dataLength = this.traceSource.length;
        this.config.maxResults = dataLength;
        this.numberOfRecords = dataLength;
        this.no_of_pages = Math.ceil( this.config.maxResults / this.config.maxPage );
        this.loadTrace();
        this.updatePage();
        console.log('trace-init');
        this.applyFilter();
    }

    loadTrace() {
        for (let i = 0; i < this.traceSource.length; i++) {
            this.traceData.push(new Trace((i), this.traceSource[i]));
        }
    }

    search() {
        console.log('search');
        console.log(this.filter);
        const _filter = this.filter.toString();
        if (_filter && _filter.length > 0) {
            console.log('filter');
            console.log(_filter);
            console.log('----------');
            this.filterIsSet = true;
            this.filteredTraceData = [];
            const data = this.traceData.filter((item) => item.data.indexOf(_filter) > 0 );
            data.forEach((item, ) => {
                this.filteredTraceData.push(item);
            });
            this.numberOfRecords = data.length;
        }
    }
    unsetFilter() {
        this.filterIsSet = false;
        this.reset();
    }

    applyFilter() {
        if (this.filter !== undefined && this.filter.length > 0) {
            this.filteredTraceData = [];
            const data = this.traceData.filter((item) => item.data.indexOf(this.filter) > 0 );
            data.forEach((item, ) => {
                this.filteredTraceData.push(item);
            });
        }
        this.numberOfRecords = this.filteredTraceData.length;

        if (this.numberOfRecords === 0) {
            this.reset();
        }
    }

    getHtmlLog(log: string) {
        return this.sanitizer.bypassSecurityTrustHtml(log);
    }
    next() {
        if ( (this.config.page + 1) < this.no_of_pages ) {
            ++this.config.page;
        }
        this.updatePage();
    }
    previous() {
        if ((this.config.page - 1) >= 1) {
            --this.config.page;
        }
        this.updatePage();
    }
    goto(page: number) {
        this.config.page = page;
        this.updatePage();
    }

    reset() {
        this.config.page = 1;
        this.updatePage();
    }

    updatePage() {
        const start = (this.config.page !== 1) ? this.config.page * this.config.maxPage : 1;
        const end = start + this.config.maxPage;
        this.filteredTraceData = [];
        for ( let i = start; i < end; i++) {
            this.filteredTraceData.push(this.traceData[i]);
        }
        this.numberOfRecords = this.filteredTraceData.length;
    }
}

export class PageConfig {
    constructor(public page?: number, public maxResults?: number, public maxPage?: number) {}
}

export class Trace {
    constructor(public id?: number, public data?: string) {}
}
