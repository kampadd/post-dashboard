import { Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { PostFile } from '../../entities/post-project';
import { JhiEventManager } from 'ng-jhipster';
import { DataService } from '../../shared';
import { HighlightJS, HighlightOptions } from 'ngx-highlightjs';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'jhi-fileviewer',
    templateUrl: './fileviewer.component.html',
    styleUrls: ['./fileviewer.component.scss'],
    providers: [ DataService ]
})
export class FileviewerComponent implements OnInit, OnDestroy, AfterViewInit {
    content: string;
    fileName: string;
    languages: string[] = ['properties', 'javascript', 'html', 'sql', 'xml'];
    options: HighlightOptions = {theme: 'sunburst'};
    contentReady: boolean;
    loading: boolean;

    constructor(
                private treeviewService: DataService,
                private hljs: HighlightJS,
                private eventManager: JhiEventManager,
                private domSanitizer: DomSanitizer,
                ) {
            this.fileName = null;
        }

        ngAfterViewInit() {
            this.hljs.isReady.subscribe(() => {
                this.hljs.initHighlightingOnLoad();
            });
        }

        ngOnDestroy() {
        }

        ngOnInit() {
            console.log('started...');
            this.contentReady = false;
            this.hljs.configure(this.options);
            this.registerContentListener();
        }

        onHighlight(e) {
            console.log('highlighted');
            console.log(e);
        }

        registerContentListener() {
            this.eventManager.subscribe('file-selected', (item) => {
                console.log('fileviewer-file-listener');
                // console.log(item);
                this.content = null;
                this.contentReady = false;
                this.loading = true;
                if (item && item.content) {
                    this.loadContent(item.content);
                }
            });
        }

        loadContent(postFile: PostFile) {
            console.log('loading-content');
            this.fileName = postFile.path.substring(postFile.path.indexOf('traces'));
            const path = encodeURI(this.fileName);
            this.treeviewService.find(path).subscribe((res) => {
               // console.log(res);
                this.content = res.body;
                this.contentReady = true;
                this.loading = false;
               // console.log(this.content);
                this.eventManager.broadcast({
                    name: 'file-ready',
                    content: 'OK'
                });
            });

        }

        loadingGif() {
            const num = Math.floor(Math.random() * Math.floor(2));
            let url = '/content/images/0.gif';
            switch (num) {
                case 0:
                    url = '/content/images/loading-0.gif';
                    break;
                case 1:
                    url = '/content/images/loading-1.gif';
                    break;
                default:
                    url = '/content/images/0.gif';
                    break;
            }
            return this.getSafeUrl(url);
        }

        getSafeUrl(url: string): SafeResourceUrl {
            return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        }

}
