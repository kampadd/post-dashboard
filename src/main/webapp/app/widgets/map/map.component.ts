import { Component, Input, OnInit } from '@angular/core';
import { Extent, layer, source, ProjectionLike } from 'openlayers';
import * as ol from 'openlayers';
@Component({
    selector: 'jhi-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
    public zoom = 2;
    public opacity = 1.0;
    public width = 5;
    public minZoom = 2;
    public maxZoom = 20;
    @Input() imageStaticUrl = '';
    @Input() markX = 0;
    @Input() markY = 0;
    @Input() showVector = false;
    @Input() extent: Extent = [0, 0, 1920, 1080];
    @Input() imageExtent: Extent = [0, 0, 1920, 1080];
    @Input() minResolution: number;
    @Input() maxResolution: number;
    @Input() zIndex: number;
    projection: ol.proj.Projection;
    imageProjection: ol.proj.Projection;
    center: ol.Coordinate;

    ngOnInit() {
      this.projection = new ol.proj.Projection({
        code : 'EPSG:3857',
        extent : this.extent
      });
      this.imageProjection = new ol.proj.Projection({
        code : 'EPSG:3857',
        units: 'pixels',
        extent: this.imageExtent
      });
      this.center = ol.extent.getCenter(this.imageExtent); // ol.proj.transform(ol.extent.getCenter(this.imageExtent), 'EPSG:27700', 'EPSG:3857');
    }

  increaseZoom() {
    this.zoom  = Math.min(this.zoom + 1, 20);
    console.log('zoom: ', this.zoom);
  }

  decreaseZoom() {
    this.zoom  = Math.max(this.zoom - 1, 1);
    console.log('zoom: ', this.zoom);
  }

  increaseOpacity() {
    this.opacity  = Math.min(this.opacity + 0.1, 1);
    console.log('opacity: ', this.opacity);
  }

  decreaseOpacity() {
    this.opacity  = Math.max(this.opacity - 0.1, 0);
    console.log('opacity: ', this.opacity);
  }
}
