import { Component, Input, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import {Event} from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'jhi-svg-viewer',
    templateUrl: './svg-viewer.component.html',
    styleUrls: ['./svg-viewer.component.scss']
})
export class SVGViewerComponent implements OnInit {

    @Input() sourceFile: any;
    @ViewChild('svgviewer') svgviewer: ElementRef;

    constructor(private sanitizer: DomSanitizer) {}

    ngOnInit(): void {
        console.log('Sanitizing...');
        if (this.sourceFile !== null && typeof(this.sourceFile) !== 'undefined') {
            console.log(this.sourceFile);
            this.sourceFile = this.sanitizer.bypassSecurityTrustResourceUrl(this.sourceFile);
            console.log(this.sourceFile);
        }
    }

}
