import { Component, OnInit, EventEmitter  } from '@angular/core';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { Observable } from 'rxjs/Observable';
import { HttpResponse, HttpErrorResponse, HttpEvent, HttpClient, HttpRequest } from '@angular/common/http';
import { TraceLog, TraceLogService } from '../../entities/trace-log';
import { Subscription } from 'rxjs';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';

@Component({
    selector: 'jhi-upload',
    templateUrl: 'uploader.component.html',
    styles: ['./uploader.component.scss'],
})
export class PostUploaderComponent implements OnInit {

    traceLog: TraceLog;
    isSaving: boolean;
    accepts = '*';
    maxSize = '51200';
    options: UploaderOptions;
    formData: FormData;
    files: UploadFile[];
    file: UploadFile;
    uploadInput: EventEmitter<UploadInput>;
    humanizeBytes: Function;
    dragOver: boolean;
    url = 'api/post-projects/upload';
    projectName: string;

    constructor(private dataUtils: JhiDataUtils, public httpClient: HttpClient,
                private traceLogService: TraceLogService,
                private eventManager: JhiEventManager) {
                this.traceLog = new TraceLog();
                this.options = { concurrency: 1, maxUploads: 3 };
                this.files = []; // local uploading files array
                this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
                this.humanizeBytes = humanizeBytes;
                }

    ngOnInit() {

    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    setFileData(event, entity, field, isImage) {
        this.dataUtils.setFileData(event, entity, field, isImage);
    }

    save() {
        this.isSaving = true;
        if (this.traceLog.id !== undefined) {
            this.subscribeToSaveResponse(
                this.traceLogService.update(this.traceLog));
        } else {
            this.subscribeToSaveResponse(
                this.traceLogService.create(this.traceLog));
        }
    }
    private subscribeToSaveResponse(result: Observable<HttpResponse<TraceLog>>) {
        result.subscribe((res: HttpResponse<TraceLog>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TraceLog) {
        this.eventManager.broadcast({ name: 'traceLogListModification', content: 'OK'});
        this.isSaving = false;
    }

    private onSaveError() {
        this.isSaving = false;
    }
    onUploadOutput(output: UploadOutput): void {
        switch (output.type) {
          case 'allAddedToQueue':
              // uncomment this if you want to auto upload files when added
              // const event: UploadInput = {
              //   type: 'uploadAll',
              //   url: '/upload',
              //   method: 'POST',
              //   data: { foo: 'bar' }
              // };
              // this.uploadInput.emit(event);
            break;
          case 'addedToQueue':
            if (typeof output.file !== 'undefined') {
              this.files.push(output.file);
            }
            break;
          case 'uploading':
            if (typeof output.file !== 'undefined') {
              // update current data in files array for uploading file
              const index = this.files.findIndex((file) => typeof output.file !== 'undefined' && file.id === output.file.id);
              this.files[index] = output.file;
            }
            break;
          case 'removed':
            // remove file from array when removed
            this.files = this.files.filter((file: UploadFile) => file !== output.file);
            break;
          case 'dragOver':
            this.dragOver = true;
            break;
          case 'dragOut':
          case 'drop':
            this.dragOver = false;
            break;
          case 'done':
            // The file is downloaded
            break;
        }
      }

      startUpload(): void {
        const event: FileItem = {
          type: 'uploadAll',
          url: this.url,
          method: 'POST',
          data: { project: this.projectName, postFile: this.file }
        };

        this.uploadInput.emit(event);
      }

      cancelUpload(fileId: string): void {
        this.uploadInput.emit({ type: 'cancel', id: fileId });
      }

      removeFile(fileId: string): void {
        this.uploadInput.emit({ type: 'remove', id: fileId });
      }

      removeAllFiles(): void {
        this.uploadInput.emit({ type: 'removeAll' });
      }
      cancel() {
        console.log('cancel');
        window.history.back();
      }
}

export interface FileItem extends UploadInput {
  type: 'uploadAll' | 'uploadFile' | 'cancel' | 'cancelAll' | 'remove' | 'removeAll';
  url?: string;
  method?: string;
  id?: string;
  fieldName?: string;
  fileIndex?: number;
  file?: UploadFile;
  data?: {
      [key: string]: string | any;
  };
  headers?: {
      [key: string]: string;
  };
  includeWebKitFormBoundary?: boolean;
  withCredentials?: boolean;
}
