import { OnInit, Input, Component } from '@angular/core';
import * as d3 from 'd3';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'jhi-d3-svg',
    templateUrl: './d3-svg.component.html',
    styles: ['./d3-svg.component.scss']
})
export class D3SvgViewerComponent implements OnInit {
    @Input() sourceFile: any;
    svgElement: any;
    svg: any;
    width = 1042;
    height = 800;
    constructor(private sanitizer: DomSanitizer) {}
    ngOnInit(): void {
        console.log('Sanitizing...');
        if (this.sourceFile !== null && typeof(this.sourceFile) !== 'undefined') {
            console.log(this.sourceFile);
            // this.sourceFile = this.sanitizer.bypassSecurityTrustResourceUrl(this.sourceFile);
            console.log(this.sourceFile);
            d3.xml(this.sourceFile).then((res: Document) => {
                console.log(res);
               // this.svgElement = this.sanitizer.bypassSecurityTrustHtml(res.rootElement);
               this.svgElement = res.rootElement;
               document.getElementById('vport').appendChild(this.svgElement);
               console.log(this.svgElement);
               this.svg =  d3.select('svg');
               if (this.svg !== undefined) {
                   const rect = this.svg.select('rect');
                   rect.attr('width', this.width)
                        .attr('height', this.height)
                        .attr('pointer-events', 'all');
               }
            });
        }
    }
    scale() {
       return  d3.scaleLinear();
    }

    zoom() {
        const features = d3.select(this.svgElement).selectAll('g');
        features.attr('transform', d3.event.transform);
    }

    transform(t) {
        return (d) => {
          return 'translate(' + t.apply(d) + ')';
        };
      }
}
