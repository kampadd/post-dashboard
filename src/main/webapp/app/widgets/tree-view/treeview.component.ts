import { Component, OnInit, Input, Output, EventEmitter, OnChanges, ViewChild, Injectable } from '@angular/core';
import { TreeviewItem, DropdownTreeviewComponent, TreeviewI18n, TreeviewI18nDefault, TreeviewSelection, TreeviewConfig } from 'ngx-treeview';
import { JhiEventManager } from 'ng-jhipster';

export class DropdownTreeviewSelectI18n extends TreeviewI18nDefault {
    private internalSelectedItem: TreeviewItem;

    set selectedItem(value: TreeviewItem) {
        if (value && value.children === undefined) {
            this.internalSelectedItem = value;
        }
    }

    get selectedItem(): TreeviewItem {
        return this.internalSelectedItem;
    }

    getText(selection: TreeviewSelection): string {
        return this.internalSelectedItem ? this.internalSelectedItem.text : 'ALL';
    }
}

@Component({
    selector: 'jhi-tree-view',
    templateUrl: './treeview.component.html',
    styles: ['./treeview.component.scss'],
    providers: [
        { provide: TreeviewI18n, useClass: DropdownTreeviewSelectI18n },
    ]
})
export class NGXPostTreeviewComponent implements OnInit, OnChanges {
    @Input() config: TreeviewConfig;
    @Input() items: any;
    treeviewItems: TreeviewItem[];
    @ViewChild(DropdownTreeviewComponent) dropdownTreeviewComponent: DropdownTreeviewComponent;
    private dropdownTreeviewSelectI18n: DropdownTreeviewSelectI18n;
    @Input() value: any;
    @Output() valueChange = new EventEmitter<any>();
    @Output() selectedItem = new EventEmitter<any>();

    constructor(public i18n: TreeviewI18n, private eventManager: JhiEventManager) {
        this.initConfig();
        this.dropdownTreeviewSelectI18n = i18n as DropdownTreeviewSelectI18n;
    }

    initConfig() {
      this.config =  TreeviewConfig.create({
            hasAllCheckBox: false,
            hasFilter: false,
            hasCollapseExpand: true,
            maxHeight: 600
         });
      this.treeviewItems = [];
    }

    ngOnChanges() {

    }

    ngOnInit() {
        this.convertItems(this.items);
        this.eventManager.subscribe('pathSelected', (item) => {
            console.log('event-path-selected in treeview');
            this.items = item.content;
            this.convertItems(this.items);
            this.reset();
        });
    }

    onSelectedChange(item: any) {
        console.log('selected-file');
        this.select(item);
    }

    convertItems(item: any) {
        this.treeviewItems = [];
        const treeRoot = new TreeviewItem({
            text: item.name,
            value: item.path,
            children: item.files.length > 0 ? this.getTreeViewItems(item.files) : []
        });
        this.treeviewItems.push(treeRoot);
    }

    getTreeViewItems( files: any[] ) {
        const nodes: TreeviewItem[] = [];
        files.forEach((item) => {
            nodes.push(new TreeviewItem({
                text: item.name,
                value: item.path,
                children: item.files.length > 0 ? this.getTreeViewItems(item.files) : []
            }));
        });
        return nodes;
    }

    select(item: TreeviewItem) {
        if (item.children === undefined) {
            this.dropdownTreeviewComponent.dropdownDirective.close();
            if (this.dropdownTreeviewSelectI18n.selectedItem !== item) {
                this.dropdownTreeviewSelectI18n.selectedItem = item;
                if (this.value !== item.value) {
                    this.value = item.value;
                    this.valueChange.emit(item.value);
                    this.selectedItem.emit(item);
                    console.log('value-changed');
                    console.log(item);
                }
            }
        }
    }

    reset() {
        this.dropdownTreeviewComponent.dropdownDirective.close();
        const item = new TreeviewItem({text: 'ALL', value: 'ALL'});
        this.dropdownTreeviewSelectI18n.selectedItem = item;
        this.value = item.value;
        this.dropdownTreeviewComponent.dropdownDirective.open();
    }
}
