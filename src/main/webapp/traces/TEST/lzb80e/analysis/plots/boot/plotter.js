function POSTCanvasPlotter() {
    canvas = document.getElementById("POSTCanvasPlotter");
    ctx = canvas.getContext("2d");
    // Reinitialize mouse tracking and zoom for this particular plot
    if ((typeof(gnuplot.active_plot) == undefined || gnuplot.active_plot !== POSTCanvasPlotter) && typeof(gnuplot.mouse_update) !== undefined) {
        gnuplot.active_plot_name = "POSTCanvasPlotter";
        gnuplot.active_plot = POSTCanvasPlotter;
        canvas.onmousemove = gnuplot.mouse_update;
        canvas.onmouseup = gnuplot.zoom_in;
        canvas.onmousedown = gnuplot.saveclick;
        canvas.onkeypress = gnuplot.do_hotkey;
        if (canvas.attachEvent) { canvas.attachEvent('mouseover', POSTCanvasPlotter); } else if (canvas.addEventListener) { canvas.addEventListener('mouseover', POSTCanvasPlotter, false); }
        gnuplot.zoomed = false;
        gnuplot.zoom_axis_width = 0;
        gnuplot.zoom_in_progress = false;
        gnuplot.polar_mode = false;
        ctx.clearRect(0, 0, 1620, 768);
    }
    // Gnuplot version 4.6.4
    // short forms of commands provided by gnuplot_common.js
    function DT(dt) { gnuplot.dashtype(dt); };

    function DS(x, y) { gnuplot.dashstart(x, y); };

    function DL(x, y) { gnuplot.dashstep(x, y); };

    function M(x, y) { if (gnuplot.pattern.length > 0) DS(x, y);
        else gnuplot.M(x, y); };

    function L(x, y) { if (gnuplot.pattern.length > 0) DL(x, y);
        else gnuplot.L(x, y); };

    function Dot(x, y) { gnuplot.Dot(x / 10., y / 10.); };

    function Pt(N, x, y, w) { gnuplot.Pt(N, x / 10., y / 10., w / 10.); };

    function R(x, y, w, h) { gnuplot.R(x, y, w, h); };

    function T(x, y, fontsize, justify, string) { gnuplot.T(x, y, fontsize, justify, string); };

    function TR(x, y, angle, fontsize, justify, string) { gnuplot.TR(x, y, angle, fontsize, justify, string); };

    function bp(x, y) { gnuplot.bp(x, y); };

    function cfp() { gnuplot.cfp(); };

    function cfsp() { gnuplot.cfsp(); };

    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    CanvasTextFunctions.enable(ctx);
    ctx.strokeStyle = "rgb(215,215,215)";
    ctx.lineWidth = 1;

    ctx.lineWidth = 1;
    ctx.strokeStyle = "rgb(000,000,000)";
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 5376);
        L(15959, 5376);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 5376);
    L(1360, 5376);
    M(15959, 5376);
    L(15859, 5376);
    M(1180, 5376);
    M(435, 5376);
    ctx.stroke();
    ctx.closePath();
    T(435, 5426, 10.0, "", "ts");
    T(547, 5457, 8.0, "", "a");
    T(604, 5426, 10.0, "", "pplication");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 4812);
        L(15959, 4812);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 4812);
    L(1360, 4812);
    M(15959, 4812);
    L(15859, 4812);
    M(1180, 4812);
    M(595, 4812);
    ctx.stroke();
    ctx.closePath();
    T(595, 4862, 10.0, "", "ts");
    T(707, 4893, 8.0, "", "b");
    T(764, 4862, 10.0, "", "itadapt");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 4248);
        L(15959, 4248);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 4248);
    L(1360, 4248);
    M(15959, 4248);
    L(15859, 4248);
    M(1180, 4248);
    M(518, 4248);
    ctx.stroke();
    ctx.closePath();
    T(518, 4298, 10.0, "", "ts");
    T(630, 4329, 8.0, "", "f");
    T(668, 4298, 10.0, "", "ileparser");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 3684);
        L(15959, 3684);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 3684);
    L(1360, 3684);
    M(15959, 3684);
    L(15859, 3684);
    M(1180, 3684);
    M(756, 3684);
    ctx.stroke();
    ctx.closePath();
    T(756, 3734, 10.0, "", "ts");
    T(868, 3765, 8.0, "", "m");
    T(964, 3734, 10.0, "", "vb0");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 3120);
        L(15959, 3120);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 3120);
    L(1360, 3120);
    M(15959, 3120);
    L(15859, 3120);
    M(1180, 3120);
    M(745, 3120);
    ctx.stroke();
    ctx.closePath();
    T(745, 3170, 10.0, "", "ts");
    T(857, 3201, 8.0, "", "v");
    T(908, 3170, 10.0, "", "24r");
    T(1116, 3201, 8.0, "", "0");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 2557);
        L(15959, 2557);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 2557);
    L(1360, 2557);
    M(15959, 2557);
    L(15859, 2557);
    M(1180, 2557);
    M(769, 2557);
    ctx.stroke();
    ctx.closePath();
    T(769, 2607, 10.0, "", "ts");
    T(881, 2638, 8.0, "", "x");
    T(932, 2607, 10.0, "", "fer0");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 1993);
        L(15959, 1993);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 1993);
    L(1360, 1993);
    M(15959, 1993);
    L(15859, 1993);
    M(1180, 1993);
    M(625, 1993);
    ctx.stroke();
    ctx.closePath();
    T(625, 2043, 10.0, "", "ts");
    T(737, 2074, 8.0, "", "x");
    T(788, 2043, 10.0, "", "ferba0");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 1429);
        L(15959, 1429);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 1429);
    L(1360, 1429);
    M(15959, 1429);
    L(15859, 1429);
    M(1180, 1429);
    M(593, 1429);
    ctx.stroke();
    ctx.closePath();
    T(593, 1479, 10.0, "", "ts");
    T(705, 1510, 8.0, "", "x");
    T(756, 1479, 10.0, "", "fermvb");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 865);
        L(15959, 865);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 865);
    L(1360, 865);
    M(15959, 865);
    L(15859, 865);
    M(1180, 865);
    M(601, 865);
    ctx.stroke();
    ctx.closePath();
    T(601, 915, 10.0, "", "ts");
    T(713, 946, 8.0, "", "x");
    T(764, 915, 10.0, "", "fertrg0");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(1260, 5940);
        L(1260, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(1260, 5940);
    L(1260, 5840);
    M(1260, 301);
    L(1260, 401);
    ctx.stroke();
    ctx.closePath();
    TR(1260, 6070, 270, 10.0, "Right", "946860907.000000000");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(4200, 5940);
        L(4200, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(4200, 5940);
    L(4200, 5840);
    M(4200, 301);
    L(4200, 401);
    ctx.stroke();
    ctx.closePath();
    TR(4200, 6070, 270, 10.0, "Right", "946860907.500000000");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(7140, 5940);
        L(7140, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(7140, 5940);
    L(7140, 5840);
    M(7140, 301);
    L(7140, 401);
    ctx.stroke();
    ctx.closePath();
    TR(7140, 6070, 270, 10.0, "Right", "946860908.000000000");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(10079, 5940);
        L(10079, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(10079, 5940);
    L(10079, 5840);
    M(10079, 301);
    L(10079, 401);
    ctx.stroke();
    ctx.closePath();
    TR(10079, 6070, 270, 10.0, "Right", "946860908.500000000");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(13019, 5940);
        L(13019, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(13019, 5940);
    L(13019, 5840);
    M(13019, 301);
    L(13019, 401);
    ctx.stroke();
    ctx.closePath();
    TR(13019, 6070, 270, 10.0, "Right", "946860909.000000000");
    if (gnuplot.grid_lines) {
        var saveWidth = ctx.lineWidth;
        ctx.lineWidth = ctx.lineWidth * 0.5;
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(15959, 5940);
        L(15959, 301);
        ctx.stroke();
        ctx.closePath();
        ctx.strokeStyle = "rgb(000,000,000)";
        ctx.lineWidth = saveWidth;
    } // grid_lines
    ctx.beginPath();
    M(15959, 5940);
    L(15959, 5840);
    M(15959, 301);
    L(15959, 401);
    ctx.stroke();
    ctx.closePath();
    TR(15959, 6070, 270, 10.0, "Right", "946860909.500000000");
    ctx.beginPath();
    M(1260, 301);
    L(1260, 5940);
    L(15959, 5940);
    L(15959, 301);
    L(1260, 301);
    ctx.closePath();
    ctx.stroke();
    TR(90, 3171, 270, 10.0, "Center", "Taskset");
    T(8609, 7660, 10.0, "Center", "Time");
    ctx.beginPath();
    M(8609, 151);
    M(7252, 151);
    ctx.stroke();
    ctx.closePath();
    T(7252, 201, 10.0, "", "Taskset Boot (lzb80e/cstrace, host evc");
    T(9716, 232, 8.0, "", "s");
    T(9767, 201, 10.0, "", "1");
    T(9847, 232, 8.0, "", "0");
    T(9911, 201, 10.0, "", ")");
    if (typeof(gnuplot.hide_POSTCanvasPlotter_plot_1) == undefined || !gnuplot.hide_POSTCanvasPlotter_plot_1) {
        ctx.lineWidth = 2;
        ctx.strokeStyle = "rgb(255,000,000)";
        ctx.strokeStyle = "rgb(000,000,000)";
        T(15299, 513, 10.0, "Right", "Boot Time");
        ctx.strokeStyle = "rgb(255,000,000)";
        ctx.beginPath();
        M(15678, 495);
        L(15799, 463);
        L(15678, 431);
        M(15379, 463);
        L(15799, 463);
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        M(15632, 5427);
        L(15825, 5376);
        L(15632, 5325);
        M(8961, 5376);
        L(15825, 5376);
        M(13871, 4863);
        L(14064, 4812);
        L(13871, 4761);
        M(9026, 4812);
        L(14064, 4812);
        M(5938, 4299);
        L(6131, 4248);
        L(5938, 4197);
        M(3236, 4248);
        L(6131, 4248);
        M(12986, 3735);
        L(13179, 3684);
        L(12986, 3633);
        M(8919, 3684);
        L(13179, 3684);
        M(6816, 3171);
        L(7009, 3120);
        L(6816, 3069);
        M(3164, 3120);
        L(7009, 3120);
        M(6816, 2608);
        L(7009, 2557);
        L(6816, 2506);
        M(3211, 2557);
        L(7009, 2557);
        M(6816, 2044);
        L(7009, 1993);
        L(6816, 1942);
        M(3145, 1993);
        L(7009, 1993);
        M(5939, 1480);
        L(6132, 1429);
        L(5939, 1378);
        M(3188, 1429);
        L(6132, 1429);
        M(6815, 916);
        L(7008, 865);
        L(6815, 814);
        M(3260, 865);
        L(7008, 865);
        ctx.stroke();
        ctx.closePath();
    } // End POSTCanvasPlotter_plot_1 
    if (typeof(gnuplot.hide_POSTCanvasPlotter_plot_2) == undefined || !gnuplot.hide_POSTCanvasPlotter_plot_2) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = "rgb(000,171,000)";
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.strokeStyle = "rgb(000,171,000)";
        ctx.strokeStyle = "rgb(160,160,160)";
        ctx.beginPath();
        M(8961, 5940);
        L(8961, 301);
        M(15825, 5940);
        L(15825, 301);
        M(9026, 5940);
        L(9026, 301);
        M(14064, 5940);
        L(14064, 301);
        M(3236, 5940);
        L(3236, 301);
        M(6131, 5940);
        L(6131, 301);
        M(8919, 5940);
        L(8919, 301);
        M(13179, 5940);
        L(13179, 301);
        M(3164, 5940);
        L(3164, 301);
        M(7009, 5940);
        L(7009, 301);
        M(3211, 5940);
        L(3211, 301);
        M(7009, 5940);
        L(7009, 301);
        M(3145, 5940);
        L(3145, 301);
        M(7009, 5940);
        L(7009, 301);
        M(3188, 5940);
        L(3188, 301);
        M(6132, 5940);
        L(6132, 301);
        M(3260, 5940);
        L(3260, 301);
        M(7008, 5940);
        L(7008, 301);
        ctx.stroke();
        ctx.closePath();
    } // End POSTCanvasPlotter_plot_2 
    if (typeof(gnuplot.hide_POSTCanvasPlotter_plot_3) == undefined || !gnuplot.hide_POSTCanvasPlotter_plot_3) {
        ctx.lineWidth = 1;
        ctx.strokeStyle = "rgb(000,000,148)";
        ctx.strokeStyle = "rgb(000,171,000)";
        ctx.strokeStyle = "rgb(000,000,000)";
        T(15299, 638, 10.0, "Right", "Thread created");
        ctx.strokeStyle = "rgb(000,000,148)";
        ctx.strokeStyle = "rgb(000,171,000)";
        ctx.fillStyle = "rgb(000,171,000)";
        Pt(6, 8961, 5376, 60.0);
        Pt(6, 9775, 5376, 60.0);
        Pt(6, 11419, 5376, 60.0);
        Pt(6, 11427, 5376, 60.0);
        Pt(6, 11435, 5376, 60.0);
        Pt(6, 11749, 5376, 60.0);
        Pt(6, 11756, 5376, 60.0);
        Pt(6, 11763, 5376, 60.0);
        Pt(6, 12749, 5376, 60.0);
        Pt(6, 12754, 5376, 60.0);
        Pt(6, 12758, 5376, 60.0);
        Pt(6, 9026, 4812, 60.0);
        Pt(6, 10536, 4812, 60.0);
        Pt(6, 10570, 4812, 60.0);
        Pt(6, 10577, 4812, 60.0);
        Pt(6, 3236, 4248, 60.0);
        Pt(6, 4371, 4248, 60.0);
        Pt(6, 8919, 3684, 60.0);
        Pt(6, 9784, 3684, 60.0);
        Pt(6, 10290, 3684, 60.0);
        Pt(6, 3164, 3120, 60.0);
        Pt(6, 3481, 3120, 60.0);
        Pt(6, 3504, 3120, 60.0);
        Pt(6, 3211, 2557, 60.0);
        Pt(6, 3508, 2557, 60.0);
        Pt(6, 3145, 1993, 60.0);
        Pt(6, 3485, 1993, 60.0);
        Pt(6, 3188, 1429, 60.0);
        Pt(6, 4365, 1429, 60.0);
        Pt(6, 4375, 1429, 60.0);
        Pt(6, 3260, 865, 60.0);
        Pt(6, 3489, 865, 60.0);
        Pt(6, 15589, 588, 60.0);
    } // End POSTCanvasPlotter_plot_3 
    ctx.lineWidth = 1;
    ctx.strokeStyle = "rgb(000,000,000)";
    ctx.beginPath();
    M(1260, 301);
    L(1260, 5940);
    L(15959, 5940);
    L(15959, 301);
    L(1260, 301);
    ctx.closePath();
    ctx.stroke();

    // plot boundaries and axis scaling information for mousing 
    gnuplot.plot_term_xmax = 1620;
    gnuplot.plot_term_ymax = 768;
    gnuplot.plot_xmin = 126.0;
    gnuplot.plot_xmax = 1595.9;
    gnuplot.plot_ybot = 594.0;
    gnuplot.plot_ytop = 30.1;
    gnuplot.plot_width = 1469.9;
    gnuplot.plot_height = 563.9;
    gnuplot.plot_axis_xmin = 0;
    gnuplot.plot_axis_xmax = 9.46861e+08;
    gnuplot.plot_axis_ymin = -1;
    gnuplot.plot_axis_ymax = 9;
    gnuplot.plot_axis_x2min = "none"
    gnuplot.plot_axis_y2min = "none"
    gnuplot.plot_logaxis_x = 0;
    gnuplot.plot_logaxis_y = 0;
    gnuplot.plot_axis_width = gnuplot.plot_axis_xmax - gnuplot.plot_axis_xmin;
    gnuplot.plot_axis_height = gnuplot.plot_axis_ymax - gnuplot.plot_axis_ymin;
    gnuplot.plot_timeaxis_x = "";
}