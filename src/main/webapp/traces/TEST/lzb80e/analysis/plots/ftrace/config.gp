set format x "%.9f"

set xtic rotate

set ytics 1

set grid

set key outside right

set mouse format "%.9f"

TMF_TYPE="ftrace"

TMF_X_AXIS="uptime"

set xlabel "Duration [s]"

set ylabel "Processes"

set key outside right

set title "Ftrace (lzb80e)"
process_IDLE(x)=0
process_OLT(x)=1
process_xfibussd_788(x)=4
process_tc_main_890(x)=5
process_xfer0_860(x)=6
process_xfer0_852(x)=7
process_SYSLOG(x)=3
process_log_main_509(x)=8
process_bitadapter_882(x)=9
process_bitadapter_883(x)=10
process_xferba0_856(x)=11
process_xferba0_853(x)=12
process_mvb_dl0_879(x)=13
process_SL_FT(x)=2
process_trigger_thr_885(x)=14
process_trigger_thr_884(x)=15
process_xfermvb_861(x)=16
process_trigreply_thr_886(x)=17
process_trigreply_thr_887(x)=18
process_mvb_dev_thr_880(x)=19
process_xfermvb_862(x)=20
process_VOTFP_900(x)=21
process_rcu_preempt_7(x)=22
process_2oo3RECV_903(x)=23
process_mvb_pl_889(x)=24
process_mvb_dl0_877(x)=25
process_ksoftirqd_0_3(x)=26
process_mvb_DILI_th_895(x)=27
process_mvb_CHECK_th_896(x)=28
process_mvb_dev_thr_881(x)=29
process_kworker_u4_1_42(x)=30
process_mvb_pl_888(x)=31
process_TID_2_859(x)=32
process_kworker_u4_2_590(x)=33
process_mvb_CHECK_th_897(x)=34
process_VOTFP_901(x)=35
process_2oo3RECV_902(x)=36
process_tc_main_891(x)=37
process_ksoftirqd_1_14(x)=38
process_v24r_0_851(x)=39
process_mvb_DILI_th_894(x)=40
process_fileparser_865(x)=41
process_fileparser_863(x)=42
process_kworker_0_0_4(x)=43
process_kworker_1_2_215(x)=44
process_watchdog_0_11(x)=45
process_watchdog_1_12(x)=46
process_init_1(x)=47
process_tc_main_870(x)=48
process______sac73_898(x)=49
process_V24RECV_904(x)=50
process_bitadapter_868(x)=51
process______sac73_892(x)=52
process_mvb_pl_869(x)=53
process_reaper_836(x)=54
process_RPC_SV_837(x)=55
process___bitadapter_ct_876(x)=56
process_diag_832(x)=57
process_appl_init_812(x)=58
process______sac73_893(x)=59
process___mvb_pl_ctrl_875(x)=60
process_V24RECV_905(x)=61
process______sac73_899(x)=62
process___tc_main_ctrl_878(x)=63
process_cs_813(x)=64
process_fileparser_843(x)=65
process_appl_init_811(x)=66
process_logger_917(x)=67
process_migration_1_13(x)=68
process_log_diagfs_522(x)=69
process___fileparser_ct_849(x)=70
set label "IDLE" at graph 0, first 0.2
set label "OLT" at graph 0, first 1.2
set label "SL/FT" at graph 0, first 2.2
set label "SYSLOG" at graph 0, first 3.2
set label "xfibussd-788" at graph 0, first 4.2
set label "tc_main-890" at graph 0, first 5.2
set label "xfer0-860" at graph 0, first 6.2
set label "xfer0-852" at graph 0, first 7.2
set label "log main-509" at graph 0, first 8.2
set label "bitadapter-882" at graph 0, first 9.2
set label "bitadapter-883" at graph 0, first 10.2
set label "xferba0-856" at graph 0, first 11.2
set label "xferba0-853" at graph 0, first 12.2
set label "mvb_dl0-879" at graph 0, first 13.2
set label "trigger_thr-885" at graph 0, first 14.2
set label "trigger_thr-884" at graph 0, first 15.2
set label "xfermvb-861" at graph 0, first 16.2
set label "trigreply_thr-886" at graph 0, first 17.2
set label "trigreply_thr-887" at graph 0, first 18.2
set label "mvb_dev_thr-880" at graph 0, first 19.2
set label "xfermvb-862" at graph 0, first 20.2
set label "VOTFP-900" at graph 0, first 21.2
set label "rcu_preempt-7" at graph 0, first 22.2
set label "2oo3RECV-903" at graph 0, first 23.2
set label "mvb_pl-889" at graph 0, first 24.2
set label "mvb_dl0-877" at graph 0, first 25.2
set label "ksoftirqd/0-3" at graph 0, first 26.2
set label "mvb_DILI_th-895" at graph 0, first 27.2
set label "mvb_CHECK_th-896" at graph 0, first 28.2
set label "mvb_dev_thr-881" at graph 0, first 29.2
set label "kworker/u4:1-42" at graph 0, first 30.2
set label "mvb_pl-888" at graph 0, first 31.2
set label "TID-2-859" at graph 0, first 32.2
set label "kworker/u4:2-590" at graph 0, first 33.2
set label "mvb_CHECK_th-897" at graph 0, first 34.2
set label "VOTFP-901" at graph 0, first 35.2
set label "2oo3RECV-902" at graph 0, first 36.2
set label "tc_main-891" at graph 0, first 37.2
set label "ksoftirqd/1-14" at graph 0, first 38.2
set label "v24r_0-851" at graph 0, first 39.2
set label "mvb_DILI_th-894" at graph 0, first 40.2
set label "fileparser-865" at graph 0, first 41.2
set label "fileparser-863" at graph 0, first 42.2
set label "kworker/0:0-4" at graph 0, first 43.2
set label "kworker/1:2-215" at graph 0, first 44.2
set label "watchdog/0-11" at graph 0, first 45.2
set label "watchdog/1-12" at graph 0, first 46.2
set label "init-1" at graph 0, first 47.2
set label "tc_main-870" at graph 0, first 48.2
set label "__@@_sac73-898" at graph 0, first 49.2
set label "V24RECV-904" at graph 0, first 50.2
set label "bitadapter-868" at graph 0, first 51.2
set label "__@@_sac73-892" at graph 0, first 52.2
set label "mvb_pl-869" at graph 0, first 53.2
set label "reaper-836" at graph 0, first 54.2
set label "RPC/SV-837" at graph 0, first 55.2
set label "__bitadapter_ct-876" at graph 0, first 56.2
set label "diag-832" at graph 0, first 57.2
set label "appl_init-812" at graph 0, first 58.2
set label "__@@_sac73-893" at graph 0, first 59.2
set label "__mvb_pl_ctrl-875" at graph 0, first 60.2
set label "V24RECV-905" at graph 0, first 61.2
set label "__@@_sac73-899" at graph 0, first 62.2
set label "__tc_main_ctrl-878" at graph 0, first 63.2
set label "cs-813" at graph 0, first 64.2
set label "fileparser-843" at graph 0, first 65.2
set label "appl_init-811" at graph 0, first 66.2
set label "logger-917" at graph 0, first 67.2
set label "migration/1-13" at graph 0, first 68.2
set label "log diagfs-522" at graph 0, first 69.2
set label "__fileparser_ct-849" at graph 0, first 70.2
set xrange [946861062.375:946861065.404]
set yrange [-0.005:70]
plot process_IDLE(x) lt rgb "#AAAAAA" notitle, process_OLT(x) lt rgb "#AAAAAA" notitle, process_xfibussd_788(x) lt rgb "#AAAAAA" notitle, process_tc_main_890(x) lt rgb "#AAAAAA" notitle, process_xfer0_860(x) lt rgb "#AAAAAA" notitle, process_xfer0_852(x) lt rgb "#AAAAAA" notitle, process_SYSLOG(x) lt rgb "#AAAAAA" notitle, process_log_main_509(x) lt rgb "#AAAAAA" notitle, process_bitadapter_882(x) lt rgb "#AAAAAA" notitle, process_bitadapter_883(x) lt rgb "#AAAAAA" notitle, process_xferba0_856(x) lt rgb "#AAAAAA" notitle, process_xferba0_853(x) lt rgb "#AAAAAA" notitle, process_mvb_dl0_879(x) lt rgb "#AAAAAA" notitle, process_SL_FT(x) lt rgb "#AAAAAA" notitle, process_trigger_thr_885(x) lt rgb "#AAAAAA" notitle, process_trigger_thr_884(x) lt rgb "#AAAAAA" notitle, process_xfermvb_861(x) lt rgb "#AAAAAA" notitle, process_trigreply_thr_886(x) lt rgb "#AAAAAA" notitle, process_trigreply_thr_887(x) lt rgb "#AAAAAA" notitle, process_mvb_dev_thr_880(x) lt rgb "#AAAAAA" notitle, process_xfermvb_862(x) lt rgb "#AAAAAA" notitle, process_VOTFP_900(x) lt rgb "#AAAAAA" notitle, process_rcu_preempt_7(x) lt rgb "#AAAAAA" notitle, process_2oo3RECV_903(x) lt rgb "#AAAAAA" notitle, process_mvb_pl_889(x) lt rgb "#AAAAAA" notitle, process_mvb_dl0_877(x) lt rgb "#AAAAAA" notitle, process_ksoftirqd_0_3(x) lt rgb "#AAAAAA" notitle, process_mvb_DILI_th_895(x) lt rgb "#AAAAAA" notitle, process_mvb_CHECK_th_896(x) lt rgb "#AAAAAA" notitle, process_mvb_dev_thr_881(x) lt rgb "#AAAAAA" notitle, process_kworker_u4_1_42(x) lt rgb "#AAAAAA" notitle, process_mvb_pl_888(x) lt rgb "#AAAAAA" notitle, process_TID_2_859(x) lt rgb "#AAAAAA" notitle, process_kworker_u4_2_590(x) lt rgb "#AAAAAA" notitle, process_mvb_CHECK_th_897(x) lt rgb "#AAAAAA" notitle, process_VOTFP_901(x) lt rgb "#AAAAAA" notitle, process_2oo3RECV_902(x) lt rgb "#AAAAAA" notitle, process_tc_main_891(x) lt rgb "#AAAAAA" notitle, process_ksoftirqd_1_14(x) lt rgb "#AAAAAA" notitle, process_v24r_0_851(x) lt rgb "#AAAAAA" notitle, process_mvb_DILI_th_894(x) lt rgb "#AAAAAA" notitle, process_fileparser_865(x) lt rgb "#AAAAAA" notitle, process_fileparser_863(x) lt rgb "#AAAAAA" notitle, process_kworker_0_0_4(x) lt rgb "#AAAAAA" notitle, process_kworker_1_2_215(x) lt rgb "#AAAAAA" notitle, process_watchdog_0_11(x) lt rgb "#AAAAAA" notitle, process_watchdog_1_12(x) lt rgb "#AAAAAA" notitle, process_init_1(x) lt rgb "#AAAAAA" notitle, process_tc_main_870(x) lt rgb "#AAAAAA" notitle, process______sac73_898(x) lt rgb "#AAAAAA" notitle, process_V24RECV_904(x) lt rgb "#AAAAAA" notitle, process_bitadapter_868(x) lt rgb "#AAAAAA" notitle, process______sac73_892(x) lt rgb "#AAAAAA" notitle, process_mvb_pl_869(x) lt rgb "#AAAAAA" notitle, process_reaper_836(x) lt rgb "#AAAAAA" notitle, process_RPC_SV_837(x) lt rgb "#AAAAAA" notitle, process___bitadapter_ct_876(x) lt rgb "#AAAAAA" notitle, process_diag_832(x) lt rgb "#AAAAAA" notitle, process_appl_init_812(x) lt rgb "#AAAAAA" notitle, process______sac73_893(x) lt rgb "#AAAAAA" notitle, process___mvb_pl_ctrl_875(x) lt rgb "#AAAAAA" notitle, process_V24RECV_905(x) lt rgb "#AAAAAA" notitle, process______sac73_899(x) lt rgb "#AAAAAA" notitle, process___tc_main_ctrl_878(x) lt rgb "#AAAAAA" notitle, process_cs_813(x) lt rgb "#AAAAAA" notitle, process_fileparser_843(x) lt rgb "#AAAAAA" notitle, process_appl_init_811(x) lt rgb "#AAAAAA" notitle, process_logger_917(x) lt rgb "#AAAAAA" notitle, process_migration_1_13(x) lt rgb "#AAAAAA" notitle, process_log_diagfs_522(x) lt rgb "#AAAAAA" notitle, process___fileparser_ct_849(x) lt rgb "#AAAAAA" notitle, "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/ftrace/config-switches-0.dat" using 1:2:3:4 with vector  lw 1 lc rgb "#009900" title "switches-cpu0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/ftrace/config-tasks-0.dat" using 1:2:3:4 with vector nohead lw 3 lc rgb "#CC0000" title "tasks-cpu0", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/ftrace/config-switches-1.dat" using 1:2:3:4 with vector  lw 1 lc rgb "#009933" title "switches-cpu1", "/nfsshares/tas/tag/post/tests/traces/john/tarball_examples/lzb80e/analysis/plots/ftrace/config-tasks-1.dat" using 1:2:3:4 with vector nohead lw 3 lc rgb "#000099" title "tasks-cpu1"
pause -1
