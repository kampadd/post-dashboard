set title "Taskset Load (demo/cstrace, host SCC-1)"
set xlabel "Round"
set ylabel "Time (secs)"
set key outside right
set grid
TMF_TYPE="tsload"
TMF_X_AXIS="syncround"

set terminal canvas enhanced size 1680, 768 name "loadTaskset" mousing jsdir "content/js"
set output "../app/src/main/webapp/content/canvas/loadTaskset.js"

plot "tsload/config-main@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#FF0000" lw 2 ti "main@CN_EC",  "tsload/config-ocs_dl0@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#0000FF" lw 2 ti "ocs_dl0@CN_EC",  "tsload/config-ocs_dlxfer@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#00FF66" lw 2 ti "ocs_dlxfer@CN_EC",  "tsload/config-sdm0@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#33FFFF" lw 2 ti "sdm0@CN_EC",  "tsload/config-thacandl0@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#00FF00" lw 2 ti "thacandl0@CN_EC",  "tsload/config-xfer@CN_EC.dat"  using 1:2:3:4 with vector lc rgb "#000000" lw 2 ti "xfer@CN_EC"